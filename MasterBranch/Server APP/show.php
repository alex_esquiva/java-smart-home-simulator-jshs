<html lang="en">

<head>

<meta charset="utf-8">   
<title>JAVA Smart Home Simulator</title>   
<meta name="description">  
<link href="style/bootstrap.css" rel="stylesheet"> 
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<?php

require_once("inc/Functions.php");

if(isset($_GET["action"])){
    
    $action = $_GET["action"];   
    $session = $_GET["session"];
    
    $tool = new Tools();
    
    if($action == "objectsdata"){
        $object = new ObjectData();
        $object->session = $session;
        $rawdata = $object->getInfoBySession();
        if(empty($rawdata)){
            $tool->displayError("Data Not Founded","There are not any data to show into the data base");
        }else{
            $tool->displayObjectsDataTable($rawdata);           
        }
    }elseif ($action == "physicaldata" ) {
        $object = new ObjectData();
        $object->processJson($json);
    }elseif ($action == "scenarydata" ) {
        $object = new ScenaryData();
        $object->processJson($json);
    }
}
?>
</body>
</html>