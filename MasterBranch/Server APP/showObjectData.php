<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Java Smart Home Simulator</title>
  
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="style/css/bootstrap.css">

</head>
<body>
<!-- Latest compiled and minified JavaScript -->
<script type='text/javascript' src='style/js/jquery-1.9.1.js'></script>
<script type='text/javascript' src="style/js/bootstrap.js"></script>
<script type='text/javascript' src="style/js/highcharts.js"></script>
<script type='text/javascript' src="style/js/exporting.js"></script>
<ul class="nav nav-tabs">
  <li class="active"><a href="./">Home</a></li>
  <li><a href="realtime.php">Real Time</a></li>
</ul>

<?php
require_once 'inc/Functions.php';

$tool = new Tools();
$sessionObject = new Session();
$objectdata = new ObjectData();
if(!isset($_GET["session"])){
    $tool->displayError("Error", "You must put the session you want to show");
}else{
    $session = $_GET["session"];
}

$sessionObject->session = $session;
$objectdata->session = $session;

$rawdata = $objectdata->getInfoBySession();
if(empty($rawdata)) $tool->displayError ("Error", "Empty data for session ".$session);

$dataSession = $sessionObject->getAllInfoBySession();
$name = $dataSession[0]["name"];
$lasttime = $dataSession[0]["time"];

$averageEC = 0;
$TotalWC = 0;
$averageTC = 0;
$countEc = 1;
$countWc = 1;
$countTc = 1;
$numItems = count($rawdata);
for($i=0;$i<$numItems;$i++){
    if($i!=0){
        if(($rawdata[$i]["energy"]-$rawdata[$i-1]["energy"])!=0){
            $averageEC += $rawdata[$i]["energy"];
            $countEc++;
        }

        if(($rawdata[$i]["water"]-$rawdata[$i-1]["water"])!=0){
            $TotalWC += $rawdata[$i]["water"];
            $countWc++;
        }
        if(($rawdata[$i]["temperature"]-$rawdata[$i-1]["temperature"])!=0){
            $averageTC += $rawdata[$i]["temperature"];
            $countTc++;
        }
    }else{
        $averageEC += $rawdata[$i]["energy"];
        $TotalWC += $rawdata[$i]["water"];
        $averageTC += $rawdata[$i]["temperature"];
    }
    
    $time = $rawdata[$i]["sim_time"];
    $date = new DateTime($time);
    $rawdata[$i]["time"]=$date->getTimestamp()*1000;
}

$averageEC = $averageEC/$countEc;
//$averageWC = $averageWC/$countWc;
$averageTC = $averageTC/$countTc;


?>

<div class="page-header">
</div>

<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Session Information</h3>
          </div>
          <div class="panel-body">
              <b>Session: </b><?php echo $session;?> 
              <br>
              <b>Name: </b><?php echo $name;?> 
              <br>
              <b>Date: </b><?php echo $lasttime;?> 
          </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">General Information</h3>
          </div>
          <div class="panel-body">
              <b>Total Simulation Time: </b><span id="simulatortime"><?php echo $rawdata[count($rawdata)-1]["sim_time"];?></span>
              <br>
              <b>Number of Iteractions: </b><span id="num_items"><?php echo $objectdata->getNumItems();?></span>
          </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Physical Information</h3>
          </div>
          <div class="panel-body">
              <b>Average Energy Consumed: </b><span id="energyLabel"><?php echo round($averageEC, 3);?></span> Kw/h
            <br>
            <b>Water Consumed: </b><span id="waterLabel"><?php echo round($TotalWC, 3);?></span> L
            <br>
            <b>Average Temperature: </b><span id="temperatureLabel"><?php echo round($averageTC, 3);?></span> ºC
          </div>
        </div>
    </div>
</div>

<div class="page-header">
</div>

<div class="row">
    <div class="col-sm-12 col-md-7">
        <div id="containerEnergy" style="min-width: 150px; height: 500px; margin: 0 auto"></div>
    </div>
    <div class="col-sm-12 col-md-5">
      <ul class="list-group" id="listitems">
          <li class="list-group-item">
              
              <table style="width: 100%; text-align: center;">
                  <tr>
                      <td width="14%"><b>Status</b></td>
                      <td width="20%"><b>Object</b></td>
                      <td width="14%"><b>Place</b></td>
                      <td width="14%"><b>KW/H</b></td>
                      <td width="14%"><b>L</b></td>
                      <td width="14%"><b>ºC</b></td>
                      <td width="10%"><b>Time</b></td>
                  </tr>
              </table>
              
          </li>
          <?php
          for($i = 0;$i<count($rawdata);$i++){
              
              $statusicon = "";
              if($rawdata[$i]["status"] == "true"){
                  $statusicon = "Interface/on.png";
              }else{
                  $statusicon = "Interface/off.png";
              }
              
              $place = "-";
              if($rawdata[$i]["place"]!= "Not Founded"){
                  $place = $rawdata[$i]["place"];
              }
              
              $difEc = 0;
              $difWc = 0;
              $difTc = 0;
              if($i>0){
                  $difEc = $rawdata[$i]["energy"] - $rawdata[$i-1]["energy"];
                  $difWc = $rawdata[$i]["water"] - $rawdata[$i-1]["water"];
                  $difTc = $rawdata[$i]["temperature"] - $rawdata[$i-1]["temperature"];
              }else{
                  $difEc = $rawdata[$i]["energy"];
                  $difWc = $rawdata[$i]["water"];
                  $difTc = $rawdata[$i]["temperature"];
              }
              $energyLabel = $rawdata[$i]["energy"]."<br>";
              $waterLabel = $rawdata[$i]["water"]."<br>";
              $temperatureLabel = $rawdata[$i]["temperature"]."<br>";
              if($difEc>=0){
                $energyLabel.="<span style='Color:#00ff00'>+".round($difEc, 3)."</span>";
              }else{
                $energyLabel.="<span style='Color:#ff0000'>".round($difEc, 3)."</span>";
              }
              
              if($difWc>=0){
                  $waterLabel.="<span style='Color:#00ff00'>+".round($difWc, 3)."</span>";
              }else{
                  $waterLabel.="<span style='Color:#ff0000'>".round($difWc, 3)."</span>";
              }

              if($difTc>=0){
                  $temperatureLabel.="<span style='Color:#00ff00'>+".round($difTc, 3)."</span>";
              }else{
                  $temperatureLabel.="<span style='Color:#ff0000'>".round($difTc, 3)."</span>";
              }
              
              echo '<li class="list-group-item">                       
                        <table style="width: 100%; text-align: center;">
                            <tr>
                                <td width="14%"><img src="'.$statusicon.'" width="30px">
                                <img src="'.$rawdata[$i]["iconpath"].'" width="30px"></td>
                                <td width="20%">'.$rawdata[$i]["name"].'</td>
                                <td width="14%">'.$place.'</td> 
                                <td width="14%">'.$energyLabel.'</td> 
                                <td width="14%">'.$waterLabel.'</td> 
                                <td width="14%">'.$temperatureLabel.'</td> 
                                <td width="10%">'.$rawdata[$i]["sim_time"].'</td> 
                            </tr>
                        </table>           
                    </li>';    
          }
          ?>
      </ul>
    </div>
</div>
</body>

<script type='text/javascript'>
$(function () {
    $(document).ready(function() {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
    
        var chart;
        $('#containerEnergy').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function() {
                        // set up the updating of the chart each second
                        /*var energySeries = this.series[0];
                        var waterSeries = this.series[1];
                        var temperatureSeries = this.series[2];
                        setInterval(function() {
                            
                            var x = (new Date()).getTime(); // current time                                
                            
                            var ec = getDataSring();
                            
                            var res = ec.split("##");
                            
                            var energy = parseFloat(res[1]);
                            var water = parseFloat(res[2]);
                            var temperature = parseFloat(res[3]);
                            if(ec!=""){
                                energySeries.addPoint([x, energy], true, true);
                                waterSeries.addPoint([x, water], true, true);
                                temperatureSeries.addPoint([x, temperature], true, true);                            
                            }
                        }, 1000);
                        */
                    }
                }
            },
            title: {
                text: 'Physical Variables'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: true
            },
            exporting: {
                enabled: true
            },
            series: [{
                name: 'Energy Consumption',
                data: (function() {
                   var data = [];
                    <?php
                        for($i = 0 ;$i<count($rawdata);$i++){
                    ?>
                    data.push([<?php echo $rawdata[$i]["time"];?>,<?php echo $rawdata[$i]["energy"];?>]);
                    <?php } ?>
                return data;
                })()
            },{
                name: 'Water Consumption',
                     data: (function() {
                        var data = [];
                    <?php
                        for($i = 0 ;$i<count($rawdata);$i++){
                    ?>
                    data.push([<?php echo $rawdata[$i]["time"];?>,<?php echo $rawdata[$i]["water"];?>]);
                    <?php } ?>
                return data;
                     })() 
            },{
                name: 'Temperature',
                     data: (function() {
                         var data = [];
                    <?php
                        for($i = 0 ;$i<count($rawdata);$i++){
                    ?>
                    data.push([<?php echo $rawdata[$i]["time"];?>,<?php echo $rawdata[$i]["temperature"];?>]);
                    <?php } ?>
                return data;
                     })()     
            }]
        });
    });
    
});
//]]>  

</script>
</html>