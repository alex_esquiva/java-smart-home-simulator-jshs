<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Java Smart Home Simulator</title>
  
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="style/css/bootstrap.css">

</head>
<body>
<!-- Latest compiled and minified JavaScript -->
<script type='text/javascript' src='style/js/jquery-1.9.1.js'></script>
<script type='text/javascript' src="style/js/bootstrap.js"></script>
<script type='text/javascript' src="style/js/highcharts.js"></script>
<script type='text/javascript' src="style/js/exporting.js"></script>

<ul class="nav nav-tabs">
  <li><a href="./">Home</a></li>
  <li class="active"><a href="realtime.php">Real Time</a></li>
</ul>

<?php
    if(isset($_GET["getData"])){
            require_once 'inc/Functions.php';
            $object = new ObjectData();
            $session = "0";
            $sessionStatus = "true";
            if(isset($_GET["session"])){
                $session = $_GET["session"];
                $sessionObject = new Session();
                $sessionObject->session = $session;            
                if($sessionObject->isClosedSession()){
                    $sessionStatus =  "true";
                }else{
                    $sessionStatus =  "false";
                }
            }
        if($_GET["getData"] == "data"){
            $object->session = $session;
            $data = $object->getLastInfo();
            if(isset($data)){
                //Energy Consumption
                echo "##";
                echo $data[0]["energy"];
                echo "##";
                //Energy Water
                echo $data[0]["water"];
                echo "##";
                //Temperature
                echo $data[0]["temperature"];
                echo "##";
                //num_items
                echo $object->getNumItems();
                echo "##";
                //name
                echo $data[0]["name"];
                echo "##";
                //status
                echo $data[0]["status"];
                echo "##";
                //iconPath
                echo $data[0]["iconpath"];
                echo "##";
                //Place
                echo $data[0]["place"];
                echo "##";
                //simulator time
                echo $data[0]["sim_time"];
                echo "##";
                //session status
                echo $sessionStatus;
                echo "##";
                die();
            }else{
                //Energy Consumption
                echo "0.000";
                echo "##";
                //Energy Water
                echo "0.000";
                echo "##";
                //Temperature
                echo "0.000";
                echo "##";
                //num_items
                echo "0";
                echo "##";
                //name
                echo "null";
                echo "##";
                //status
                echo "null";
                echo "##";
                //iconPath
                echo "null";
                echo "##";
                //Place
                echo "null";
                echo "##";
                //simulator time
                echo "null";
                echo "##";
                //session status
                echo "true";
                echo "##";
                die();
            } 
        }
    }else{
        require_once 'inc/Functions.php';
        $object = new Session();
        $session =  $object->getLastSession();
        $object->session = $session;
        $data = $object->getAllInfoBySession();
        $name = $data[0]["name"];
        $time = $data[0]["time"];
        if($object->isClosedSession() || empty($session)){
            ?>
                <div class="row">
                    <div class="col-sm-12 col-md-4">

                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="page-header">
                            <h1>Session Closed</h1>
                        </div>
                        <div class="alert alert-info" style="text-align: center;">
                            <strong>Time: </strong><?php echo $time;?><br><strong>Last Session: </strong><?php echo $session;?><br>This Session is closed <br> Waiting new Session....
                        </div>
                        <div class="row">
                            <p>
                                <button type="button" style="width: 100%" class="btn btn-lg btn-primary" onclick="location.href='showObjectData.php?session=<?php echo $session;?>'">Show Objects Data</button>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">

                    </div>
                </div>
                <script>
                var session  = '<?php echo $session;?>';
                var init = true;
                function checkSession(){
                    var ec = $.ajax({
                                url: 'getSession.php', //indicamos la ruta donde se genera la hora
                                dataType: 'text',//indicamos que es de tipo texto plano
                                async: true,
                                success: function(data){
                                    if(data != session){
                                        if(init){
                                            location.href="realtime.php";
                                            init = !init;
                                        }
                                    }
                                },//ponemos el parámetro asyn a falso
                     }).responseText;
                }
                
                setInterval(function(){checkSession()},1000);

                </script>
            <?php
            die();
        }
    }
?>


<div class="page-header">
    <h5>Information</h5>
</div>

<div class="row">
    <div class="col-sm-12 col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Session Information</h3>
          </div>
          <div class="panel-body">
              <b>Session: </b><?php echo $session;?> 
              <br>
              <b>Name: </b><?php echo $name;?> 
              <br>
              <b>Date: </b><?php echo $time;?> 
          </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">General Information</h3>
          </div>
          <div class="panel-body">
              <b>Simulator Time: </b><span id="simulatortime">00:00:00</span>
              <br>
              <b>Number of Iteractions: </b><span id="num_items">0</span>
          </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Physical Information</h3>
          </div>
          <div class="panel-body">
              <b>Energy Consumption: </b><span id="energyLabel">0.000</span> Kw/h
            <br>
            <b>Water Consumption: </b><span id="waterLabel">0.000</span> L
            <br>
            <b>Temperature: </b><span id="temperatureLabel">0.000</span> ºC
          </div>
        </div>
    </div>
</div>

<div class="page-header">
    <h5>Graph</h5>
</div>

<div class="row">
    <div class="col-sm-12 col-md-8">
        <div id="containerEnergy" style="min-width: 150px; height: 350px; margin: 0 auto"></div>
    </div>
    <div class="col-sm-12 col-md-4">
      <ul class="list-group" id="listitems">
          <li class="list-group-item">
              
              <table style="width: 100%; text-align: center;">
                  <tr>
                      <td width="15%"><b>Status</b></td>
                      <td width="15%"><b>Image</b></td>
                      <td width="20%"><b>Object</b></td>
                      <td width="20%"><b>Place</b></td>
                      <td width="30%"><b>Simulator Time</b></td>
                  </tr>
              </table>
              
          </li>
          <!--<li class="list-group-item">             
              <table style="width: 100%; text-align: center;">
                  <tr>
                      <td width="15%"><img src="Interface/on.png" width="30px"></td>
                      <td width="15%"><img src="Interface/DoorIcon.png" width="30px"></td>
                      <td width="20%">Door</td>
                      <td width="20%">Hall</td> 
                      <td width="30%">00:01:08</td> 
                  </tr>
              </table>           
          </li>-->
      </ul>
    </div>
</div>

  
</body>

<script type='text/javascript'>//<![CDATA[ 
    var dataString = "";
    var energy = 0;
    var water = 0;
    var temperature = 0;
    function getDataSring(){
        return dataString;
        
    }
    
    function getEnergy(){
        return energy;
    }
    
    function setEnergy(ene){
        energy = ene;
    }
    
    function getWater(){
        return water;
    }
    
    function setWater(wat){
        water = wat;
    }
    
    function getTemperature(){
        return temperature;
    }
    
    function setEnergy(tem){
        temperature = tem;
    }
    var antiloops = true;
    function updatePhysicalInformation(){
        var lastEnergy = getEnergy();
        var lastWater = getWater();
        var lastTemperature = getTemperature();
;        
        var res = dataString.split("##");
        
        if(dataString=="") return;
        
        var energy = res[1];
        var water = res[2];
        var temperature = res[3];
        var num_items = res[4];
        var name = res[5];
        var status = res[6];
        var iconpath = res[7];
        var place = res[8];
        var simulatortime = res[9]; 
        var sessionStatus = res[10]; 
        
        var incEnergy = 0.0;
        var incWater = 0.0;
        var incTemperature = 0.0;
        var text = "";
        if(energy){
            incEnergy = parseFloat(energy)-parseFloat(lastEnergy);
            //alert(incEnergy);
            text = energy;
            if(incEnergy>0){
                text == energy + "<span style='Color:#ff0000'>(+"+incEnergy+")</span>";
            }
            if(incEnergy<0){
                text == energy + "<span style='Color:#00ff00'>(-"+incEnergy+")</span>";
            }
            document.getElementById("energyLabel").innerHTML = text;
        }
        if(water){
            document.getElementById("waterLabel").innerHTML = water;
        }
        if(temperature){
            document.getElementById("temperatureLabel").innerHTML = temperature;
        }
        
        updateGeneralInformation(simulatortime,num_items);
        updatelistItems(name,status,place,iconpath,simulatortime);  
        
        if(sessionStatus=="true"){
            if(antiloops){
                location.href="realtime.php";
                antiloops = false;
            }
        }
             
    }
    function updateGeneralInformation(simulatortime,num_items){
        //update info
        document.getElementById("simulatortime").innerHTML = simulatortime;
        document.getElementById("num_items").innerHTML = num_items;
    }
    
    var lastSimulatorTime = "";
    function updatelistItems(name,status,place,iconpath,simulatortime){
        var listItems = document.getElementById("listitems");
        
        if(!name){
            return;
        }
        
        if(lastSimulatorTime == simulatortime){
            return;
        }else{
            lastSimulatorTime = simulatortime;
        }
        
        if(place=="Not Founded"){
            place = "-";
        }
        
        var statusIcon = "";
        
        if(status=="true"){
            statusIcon = "Interface/on.png";
        }else{
            statusIcon = "Interface/off.png";
        }    
        
        var html = '<li class="list-group-item">\n\
        <table style="width: 100%; text-align: center;">\n\
            <tr>\n\
                <td width="15%"><img src="'+statusIcon+'" width="30px"></td>\n\
                <td width="15%"><img src="'+iconpath+'" width="30px"></td>\n\
                <td width="20%">'+name+'</td>\n\
                <td width="20%">'+place+'</td>\n\
                <td width="30%">'+simulatortime+'</td>\n\
            </tr>\n\
        </table></li>';
        listItems.innerHTML = listItems.innerHTML + html;
    }
    
    function updateDateString(){
        
        var ec = $.ajax({
            url: 'realtime.php?getData=data&session=<?php echo $session;?>', //indicamos la ruta donde se genera la hora
            dataType: 'text',//indicamos que es de tipo texto plano
            async: true,
            success: function(data){
                dataString=data;        
            },//ponemos el parámetro asyn a falso
        }).responseText;
        
        updatePhysicalInformation();
    }
    updateDateString();
    setInterval(function(){updateDateString()},1000);
    
$(function () {
    $(document).ready(function() {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
    
        var chart;
        $('#containerEnergy').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function() {
                        // set up the updating of the chart each second
                        var energySeries = this.series[0];
                        var waterSeries = this.series[1];
                        var temperatureSeries = this.series[2];
                        setInterval(function() {
                            
                            var x = (new Date()).getTime(); // current time                                
                            
                            var ec = getDataSring();
                            
                            var res = ec.split("##");
                            
                            var energy = parseFloat(res[1]);
                            var water = parseFloat(res[2]);
                            var temperature = parseFloat(res[3]);
                            if(ec!=""){
                                energySeries.addPoint([x, energy], true, true);
                                waterSeries.addPoint([x, water], true, true);
                                temperatureSeries.addPoint([x, temperature], true, true);                            
                            }
                        }, 1000);
                    }
                }
            },
            title: {
                text: 'Live random data'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Energy Consumption',
                data: (function() {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;
    
                    for (i = -19; i <= 0; i++) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                })()
            },{
                name: 'Water Consumption',
                     data: (function() {
                         // generate an array of random data
                         var data = [],
                             time = (new Date()).getTime(),
                             i;

                         for (i = -19; i <= 0; i++) {
                             data.push({
                                 x: time + i * 1000,
                                 y: 0
                             });
                         }
                         return data;
                     })() 
            },{
                name: 'Temperature',
                     data: (function() {
                         // generate an array of random data
                         var data = [],
                             time = (new Date()).getTime(),
                             i;

                         for (i = -19; i <= 0; i++) {
                             data.push({
                                 x: time + i * 1000,
                                 y: 0
                             });
                         }
                         return data;
                     })()     
            }]
        });
    });
    
});
//]]>  

</script>




</html>