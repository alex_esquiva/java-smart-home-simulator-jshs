<html lang="en">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Java Smart Home Simulator</title>
  
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="style/css/bootstrap.css">

</head>
<body>
<!-- Latest compiled and minified JavaScript -->
<script type='text/javascript' src='style/js/jquery-1.9.1.js'></script>
<script type='text/javascript' src="style/js/bootstrap.js"></script>
<script type='text/javascript' src="style/js/highcharts.js"></script>
<script type='text/javascript' src="style/js/exporting.js"></script>

<ul class="nav nav-tabs">
  <li class="active"><a href="./">Home</a></li>
  <li><a href="realtime.php">Real Time</a></li>
</ul>
<?php
require_once "./inc/Functions.php";
//IF WE WANT THE INFORMATION IN TEXT PLANE
if(isset($_GET["action"])){

   if($_GET["action"]=="console"){
      $object = new Session();
      $rawdata = $object->getAllInfo();
      for($i = 0; $i<count($rawdata);$i++){
         echo "Session: ".$rawdata[$i][0]." Name: ".$rawdata[$i][1]." Time: ".$rawdata[$i][3]."\n";
      }
   }

}else{
    $object = new Session();
    
    $rawdata = $object->getAllInfo();
    $tool = new Tools();
    if(empty($rawdata)){
        $tool->displayError("Data Not Founded","There are not any session to show into the data base");
    }else{
        //$tool->displayStadisticInfo();
        $tool->displaySessionTable($rawdata);
    }
}

   

?>

    
</body>
</html>