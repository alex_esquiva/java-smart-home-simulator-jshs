<?php
class Tools{
	function connectDB(){
		$conexion = mysqli_connect("localhost", "root", "", "jshs");
		//$conexion = mysqli_connect("localhost", "root", "", "jshs");
		if($conexion){
			//echo 'La conexion de la base de datos se ha hecho satisfactoriamente<br>';
		}else{
                    $this->displayError("Data Base Error","Connection refused...");
		}		
		return $conexion;
	}
	function disconnectDB($conexion){
		$close = mysqli_close($conexion);
		if($close){
			//echo 'La desconexion de la base de datos se ha hecho satisfactoriamente<br>';
		}else{
			$this->displayError("Data Base Error","Disconnection refused...");
		}	
		return $close;
	}
        
        function getArraySQL($sql){
		//Creamos la conexión
		$conexion = $this->connectDB();
		//generamos la consulta
		if(!$result = mysqli_query($conexion, $sql)) die($this->displayError("Data Base Error",mysqli_error($conexion)));
		$rawdata = array();
		//guardamos en un array multidimensional todos los datos de la consulta
		$i=0;
		while($row = mysqli_fetch_array($result))
		{
		    $rawdata[$i] = $row;
		    $i++;
		}
		$this->disconnectDB($conexion);
		return $rawdata;
	}
        function displaySessionTable($rawdata){
               
            $scenaryObject = new ScenaryData;
            $actionObject = new ObjectData;

            for($i = 0;$i<count($rawdata);$i++){
               //add session to data
               $data[$i][0]=$rawdata[$i]["session"];
               //add Name session
                $data[$i][1] = $rawdata[$i]["name"];
               $actionObject->session=$data[$i][0];
               if($actionObject->getInfoBySession()==null){
                  $data[$i][2] = "No Data";
               }else{
                  $data[$i][2] = "<a href='showObjectData.php?session=".$data[$i][0]."'>Show Session Data</a>";
               }
               //add num_items
               $objectdata = new ObjectData();
               $objectdata->session = $data[$i][0];
               $data[$i][3]= $objectdata->getNumItems();
               //add Time
               $data[$i][4]=$rawdata[$i]["time"];
            }
            
            echo '<div class="page-header"></div>';
            
            echo '<div class="row">';
            echo '<div class="col-md-2">';
            
            echo '</div>';
            echo '<div class="col-md-8">';
            
            echo '<table class="table table-striped table-bordered table-condensed" style="text-align:center;">';
            echo '<th style="text-align:center;">Session</th><th style="text-align:center;">Name</th><th style="text-align:center;">Session Data</th><th style="text-align:center;">Iteractions</th><th style="text-align:center;">Time</th>';
            for($i = 0;$i<count($data);$i++){
         ?>
            <tr>
               <td><?php echo $data[$i][0]; ?></td>
               <td><?php echo $data[$i][1]; ?></td>
               <td><?php echo $data[$i][2]; ?></td>
               <td><?php echo $data[$i][3]; ?></td>
               <td><?php echo $data[$i][4]; ?></td>
            </tr>

         <?php
            }
            echo "</table>"; 
            echo '</div>';
            echo '<div class="col-md-2">';
            
            echo '</div>';
            echo '</div>';
        }
        
        function displayObjectsDataTable($rawdata){
            echo '<table class="table table-striped table-bordered table-condensed">';
            $columnas = count($rawdata[0])/2;
            //echo $columnas;
            $filas = count($rawdata);
            //echo "<br>".$filas."<br>";
            //Añadimos los titulos

            for($i=1;$i<count($rawdata[0]);$i=$i+2){
                    next($rawdata[0]);
                    if(key($rawdata[0]) == "session"){
                        echo "<th><b>Status</b></th>";
                    }else if(key($rawdata[0]) == "iconpath"){
                    }else if(key($rawdata[0]) == "code"){
                    }else if(key($rawdata[0]) == "status"){
                    }else{
                        echo "<th><b>".key($rawdata[0])."</b></th>";
                    }
                    next($rawdata[0]);
            }
            for($i=0;$i<$filas;$i++){
                    echo "<tr>";
                    for($j=0;$j<$columnas;$j++){
                            if($j==1){
                                echo "<td>"
                                    .$rawdata[$i][$j]
                                    ."<br><img width=100px height=100px src=".$rawdata[$i]["iconpath"]."></img>"
                                    ."</td>";  
                            }else if($j==0){
                                if($rawdata[$i]["status"]=="true"){
                                    $color="Interface/on.png";
                                }else{
                                    $color="Interface/off.png";
                                }          
                                echo "<td><img width=100px src='".$color."'></img></td>";  
                            }else if($j==2){
                            }else if($j==7){
                            }else if($j==9){
                            }else{
                                echo "<td>".$rawdata[$i][$j]."</td>";  
                            }
                    }
                    echo "</tr>";
            }		
            echo '</table>';
        }
        function displayError($title,$message){
            ?>
            <div class="row">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                    <div class="page-header">
                        <h1><?php echo $title; ?></h1>
                    </div>
                    <div class="alert alert-info">
                        <?php echo $message; ?>
                    </div>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
            <?php
            die();
        }
}

?>