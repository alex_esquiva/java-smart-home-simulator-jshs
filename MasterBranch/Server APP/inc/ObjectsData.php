<?php
require_once("Functions.php");

class ObjectData{

	public $session=0;

	function create($session,$name,$code,$energy,$water,$temperature,$place,$status,$iconpath,$sim_time){
		$connect = new Tools();
		$conexion = $connect->connectDB();
		$sql = "insert into objectsdata (session,name,code,energy,water,temperature,place,status,iconpath,sim_time) 
		values (".$session.",'".$name."',".$code.",'".$energy."','".$water."','".$temperature."','".$place."','".$status."','".$iconpath."','".$sim_time."')";
		
		$consulta = mysqli_query($conexion,$sql);
	
		if($consulta){
                    echo 'New Object Data added';
                }else{
                    echo "Error<br><br>".mysqli_error($conexion);
		}		
		$connect->disconnectDB($conexion);

		return $consulta;

	}

	function getAllInfo(){
		//Creamos la consulta
		$sql = "SELECT * FROM objectsdata ORDER BY time DESC;";
		//obtenemos el array
		return $this->getArraySQL($sql);
	}
    
	function getInfoBySession(){		
		//Creamos la consulta
		$sql = "SELECT * FROM objectsdata where session = ".$this->session." ORDER BY time;";
		//obtenemos el array
		return $this->getArraySQL($sql);
	}
        
        function getLastInfo(){
            //Creamos la consulta
            $sql = "SELECT * FROM objectsdata WHERE session = $this->session ORDER BY time DESC limit 0,1;";
            //obtenemos el array
            $rawdata =  $this->getArraySQL($sql);
            return $rawdata;
        }
        
        function getNumItems(){
            $connect = new Tools();
            $conexion = $connect->connectDB();
            $sql = "SELECT * FROM objectsdata WHERE session = $this->session;";
            $result = mysqli_query($conexion,$sql);
            $numero = mysqli_num_rows($result);
            $connect->disconnectDB($conexion);
            return $numero;
        }


	function getFilterInfo(){
                //Get info SESSION
                $info = $this->getInfoBySession();
                $rawdata = "";
                for($i=0;$i<count($info);$i++){
                    $rawdata[$i]["Session"]=$this->session;
                    
                    //getName
                    $object = new Object();
                    $object->code = $info[$i]["code"];
                    $name = $object->getName();
                    
                    
                    $rawdata[$i]["Simulator Time"]=$info[$i]["sim_time"];
                    //echo $rawdata[$i][2]=$info[$i]["sim_time"];
                    
                    $rawdata[$i]["Name"]=$name;
                    //$rawdata[$i][1]=$name;
                   
                    $rawdata[$i]["Code"]=$info[$i]["code"];;
                   
                    $rawdata[$i]["Energy Consumption"]=$info[$i]["energy"];
                    //echo $rawdata[$i][3]=$info[$i]["energy"];
                   
                    $rawdata[$i]["Water Consumption"]=$info[$i]["water"];
                    //$rawdata[$i][4]=$info[$i]["water"];
                 
                    $rawdata[$i]["Temperature"]=$info[$i]["temperature"];
                    //$rawdata[$i][5]=$info[$i]["temperature"];
                 
                    $rawdata[$i]["Place"]=$info[$i]["place"];
                    //$rawdata[$i][6]=$info[$i]["time"];
                 
                    $rawdata[$i]["Global Time"]=$info[$i]["time"];
                    //$rawdata[$i][6]=$info[$i]["time"];
                 
                    $rawdata[$i]["Status"]=$info[$i]["status"];
                    //$rawdata[$i][7]=$info[$i]["status"];
                    
                }

		return $rawdata;
	}
        
        function getEnergyConsumptionInfo(){
            //Creamos la consulta
            $sql = "SELECT energy,time FROM objectsdata where session = ".$this->session.";";
            //obtenemos el array
            return $this->getArraySQL($sql);
        }
        function getLastEnergyConsumptionInfo(){
            //Creamos la consulta
            $sql = "SELECT energy FROM objectsdata ORDER BY time DESC limit 0,1;";
            //obtenemos el array
            $rawdata =  $this->getArraySQL($sql);
            return $rawdata[0][0];
        }
        function getWaterConsumptionInfo(){
            //Creamos la consulta
            $sql = "SELECT water,time FROM objectsdata where session = ".$this->session.";";
            //obtenemos el array
            return $this->getArraySQL($sql);
        }
        function getLastWaterConsumptionInfo(){
            //Creamos la consulta
            $sql = "SELECT water FROM objectsdata ORDER BY time DESC limit 0,1;";
            //obtenemos el array
            $rawdata =  $this->getArraySQL($sql);
            return $rawdata[0][0];
        }
        function getTemperatureInfo(){
            //Creamos la consulta
            $sql = "SELECT temperature,time FROM objectsdata where session = ".$this->session.";";
            //obtenemos el array
            return $this->getArraySQL($sql);
        }
        function getLastTemperatureInfo(){
            //Creamos la consulta
            $sql = "SELECT temperature FROM objectsdata ORDER BY time DESC limit 0,1;";
            //obtenemos el array
            $rawdata =  $this->getArraySQL($sql);
            return $rawdata[0][0];
        }

	function getArraySQL($sql){
                $tools = new Tools();
		return $tools->getArraySQL($sql);
	}
        function processJSON($data){
            for($i = 0; $i<count($data);$i++){
		$this->create($data[$i]->session, $data[$i]->name, $data[$i]->code, $data[$i]->energy, $data[$i]->water, $data[$i]->temperature, $data[$i]->place, $data[$i]->status, $data[$i]->iconpath, $data[$i]->sim_time);    
            }
        }
}

