<?php
require_once("Functions.php");

class Session{

	public $session=0;
        
	function create($session,$name){
		$connect = new Tools();
		$conexion = $connect->connectDB();
		$sql = "insert into sessions (session,name) 
		values (".$session.",'".$name."')";	
		$consulta = mysqli_query($conexion,$sql);
		if($consulta){
			echo 'New Session Added\n';
		}else{
			echo "Error: ".mysqli_error($conexion)."\n\n";
		}		
		$connect->disconnectDB($conexion);
		return $consulta;
	}

	function getAllInfo(){
            //Creamos la consulta
            $sql = "SELECT * FROM sessions ORDER BY session DESC;";
            //obtenemos el array
            return $this->getArraySQL($sql);
	}
        
        function getAllInfoBySession(){
            //Creamos la consulta
            $sql = "SELECT * FROM sessions WHERE session = $this->session;";
            //obtenemos el array
            return $this->getArraySQL($sql);
        }

	function getLastSession(){
            //Creamos la consulta
            $sql = "SELECT session FROM sessions ORDER BY session DESC LIMIT 0,1;";
            //obtenemos el array
            $array = $this->getArraySQL($sql);
            
            if(empty($array[0][0])){
                return "0";
            }else{
                return $array[0][0];
            }
	}

	function existSession(){
            //Creamos la consulta
            $sql = "SELECT session FROM sessions where session = ".$this->session.";";
            //obtenemos el array
            return $this->getArraySQL($sql);
	}
        
        function isClosedSession(){
            $exist = $this->existSession();
            if(empty($exist[0][0])){
                return false;
            }else{
                //Creamos la consulta
                $sql = "SELECT closed FROM sessions where session = ".$this->session.";";
                //obtenemos el array
                $array = $this->getArraySQL($sql);
                if($array[0][0]==0){
                    return false;
                }else{
                    return true;
                }
            }
        }

	function getArraySQL($sql){
            $tools = new Tools();
            return $tools->getArraySQL($sql);
            
	}
        
        function processJSON($data){
            for($i = 0; $i<count($data);$i++){
		$this->create($data[$i]->session, $data[$i]->name);    
            }
        }
        
        function closeSession($session){
            //Creamos la consulta
            $connect = new Tools();
            $conexion = $connect->connectDB();
            $sql = "Update sessions SET closed = 1 where session = $session;";
            $consulta = mysqli_query($conexion,$sql);
            if($consulta){
                    echo "Session $session Closed\n";
            }else{
                    echo "Error: ".mysqli_error($conexion)."\n\n";
            }		
            $connect->disconnectDB($conexion);
            return $consulta;
        }
}