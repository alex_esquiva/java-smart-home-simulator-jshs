<?php
require_once("Functions.php");

class Object{
        public $code=0;
        
	function create($name,$code,$speed,$energyOn,$energyOff,$waterOn,$waterOff,$temperatureOn,$temperatureOff){
		$connect = new Tools();
		$conexion = $connect->connectDB();
                
                if($this->existByName($name)){
                    //Update                    
                }else{
                    $sql = "insert into objects (name,code,speed,energyOn,energyOff,waterOn,waterOff,temperatureOn,temperatureOff) 
                    values ('".$name."','".$code."','".$speed."','".$energyOn."','".$energyOff."','".$waterOn."','".$waterOff."','".$temperatureOn."','".$temperatureOff."')";	
                    $consulta = mysqli_query($conexion,$sql);
                    if($consulta){
                            echo 'New Object Added\n';
                    }else{
                            echo "Error: ".mysqli_error($conexion)."\n\n";
                    }		
                    $connect->disconnectDB($conexion);
                    return $consulta;
                }                
	}

	function getAllInfo(){
		//Creamos la consulta
		$sql = "SELECT * FROM objects ORDER BY code DESC;";
		//obtenemos el array
		return $this->getArraySQL($sql);
	}
        
        function getName(){
            //Creamos la consulta
            $sql = "SELECT name FROM objects where code=".$this->code.";";
            //obtenemos el array
            $rawdata = $this->getArraySQL($sql);
            if(empty($rawdata[0][0])){
                return "Not Found";
            }else{
                return $rawdata[0][0];
            }
        }


	function getArraySQL($sql){
            $tools = new Tools();
            return $tools->getArraySQL($sql);
	}
        
        function existByName($name){
            //Creamos la consulta
            $sql = "SELECT name FROM objects where name='".$name."';";
            //obtenemos el array
            $rawdata = $this->getArraySQL($sql);
            if(empty($rawdata[0][0])){
                return false;
            }else{
                return true;
            }
        }
        
        function processJSON($data){
            for($i = 0; $i<count($data);$i++){
		$this->create($data[$i]->name, $data[$i]->code, $data[$i]->speed, $data[$i]->energyOn, $data[$i]->energyOff, $data[$i]->waterOn, $data[$i]->waterOff, $data[$i]->temperatureOn, $data[$i]->temperatureOff);    
                //echo $data[$i]->name;
            }
        }
}