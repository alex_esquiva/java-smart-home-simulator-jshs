<?php


require_once("Functions.php");

class ScenaryData{

	public $session=0;

	function create($session,$code,$position,$rotation,$status){
		$connect = new Tools();
		$conexion = $connect->connectDB();
		$sql = "insert into scenarydata (session,code,position,rotation,status) 
		values (".$session.",".$code.",'".$position."','".$rotation."','".$status."')";		
		$consulta = mysqli_query($conexion,$sql);	
		if($consulta){
			echo 'New Scenary Data Added\n';
		}else{
			echo "Error: ".mysqli_error($conexion)."\n\n";
		}		
		$connect->disconnectDB($conexion);
		return $consulta;

	}

	function getAllInfo(){
		//Creamos la consulta
		$sql = "SELECT * FROM scenarydata ORDER BY time DESC;";
		//obtenemos el array
		return $this->getArraySQL($sql);
	}

	function getSessionInfo(){
		
		//Creamos la consulta
		$sql = "SELECT code,position,rotation,status FROM scenarydata where session = ".$this->session.";";
		//obtenemos el array
		return $this->getArraySQL($sql);
	}

	function existSession(){
		//Creamos la consulta
		$sql = "SELECT session FROM scenarydata where session = ".$this->session.";";
		//obtenemos el array
		return $this->getArraySQL($sql);

	}

	function getArraySQL($sql){
            $tools = new Tools();
            return $tools->getArraySQL($sql);
	}
        
        function processJSON($data){
            for($i = 0; $i<count($data);$i++){
		$this->create($data[$i]->session, $data[$i]->code, $data[$i]->position, $data[$i]->rotation, $data[$i]->status);    
            }
        }
}