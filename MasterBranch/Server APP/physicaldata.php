<html lang="en">

<head>

<meta charset="utf-8">   
<title>JAVA Smart Home Simulator</title>   
<meta name="description" content="Creating a Zebra table with Twitter Bootstrap. Learn with example of a Zebra Table with Twitter Bootstrap.">  
<link href="style/bootstrap.css" rel="stylesheet"> 
 <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<?php

//1º Show Energy Consumption graph an table
//2º Show Water Consumption graph an table
//3º Show Temperature Consumption graph an table

require_once("inc/Functions.php");
if(isset($_GET["session"])){
	$object = new ObjectData();

	//get Session
	$session = $_GET["session"];
	$object->session = $session;
	//getEconsumptionData
	$eConsumption = $object->getEnergyConsumptionInfo();
	//getWconsumptionData
	$wConsumption = $object->getWaterConsumptionInfo();
	//getTdata
	$tData = $object->getTemperatureInfo();

	echo "<div id='econsumption'></div>";
	drawTable($eConsumption);
	echo "<div id='wconsumption'></div>";
	drawTable($wConsumption);
	echo "<div id='tdata'></div>";
	drawTable($tData);

	  /*$object->session = $_GET["session"];
	  $rawdata = $object->getSessionInfo();
	  drawTable($rawdata);*/
}else{
    $object = new ScenaryData;
    $rawdata = $object->getAllInfo();
    drawTable($rawdata);
}

function drawTable($rawdata){
   //DIBUJAMOS LA TABLA
   
   if(empty($rawdata)) return;
   
   echo '<table class="table table-striped table-bordered table-condensed">';
   $columnas = count($rawdata[0])/2;
   //echo $columnas;
   $filas = count($rawdata);
   //echo "<br>".$filas."<br>";

   //Añadimos los titulos

      
   for($i=1;$i<count($rawdata[0]);$i=$i+2){
      next($rawdata[0]);
      echo "<th><b>".key($rawdata[0])."</b></th>";
      next($rawdata[0]);
   }

   for($i=0;$i<$filas;$i++){

      echo "<tr>";
      for($j=0;$j<$columnas;$j++){
         echo "<td>".$rawdata[$i][$j]."</td>";
         
      }
      echo "</tr>";
   }
      
   echo '</table>';
}
?>
<script src="https://code.jquery.com/jquery.js"></script>
    <!-- Importo el archivo Javascript de Highcharts directamente desde su servidor -->
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>

$(function () {
        $('#econsumption').highcharts({
    chart: {
        type: 'area'
        //defaultSeriesType: 'spline'
        
    },
    rangeSelector : {
        enabled: false
    },
    title: {
        text: 'Energy Consumption'
    },
    xAxis: {
        type: 'datetime'
        //tickPixelInterval: 150,
        //maxZoom: 20 * 1000
    },
    yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
            text: 'Energy [KW/H]',
            margin: 10
        }
    },
    series: [{
        name: 'value',
        data: (function() {
                // generate an array of random data
                var data = [];
                <?php
                    for($i = 0 ;$i<count($eConsumption);$i++){
                    	$time = $eConsumption[$i][1];
                    	$date = new DateTime($time);
                    	$eConsumption[$i][1]=$date->getTimestamp()*1000;
                ?>
                data.push([<?php echo $eConsumption[$i][1];?>,<?php echo $eConsumption[$i][0];?>]);
                <?php } ?>
                return data;
            })()
    }],
    credits: {
            enabled: false
    }
    });
});

$(function () {
        $('#wconsumption').highcharts({
    chart: {
        type: 'area'
        //defaultSeriesType: 'spline'
        
    },
    rangeSelector : {
        enabled: false
    },
    title: {
        text: 'Water Consumption'
    },
    xAxis: {
        type: 'datetime'
        //tickPixelInterval: 150,
        //maxZoom: 20 * 1000
    },
    yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
            text: 'Water [L]',
            margin: 10
        }
    },
    series: [{
        name: 'value',
        data: (function() {
                // generate an array of random data
                var data = [];
                <?php
                    for($i = 0 ;$i<count($wConsumption);$i++){
                    	$time = $wConsumption[$i][1];
                    	$date = new DateTime($time);
                    	$wConsumption[$i][1]=$date->getTimestamp()*1000;
                ?>
                data.push([<?php echo $wConsumption[$i][1];?>,<?php echo $wConsumption[$i][0];?>]);
                <?php } ?>
                return data;
            })()
    }],
    credits: {
            enabled: false
    }
    });
});

$(function () {
        $('#tdata').highcharts({
    chart: {
        type: 'area'
        //defaultSeriesType: 'spline'
        
    },
    rangeSelector : {
        enabled: false
    },
    title: {
        text: 'Temperature'
    },
    xAxis: {
        type: 'datetime'
        //tickPixelInterval: 150,
        //maxZoom: 20 * 1000
    },
    yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
            text: 'Temperature [ºC]',
            margin: 10
        }
    },
    series: [{
        name: 'value',
        data: (function() {
                // generate an array of random data
                var data = [];
                <?php
                    for($i = 0 ;$i<count($tData);$i++){
                    	$time = $tData[$i][1];
                    	$date = new DateTime($time);
                    	$tData[$i][1]=$date->getTimestamp()*1000;
                ?>
                data.push([<?php echo $tData[$i][1];?>,<?php echo $tData[$i][0];?>]);
                <?php } ?>
                return data;
            })()
    }],
    credits: {
            enabled: false
    }
    });
});

</script>  
</body>
</html>