<?php

require_once("inc/Functions.php");
if(isset($_POST["action"])){
    
    $action = $_POST["action"];
    
    $json = $_POST["json"];
    $json = urldecode($json);
    $json = str_replace("\\", "",$json);
    $json = json_decode($json);
    
    if($action == "Insert_Objects"){
        $object = new Object();
        $object->processJson($json);
    }elseif ($action == "Insert_ObjectsData" ) {
        $object = new ObjectData();
        $object->processJson($json);
    }elseif ($action == "Insert_ScenaryData" ) {
        $object = new ScenaryData();
        $object->processJson($json);
    }elseif ($action == "Insert_Sessions" ) {
        $object = new Session();
        $object->processJson($json);
    }elseif ($action == "Close_Session" ) {
        $object = new Session();
        $object->closeSession($json[0]->session);
    }
}
