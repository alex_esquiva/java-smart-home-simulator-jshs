package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.Quaternion;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.CartoonEdgeFilter;
import com.jme3.renderer.Caps;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Sphere;
import com.jme3.ui.Picture;
import Doors.DoorControl;
import Doors.InitDoors;
import GUI.MyGUI;
import com.jme3.niftygui.NiftyJmeDisplay;
import Doors.FocusDoor;
import Fan.InitFan;
import control.FocusShoot;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Console;
import de.lessvoid.nifty.controls.ConsoleCommands;
import de.lessvoid.nifty.tools.Color;
import java.util.Vector;


public class Main extends SimpleApplication
        implements ActionListener{

  private Node staticScene;
  private BulletAppState bulletAppState;
  private RigidBodyControl landscape;
  private CharacterControl player;
  private Vector3f walkDirection = new Vector3f();
  private boolean left = false, right = false, up = false, down = false;

  
  
  private Geometry mark;
  
  private MyGUI gui;
  HUD hud = new HUD();
  BitmapText hudText;
  Nifty nifty;
  FocusDoor focusDoor;
  private boolean console = false;

  public static void main(String[] args) {
    Main app = new Main();
     //some presets
    AppSettings settings = new AppSettings(true);
    settings.setResolution(1366,768);
    //settings.setResolution(800,600);
    settings.setFrameRate(60);
    settings.setFullscreen(true);
    settings.setVSync(true);
    app.setSettings(settings);
    //app.setShowSettings(false);
    app.setPauseOnLostFocus(true);
    app.start();
  }

  public void simpleInitApp() {   
    //don't show stats
    setDisplayFps(false);
    setDisplayStatView(false);

    /** Set up Physics */
    bulletAppState = new BulletAppState();
    bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);//for bullets
    stateManager.attach(bulletAppState);
    //bulletAppState.getPhysicsSpace().enableDebug(assetManager);
    // We re-use the flyby camera for rotation, while positioning is handled by physics
    viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
    flyCam.setMoveSpeed(100);
    
    //init Doors
    new InitDoors(rootNode, assetManager, bulletAppState);
    //init Fan
    new InitFan(rootNode, assetManager, bulletAppState);
    focusDoor = new FocusDoor(rootNode,rootNode,cam,assetManager);
    //init
    //Init All
    
    initScene();
    setUpKeys();
    setUpLight();
    //setUpSky();
    //setCartoonEffect();
    initMark();
    initHUD();
    
  }
  
  public void initHUD(){
/*
    hud = new HUD();

    //PIC
    Picture pic = new Picture("HUD Picture");
    pic.setImage(assetManager, "Textures/hudFrame.png", true);
    pic.setWidth(settings.getWidth());
    pic.setHeight(settings.getHeight()/8);
    pic.setPosition(0,settings.getHeight()-settings.getHeight()/8);
    guiNode.attachChild(pic);
    //Show HUD

    hudText = new BitmapText(guiFont, false);   
    hudText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
    hudText.setSize(settings.getHeight()/45);
    hudText.setColor(ColorRGBA.Green);                             // font color
    hudText.setLocalTranslation(settings.getWidth()/45,settings.getHeight()-settings.getHeight()/45, 0); // 
    guiNode.attachChild(hudText);
    
    //Puntero
    BitmapText hudText2 = new BitmapText(guiFont, false);   
    //hudText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
    hudText2.setSize(settings.getHeight()/45);
    hudText2.setText("*");
    hudText2.setColor(ColorRGBA.Green);                             // font color
    hudText2.setLocalTranslation(settings.getWidth()/2-5,settings.getHeight()/2+10, 0); // 
    guiNode.attachChild(hudText2);
   */
    
    NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,inputManager,audioRenderer,guiViewPort);
    nifty = niftyDisplay.getNifty();
        
    //initGui
    gui = new MyGUI(this,player);
    nifty.fromXml("Interface/mygui.xml", "start",gui);

    // attach the nifty display to the gui view port as a processor
    guiViewPort.addProcessor(niftyDisplay);
    guiViewPort.setEnabled(true);
    //***DEBUG MODE
    //nifty.setDebugOptionPanelColors(true);
    //***Hide Console
    gui.hideConsole(true);


  }
  
  public void initScene(){  
    //Preload Levels
    staticScene = (Node) assetManager.loadModel("Scenes/newSceneTest.j3o");
    staticScene.scale(5f);      
    
    //Set up physics and add level/player to scenegraph
    setLevel(staticScene);    
  }
  
  private void setLevel(Spatial level) {
     // We set up collision detection for the scene by creating a
    // compound collision shape and a static RigidBodyControl with mass zero.
    CollisionShape sceneShape =
            CollisionShapeFactory.createMeshShape((Node) level);

    landscape = new RigidBodyControl(sceneShape, 0);  
    level.addControl(landscape);

    // We set up collision detection for the player by creating
    // a capsule collision shape and a CharacterControl.
    // The CharacterControl offers extra settings for
    // size, stepheight, jumping, falling, and gravity.
    // We also put the player in its starting position.
    CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1f, 8f, 1);
    player = new CharacterControl(capsuleShape, 0.05f);
    player.setJumpSpeed(20);
    player.setFallSpeed(30);
    player.setGravity(30);
    player.setPhysicsLocation(new Vector3f(0,10,10));
    // We attach the scene and the player to the rootnode and the physics space,
    // to make them appear in the game world.
    rootNode.attachChild(level);
    bulletAppState.getPhysicsSpace().add(landscape);
    bulletAppState.getPhysicsSpace().add(player); 
    
    
  }
  
  private PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
  }
  
  private void setUpLight() {
    // We add light so we see the scene
    AmbientLight al = new AmbientLight();
    al.setColor(ColorRGBA.White.mult(0.1f));
    rootNode.addLight(al);

    DirectionalLight dl = new DirectionalLight();
    dl.setColor(ColorRGBA.White);
    dl.setDirection(new Vector3f(1.5f, -2, 2).normalizeLocal());
    rootNode.addLight(dl);
  }

  /** We over-write some navigational key mappings here, so we can
   * add physics-controlled walking and jumping: */
  private void setUpKeys() {
    inputManager.deleteMapping(INPUT_MAPPING_EXIT);
    inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
    inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
    inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
    inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
    inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
    inputManager.addMapping("Action", new KeyTrigger(KeyInput.KEY_E));
    inputManager.addMapping("Quit", new KeyTrigger(KeyInput.KEY_ESCAPE));
    inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
    inputManager.addMapping("Console", new KeyTrigger(KeyInput.KEY_C));
    
    inputManager.addListener(this, "Left");
    inputManager.addListener(this, "Right");
    inputManager.addListener(this, "Up");
    inputManager.addListener(this, "Down");
    inputManager.addListener(this, "Jump");
    inputManager.addListener(actionListener,"Action");
    inputManager.addListener(this, "Quit"); // listen for ESC key to close game
    inputManager.addListener(actionListener, "Shoot");
    inputManager.addListener(actionListener,"Console");
  }
  
  /*private void setUpSky() {
  
        Texture west = assetManager.loadTexture("Textures/Sky/Lagoon/west.jpg");
        Texture east = assetManager.loadTexture("Textures/Sky/Lagoon/east.jpg");
        Texture north = assetManager.loadTexture("Textures/Sky/Lagoon/north.jpg");
        Texture south = assetManager.loadTexture("Textures/Sky/Lagoon/south.jpg");
        Texture up = assetManager.loadTexture("Textures/Sky/Lagoon/up.jpg");
        Texture down = assetManager.loadTexture("Textures/Sky/Lagoon/down.jpg");

        Spatial sky = SkyFactory.createSky(assetManager, west, east, north, south, up, down);
        rootNode.attachChild(sky);
  
  }*/
  
  protected void initMark() {
    Sphere sphere = new Sphere(30, 30, 0.2f);
    mark = new Geometry("BOOM!", sphere);
    Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    mark_mat.setColor("Color", ColorRGBA.Red);
    mark.setMaterial(mark_mat);
  }

  /** These are our custom actions triggered by key presses.
   * We do not walk yet, we just keep track of the direction the user pressed. */
  public void onAction(String binding, boolean value, float tpf) {
        
    hud.setToken(binding);
    
    //If console is on, don't use this
    if(!console){
        if (binding.equals("Left")) {
          if (value) { left = true; } else { left = false; }
        } else if (binding.equals("Right")) {
          if (value) { right = true; } else { right = false; }
        } else if (binding.equals("Up")) {
          if (value) { up = true; } else { up = false; }
        } else if (binding.equals("Down")) {
          if (value) { down = true; } else { down = false; }
        } else if (binding.equals("Jump")) {
          player.jump();
        } else if (binding.equals("Quit")) {
          stop(); // simple way to close game
        } 
    }
  }
  
  private ActionListener actionListener = new ActionListener() {
    public void onAction(String name, boolean keyPressed, float tpf) {
        if (name.equals("Action") && !keyPressed) {
            focusDoor.tryActionDoor();
        }
      
        if (name.equals("Shoot") && !keyPressed) {

            FocusShoot f = new FocusShoot(rootNode,rootNode,cam,assetManager);
            f.tryShoot();
       }
       if (name.equals("Console") && !keyPressed) { 
           
           //Hide console
           gui.hideConsole(console);
           //Set Drag to Rotate
           flyCam.setDragToRotate(!console);
           //Set Player Enable
           player.setEnabled(console);
           //Change state
           console = !console;
           
       }
    }
  };
  
  /**
   * This is the main event loop--walking happens here.
   * We check in which direction the player is walking by interpreting
   * the camera direction forward (camDir) and to the side (camLeft).
   * The setWalkDirection() command is what lets a physics-controlled player walk.
   * We also make sure here that the camera moves with player.
   */
  @Override
  public void simpleUpdate(float tpf) {
    
        Vector3f camDir = cam.getDirection().clone().multLocal(0.6f);
        Vector3f camLeft = cam.getLeft().clone().multLocal(0.4f);
        walkDirection.set(0, 0, 0);
        if (left)  { walkDirection.addLocal(camLeft); }
        if (right) { walkDirection.addLocal(camLeft.negate()); }
        if (up)    { walkDirection.addLocal(camDir); }
        if (down)  { walkDirection.addLocal(camDir.negate()); }
        player.setWalkDirection(walkDirection);
        cam.setLocation(player.getPhysicsLocation());  
        
        gui.updatePosition(player.getPhysicsLocation());
        gui.update(tpf);
        //Insert position to HUD
        //hud.setPosition(player.getPhysicsLocation());
        //Insert Door State 
        //hud.setDoorState(Door1.getControl(DoorControl.class).getAction());
        //HUD
        //hudText.setText(hud.getInfo());
        
        
  }
  
  private void setCartoonEffect() {
        /**
         * are minimum requirements for cel shader met?
         */
        if (renderer.getCaps().contains(Caps.GLSL100)) {
            FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
            viewPort.addProcessor(fpp);

            CartoonEdgeFilter toon = new CartoonEdgeFilter();
            toon.setEdgeColor(ColorRGBA.Black);
            fpp.addFilter(toon);
        }
    }
  
  

};