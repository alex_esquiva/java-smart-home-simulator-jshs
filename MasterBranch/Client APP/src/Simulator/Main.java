package Simulator;

import Control.BuildMode;
import Control.LightControl;
import Control.PhysicalCharacteristics;
import DataBase.ObjectsDB;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.system.AppSettings;
import GUI.MyGUI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Alejandro Esquiva Rodríguez
 */
public class Main extends SimpleApplication{

  private BulletAppState bulletAppState;
  private CharacterControl player;
  private InitApp initApp;
  private boolean init = true;
  
  public AppSettings settings;
  
  private MyGUI gui;
  private ObjectsDB db;
  private BuildMode buildMode;
  private PhysicalCharacteristics physicalCharacteristics;
  private MyConfig myConfig;
  private LightControl myLightControl;
  /**
   * 
   * @param args 
   */
  public static void main(String[] args) {
    Main app = new Main();
     //some presets
    AppSettings settings = new AppSettings(true);
    settings.setTitle("Smart Home Simulator");
    settings.setSettingsDialogImage("Interface/portada.png");
    settings.setResolution(MyConfig.RESOLUTION_WIDTH,MyConfig.RESOLUTION_HEIGHT);
    settings.setFrameRate(MyConfig.FRAMERATE);
    settings.setFullscreen(true);
    settings.setVSync(true);
    settings.setStereo3D(false);

    //ICONO settings.setIcons(args);
    app.setSettings(settings);
    //app.setShowSettings(false);
    app.setPauseOnLostFocus(true);
    app.start();
  }
/**
 * Init App
 */
  public void simpleInitApp() {   
    //don't show stats
    setDisplayFps(false);
    setDisplayStatView(false);
    
    bulletAppState = null;
    myConfig = new MyConfig(this);
    db = new ObjectsDB(this);
    try {
          //Init Scenary
          initApp = new InitApp(this);
          
    } catch (Exception ex) {
          Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }
    buildMode = new BuildMode(this);
    setPhysicalCharacteristics(new PhysicalCharacteristics());
    
    //Zoom out
    flyCam.setZoomSpeed(0);
    //Day Control
    setMyLightControl(new LightControl(this));
  }
  
  /**
   * This is the main event loop--walking happens here.
   * We check in which direction the player is walking by interpreting
   * the camera direction forward (camDir) and to the side (camLeft).
   * The setWalkDirection() command is what lets a physics-controlled player walk.
   * We also make sure here that the camera moves with player.
   */
  @Override
  public void simpleUpdate(float tpf) {
        
        if(init){
            initApp.initScenary();
            init = !init;
        }
        
        initApp.update(tpf);        
        gui.getMyInfoPanel().updatePosition(player.getPhysicsLocation());
        gui.update(tpf);
        
        //Physical Characteristics
        gui.getMyInfoPanel().updateEnergyLabel(physicalCharacteristics.getEnergyConsumption());
        gui.getMyInfoPanel().updateWaterLabel(physicalCharacteristics.getTotalWater());
        gui.getMyInfoPanel().updateTemperatureLabel(physicalCharacteristics.getTemperature());
        
        //SmartLights
        //myLightControl.smartLights();
  }
  
    /**
     * @return the gui
     */
    public MyGUI getGui() {
        return gui;
    }

    /**
     * @param gui the gui to set
     */
    public void setGui(MyGUI gui) {
        this.gui = gui;
    }

    /**
     * @return the player
     */
    public CharacterControl getPlayer() {
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(CharacterControl player) {
        this.player = player;
    }

    /**
     * @return the bulletAppState
     */
    public BulletAppState getBulletAppState() {
        return bulletAppState;
    }

    /**
     * @param bulletAppState the bulletAppState to set
     */
    public void setBulletAppState(BulletAppState bulletAppState) {
        this.bulletAppState = bulletAppState;
        this.bulletAppState.startPhysics();
    }

    /**
     * @return the db
     */
    public ObjectsDB getDb() {
        return db;
    }

    /**
     * @param db the db to set
     */
    public void setDb(ObjectsDB db) {
        this.db = db;
    }

    /**
     * @return the buildMode
     */
    public BuildMode getBuildMode() {
        return buildMode;
    }

    /**
     * @param buildMode the buildMode to set
     */
    public void setBuildMode(BuildMode buildMode) {
        this.buildMode = buildMode;
    }

    /**
     * @return the physicalCharacteristics
     */
    public PhysicalCharacteristics getPhysicalCharacteristics() {
        return physicalCharacteristics;
    }

    /**
     * @param physicalCharacteristics the physicalCharacteristics to set
     */
    public void setPhysicalCharacteristics(PhysicalCharacteristics physicalCharacteristics) {
        this.physicalCharacteristics = physicalCharacteristics;
    }

    /**
     * @return the initApp
     */
    public InitApp getInitApp() {
        return initApp;
    }

    /**
     * @param initApp the initApp to set
     */
    public void setInitApp(InitApp initApp) {
        this.initApp = initApp;
    }

    /**
     * @return the myConfig
     */
    public MyConfig getMyConfig() {
        return myConfig;
    }

    /**
     * @param myConfig the myConfig to set
     */
    public void setMyConfig(MyConfig myConfig) {
        this.myConfig = myConfig;
    }
    
    
    public void exitSimulator() {
            //Close session
            this.getDb().closeSession();
            //Exit 
            System.exit(1);
    }

    /**
     * @return the myDayControl
     */
    public LightControl getMyLightControl() {
        return myLightControl;
    }

    /**
     * @param myDayControl the myDayControl to set
     */
    public void setMyLightControl(LightControl myLightControl) {
        this.myLightControl = myLightControl;
    }
    
  
};