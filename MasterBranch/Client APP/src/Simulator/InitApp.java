/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulator;


import GUI.MyGUI;
import ObjectSource.BathroomSet;
import ObjectSource.BathroomSink;
import ObjectSource.Bed;
import ObjectSource.Computer;
import ObjectSource.Dishwasher;
import ObjectSource.Door;
import ObjectSource.DoorExterior;
import ObjectSource.Extractor;
import ObjectSource.Fan;
import ObjectSource.Fridge;
import ObjectSource.Grill;
import ObjectSource.House;
import ObjectSource.Jacuzzi;
import ObjectSource.KitchenSink;
import ObjectSource.LightBasic;
import ObjectSource.LightFloorTraditional;
import ObjectSource.MainDoor;
import ObjectSource.Microwave;
import ObjectSource.MyTerrain;
import ObjectSource.Oven;
import ObjectSource.Radiator;
import ObjectSource.Shower;
import ObjectSource.SmokeStack;
import ObjectSource.Switch;
import ObjectSource.Tv;
import ObjectSource.WC;
import ObjectSource.WashingMachine;
import ObjectSource.WindowBig;
import ObjectSource.WindowLarge;
import ObjectSource.WindowLargeV2;
import ObjectSource.WindowMedium;
import ObjectSource.WindowSmall;


import com.jme3.bullet.BulletAppState;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FXAAFilter;
import com.jme3.renderer.Caps;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import de.lessvoid.nifty.Nifty;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author IdeaPad Z500
 */
public class InitApp {
    
    private Main myMain;
    private KeysInput myKeys;
    private Player myPlayer;
    private Nifty nifty;
    
    public InitApp(Main app){
        myMain = app;
        
        //Set Up Physics
        setUpPhysics();
        //Set Up Light
        //setUpLight();
        //Set Up Sky
        //setUpSky();
        //Set Up Cartoon Filter
        setFilter();    
        //Set Up Player
        myPlayer = new Player(myMain);
        //Set Up Keys
        myKeys = new KeysInput(myMain);
        //Init GUI
        initGUI();
        
        myMain.getPlayer().setEnabled(true);
    }
    
   public void initScenary(){
        //Init Terrain
        new MyTerrain(myMain);
        //Init House
        new House(myMain);
                
        //Main Door
        new MainDoor(myMain, new Vector3f(6.420265f, 1.2527373f, -49.938473f));
        //Exterior Door
        new DoorExterior(myMain, new Vector3f(-21.62867f, 1.252738f, -130.10611f),(float)Math.PI);
        new DoorExterior(myMain, new Vector3f(3.6566815f, 1.252737f, -118.47507f),(float)Math.PI);
        new DoorExterior(myMain, new Vector3f(15.989758f, 1.252737f, -118.47507f),(float)Math.PI);

        //Living Room
        new Fan(myMain,new Vector3f(-17.380367f, 16.262413f, -62.44456f));
        new SmokeStack(myMain,new Vector3f(-28.635077f, 1.2767801f, -67.37068f));
        new Tv(myMain, new Vector3f(-24.835608f, 10.6232815f, -62.56837f),(float) (Math.PI/2));
        new LightBasic(myMain,new Vector3f(1.5718163f, 16.262413f, -62.99157f));
        new LightBasic(myMain,new Vector3f(-17.099356f, 16.262413f, -87.16169f));
        new LightBasic(myMain,new Vector3f(-17.099356f, 16.262413f, -53.33947f));
        new LightBasic(myMain,new Vector3f(-17.099356f, 16.262413f, -72.10208f));
        new Switch(myMain, new Vector3f(4.972931f, 6f, -74.5f));
        
        //Kitchen
        new Microwave(myMain,new Vector3f(-24.02696f, 8.224608f, -100.1f),(float) Math.PI);
        new Fridge(myMain,new Vector3f(-14.427944f, 1.2529268f, -100.0f), (float) Math.PI);
        new Dishwasher(myMain,new Vector3f(-25.628168f, 1.3f, -111.0508f),(float)Math.PI/2);
        new Oven(myMain,new Vector3f(-15.054392f, 1.252738f, -116.00536f),(float)Math.PI/2);
        new Extractor(myMain, new Vector3f(-16.220095f, 16.262413f, -116.04702f));
        new LightBasic(myMain,new Vector3f(-23.325853f, 16.262411f, -121.00247f));
        new LightBasic(myMain,new Vector3f(-8.81063f, 16.262413f, -121.00247f));
        new LightBasic(myMain,new Vector3f(-8.81063f, 16.262413f, -108.01088f));
        new LightBasic(myMain,new Vector3f(-21.529366f, 16.262413f, -108.01088f));
        new Switch(myMain, new Vector3f(2.7591228f, 6f, -105.18019f),-(float) Math.PI/2);
        
        //Stairs
        new Fan(myMain,new Vector3f(10.01255f, 16.262413f, -97.62993f));
        new LightBasic(myMain,new Vector3f(11f, 16.262413f, -111.78179f));
        new LightBasic(myMain,new Vector3f(11f, 16.262413f, -88.84073f));
        new Switch(myMain, new Vector3f(13.1599865f, 6f, -83.02133f),(float) (Math.PI));
        
        //Garage
        new WashingMachine(myMain, new Vector3f(36.552364f, 0.0f, -75.11001f), (float) (Math.PI/2));
        new WashingMachine(myMain, new Vector3f(33.207855f, 0.0f, -75.11001f), (float) (Math.PI/2));
        new LightBasic(myMain,new Vector3f(39.425865f, 16.262413f, -67.044876f));
        new LightBasic(myMain,new Vector3f(39.425865f, 16.262413f, -55.183075f));
        new LightBasic(myMain,new Vector3f(17.336212f, 16.262413f, -55.183075f));
        new LightBasic(myMain,new Vector3f(17.336212f, 16.262413f, -71.156006f));
        new Switch(myMain, new Vector3f(23.413378f, 6f, -74.46675f));
        
        //Office
        new Computer(myMain, new Vector3f(28.750141f, 5.0627375f, -90.91125f));
        new LightBasic(myMain,new Vector3f(27.988934f, 16.262413f, -87.12791f));
        new LightBasic(myMain,new Vector3f(27.988934f, 16.262413f, -96.71665f));
        new Switch(myMain, new Vector3f(25.229906f, 6f, -100.05809f));
        
        //Hall
        new LightBasic(myMain,new Vector3f(30.018967f, 32.515045f, -83.48861f));
        new LightBasic(myMain,new Vector3f(14.262221f, 32.515045f, -83.48861f));
        new LightBasic(myMain,new Vector3f(-2.191134f, 32.515045f, -83.48861f));
        new Switch(myMain, new Vector3f(36.61492f, 22.5f, -77.64149f),(float) -(Math.PI/2));
        
        
        //Bedroom 1
        new Bed(myMain, new Vector3f(36.568146f, 17.51282f, -54.536606f),(float) -(Math.PI/2));
        new LightFloorTraditional(myMain, new Vector3f(33.651047f, 17.51282f, -50.737022f));
        new LightFloorTraditional(myMain, new Vector3f(33.651047f, 17.51282f, -67.23081f));
        new LightBasic(myMain,new Vector3f(32.88927f, 32.515045f, -51.070198f));
        new LightBasic(myMain,new Vector3f(32.88927f, 32.515045f, -69.42519f));
        new LightBasic(myMain,new Vector3f(13.177101f, 32.515045f, -69.42519f));
        new LightBasic(myMain,new Vector3f(13.177101f, 32.515045f, -54.47091f));
        new Switch(myMain, new Vector3f(36.61492f, 22.5f, -69.50713f),(float) -(Math.PI/2));
        
        //Bedroom 2
        new Bed(myMain, new Vector3f(-15.336046f, 17.51282f, -50.583973f),(float) (Math.PI));
        new LightFloorTraditional(myMain, new Vector3f(-18.069529f, 17.51282f, -52.25926f));
        new LightFloorTraditional(myMain, new Vector3f(-2.1221824f, 17.51282f, -52.25926f));
        new LightBasic(myMain,new Vector3f(-1.4545784f, 32.515045f, -54.26462f));
        new LightBasic(myMain,new Vector3f(-1.4545784f, 32.515045f, -69.89664f));
        new LightBasic(myMain,new Vector3f(-25.276207f, 32.515045f, -69.89664f));
        new LightBasic(myMain,new Vector3f(-25.276207f, 32.515045f, -54.910297f));
        new Switch(myMain, new Vector3f(7.1149216f, 22.712772f, -66.58654f),(float) -(Math.PI/2));
        
        //Bedroom 3
        new Bed(myMain, new Vector3f(-28.54214f, 17.51282f, -118.74892f),(float) (Math.PI/2));
        new LightFloorTraditional(myMain, new Vector3f(-27.054066f, 17.51282f, -121.66025f));
        new LightFloorTraditional(myMain, new Vector3f(-27.054066f, 17.51282f, -105.00984f));
        new Tv(myMain, new Vector3f(-5.4153013f, 25.195984f, -113.671455f),(float) -(Math.PI/2));
        new LightBasic(myMain,new Vector3f(-18.489809f, 32.515045f, -97.514885f));
        new LightBasic(myMain,new Vector3f(-18.489809f, 32.515045f, -115.437614f));
        new Switch(myMain, new Vector3f(-7.6809297f, 22.5f, -92.17615f),(float) (Math.PI));
        
        //Bedroom 4
        new Bed(myMain, new Vector3f(36.39524f, 17.51282f, -101.85285f),(float) -(Math.PI/2));
        new LightFloorTraditional(myMain, new Vector3f(34.10196f, 17.51282f, -115.05637f));
        new LightFloorTraditional(myMain, new Vector3f(34.10196f, 17.51282f, -99.00824f));
        new Tv(myMain, new Vector3f(21.256409f, 26.347082f, -108.335175f),(float) (Math.PI/2));
        new LightBasic(myMain,new Vector3f(12.173557f, 32.515045f, -97.07346f));
        new LightBasic(myMain,new Vector3f(28.428408f, 32.515045f, -97.07346f));
        new LightBasic(myMain,new Vector3f(28.428408f, 32.515045f, -111.419174f));
        new Switch(myMain, new Vector3f(14.960852f, 22.5f, -100.48691f));
        
        
        //Bathroom 1
        new BathroomSink(myMain, new Vector3f(36.8015f, 1.2754984f, -102.3983f),(float) -(Math.PI/2));
        new WC(myMain, new Vector3f(30.281776f, 1.2527375f, -112.000435f),(float) (Math.PI/2));
        new LightBasic(myMain,new Vector3f(25.91852f, 16.262413f, -104.44996f));
        new LightBasic(myMain,new Vector3f(31.754206f, 16.262413f, -110.75044f));
        new Switch(myMain, new Vector3f(24.358543f, 6f, -107.097305f));
        
        //Bathroom 2
        new BathroomSink(myMain, new Vector3f(-12.297412f, 17.557518f, -91.51077f));
        new WC(myMain, new Vector3f(-22.246609f, 17.51282f, -87.75601f));
        new BathroomSet(myMain, new Vector3f(-16.077763f, 17.51282f, -79.5f));
        new LightBasic(myMain,new Vector3f(-12.579557f, 32.515045f, -83.5544f));
        new LightBasic(myMain,new Vector3f(-22.586143f, 32.515045f, -83.5544f));
        new Switch(myMain, new Vector3f(-12.197496f, 22.5f, -79.064f),(float) (Math.PI));
        
        //Bathroom 3
        new Shower(myMain, new Vector3f(-2.2f, 17.51282f, -110.2514f));
        new WC(myMain, new Vector3f(13.989824f, 17.51282f, -106.175224f),(float) (Math.PI));
        new BathroomSink(myMain, new Vector3f(6.453492f, 17.634937f, -102.626854f),(float) (Math.PI));
        new LightBasic(myMain,new Vector3f(0.70341635f, 32.515045f, -106.67742f));
        new LightBasic(myMain,new Vector3f(13.608517f, 32.515045f, -107.82676f));
        new Switch(myMain, new Vector3f(-3.328078f, 22.5f, -106.45914f),(float) (Math.PI/2));
                
        //All Doors
        new Door(myMain,new Vector3f(7.343978f, 1.252738f, -70.064095f), (float) (-Math.PI/2));
        new Door(myMain,new Vector3f(19.45429f, 1.2527375f, -95.846375f), (float) (-Math.PI/2));
        new Door(myMain,new Vector3f(19.459911f, 1.252738f, -102.88278f), (float) (-Math.PI/2));
        new Door(myMain,new Vector3f(30.798883f, 17.51282f, -74.79138f), (float) (Math.PI));
        new Door(myMain,new Vector3f(17.856438f, 17.51282f, -91.912865f), 0);
        new Door(myMain,new Vector3f(-0.16817284f, 17.51282f, -91.91083f), 0);
        new Door(myMain,new Vector3f(0.72031164f, 17.51282f, -101.391495f),0);
        new Door(myMain,new Vector3f(-7.6019964f, 17.51282f, -85.22244f), (float) (Math.PI/2));
        new Door(myMain,new Vector3f(-4.3044634f, 17.51282f, -74.786736f), (float) (Math.PI));
        
        //All Radiators
        new Radiator(myMain, new Vector3f(-17.170813f, 1.3865843f, -50.5f));
        new Radiator(myMain, new Vector3f(14.885437f, 1.2527375f, -83.03914f));
        
        //**180º
        new Radiator(myMain, new Vector3f(-15.888623f, 1.2716937f, -99.5f),(float) Math.PI);
        new Radiator(myMain, new Vector3f(17.952873f, 17.51282f, -74.43765f),(float) Math.PI);
        new Radiator(myMain, new Vector3f(15.335106f, 17.51282f, -100.35448f),(float) Math.PI);
        new Radiator(myMain, new Vector3f(-16.766773f, 17.51909f, -129.83813f),(float) Math.PI);
        new Radiator(myMain, new Vector3f(-20.841898f, 17.51282f, -74.450134f),(float) Math.PI);
        
        //**90º
        new Radiator(myMain, new Vector3f(2.756958f, 1.252738f, -81.32811f),(float) Math.PI/2);
        new Radiator(myMain, new Vector3f(36.61492f, 17.525751f, -83.4435f),(float) Math.PI/2);
        
        //**270º
        new Radiator(myMain, new Vector3f(-28.635078f, 17.542023f, -83.72161f),(float) -Math.PI/2);
        new Radiator(myMain, new Vector3f(19.774874f, 1.2651653f, -88.7545f),(float) -Math.PI/2);
        
        
        //All Windows
        new WindowSmall(myMain,new Vector3f(-1.356408f, 10.64505f, -50.045567f));
        new WindowSmall(myMain,new Vector3f(-2.2761033f, 26.917238f, -50.061672f));
        new WindowSmall(myMain,new Vector3f(-9.76389f, 26.919756f, -50.037464f));
        new WindowSmall(myMain,new Vector3f(-17.26622f, 26.912273f, -50.01643f));
        //Init Windows Big
        new WindowBig(myMain,new Vector3f(-28.658642f, 22.574377f, -53.68325f),-(float) Math.PI/2);
        new WindowBig(myMain,new Vector3f(-28.658642f, 22.569096f, -67.90725f),-(float) Math.PI/2);
        new WindowBig(myMain,new Vector3f(-28.658642f, 22.547447f, -82.17346f),-(float) Math.PI/2);
        //new WindowBig(myMain,new Vector3f(36.51912f, 5.7793627f, -93.086105f),(float) Math.PI/2);
        //new WindowBig(myMain,new Vector3f(36.51912f, 22.259918f, -85.03314f),(float) Math.PI/2);
        //Init Windows Medium
        new WindowMedium(myMain, new Vector3f(28.960012f, 23.535732f, -45.43676f));
        new WindowMedium(myMain, new Vector3f(17.104406f, 23.535732f, -50.422707f));
        new WindowMedium(myMain, new Vector3f(12.605443f, 23.535732f, -50.419086f));
        
        //Init Windows LargeV2
        new WindowLargeV2(myMain, new Vector3f(-18.977863f, 6.1687107f, -50.31107f));
        new WindowLargeV2(myMain, new Vector3f(-10.3428755f, 6.168711f, -50.30703f));
        new WindowLargeV2(myMain, new Vector3f(-14.669184f, 6.1687107f, -50.316227f));
        //Init Windows Large
        new WindowLarge(myMain,new Vector3f(-28.64079f, 5.7793617f, -80.98726f),-(float) Math.PI/2);
        new WindowLarge(myMain,new Vector3f(-23.14991f, 22.28556f, -129.84694f),(float) Math.PI);
        new WindowLarge(myMain,new Vector3f(21.835266f, 22.044245f, -118.12547f),(float) Math.PI);
        
        //Update Labels
        myMain.getGui().getMyInfoPanel().updateEnergyLabel(myMain.getPhysicalCharacteristics().getEnergyConsumption());
        myMain.getGui().getMyInfoPanel().updateWaterLabel(myMain.getPhysicalCharacteristics().getWaterConsumption());
        myMain.getGui().getMyInfoPanel().updateTemperatureLabel(myMain.getPhysicalCharacteristics().getTemperature());
        
        try {
                //Send Objects Info to our Data base
                myMain.getDb().sendObjectsInfo();
        } catch (Exception ex) {
                Logger.getLogger(InitApp.class.getName()).log(Level.SEVERE, null, ex);
        }
   }

    void update(float tpf) {
        myKeys.update(tpf);
        getMyPlayer().update(tpf);
    }

    private void setUpPhysics() {
        /** Set up Physics */
        BulletAppState bulletAppState = new BulletAppState();
        bulletAppState.setThreadingType(BulletAppState.ThreadingType.PARALLEL);//for bullets
        myMain.getStateManager().attach(bulletAppState);
        //bulletAppState.getPhysicsSpace().enableDebug(assetManager);
        // We re-use the flyby camera for rotation, while positioning is handled by physics
        myMain.getViewPort().setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        myMain.getFlyByCamera().setMoveSpeed(100);
        myMain.setBulletAppState(bulletAppState);
    }
    
      private void setFilter() {
        /**
         * are minimum requirements for cel shader met?
         */
        if (myMain.getRenderer().getCaps().contains(Caps.GLSL100)) {
            FilterPostProcessor fpp = new FilterPostProcessor(myMain.getAssetManager());
            myMain.getViewPort().addProcessor(fpp);
            
            FXAAFilter filter = new FXAAFilter();
            //fpp.addFilter(toon);
            fpp.addFilter(filter);
            
        }
    }
      
    private void setUpLight() {
        /*// We add light so we see the scene
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(0.1f));
        myMain.getRootNode().addLight(al);

        DirectionalLight dl = new DirectionalLight();
        dl.setColor(ColorRGBA.White);
        dl.setDirection(new Vector3f(1.5f, -2, 2).normalizeLocal());
        myMain.getRootNode().addLight(dl);
                */
        float intensity = 0.6f;
        try{
            intensity = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.GENERAL_XML_PATH, "INIT_LIGHT_INTENSITY"));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(intensity));
        myMain.getRootNode().addLight(al);

        DirectionalLight dl1 = new DirectionalLight();
        dl1.setDirection(new Vector3f(1, -1, 1).normalizeLocal());
        //dl1.setColor(new ColorRGBA(0.965f, 0.949f, 0.772f, 1f).mult(0.7f));
        dl1.setColor(ColorRGBA.White.mult(intensity));
        myMain.getRootNode().addLight(dl1);

        DirectionalLight dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-1, -1, -1).normalizeLocal());
        //dl.setColor(new ColorRGBA(0.965f, 0.949f, 0.772f, 1f).mult(0.7f));
        dl.setColor(ColorRGBA.White.mult(intensity));
        myMain.getRootNode().addLight(dl);
    }
  
    private void setUpSky() {
        Texture west = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_west.jpg");
        Texture east = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_east.jpg");
        Texture north = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_north.jpg");
        Texture south = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_south.jpg");
        Texture up = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_up.jpg");
        Texture down = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_down.jpg");

        Spatial sky = SkyFactory.createSky(myMain.getAssetManager(), west, east, north, south, up, down);
        myMain.getRootNode().attachChild(sky);
  
    }
    
    public void initGUI(){
    
    NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(myMain.getAssetManager(),myMain.getInputManager(),myMain.getAudioRenderer(),myMain.getGuiViewPort());
    nifty = niftyDisplay.getNifty();
        
    //initGui
    MyGUI gui = new MyGUI(myMain);
    nifty.fromXml(MyConfig.GUI_PATH, "start",gui);
    // attach the nifty display to the gui view port as a processor
    myMain.getGuiViewPort().addProcessor(niftyDisplay);
    myMain.getGuiViewPort().setEnabled(true);
    
    //***DEBUG MODE
    //nifty.setDebugOptionPanelColors(true);
    //***Hide Console
    gui.getMyConsole().hideConsole();
    myMain.setGui(gui);
  }

    /**
     * @return the myPlayer
     */
    public Player getMyPlayer() {
        return myPlayer;
    }

    /**
     * @param myPlayer the myPlayer to set
     */
    public void setMyPlayer(Player myPlayer) {
        this.myPlayer = myPlayer;
    }
    
    
}
