/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulator;

import Control.ObjectControl;
import Control.SpacePosition;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;

/**
 *
 * @author IdeaPad Z500
 */
public class Player {
    private CharacterControl player;
    Main myMain;
    String name="",path="";
    private String place = "";
    public Player(Main app) {
        myMain = app;
        // We set up collision detection for the player by creating
        // a capsule collision shape and a CharacterControl.
        // The CharacterControl offers extra settings for
        // size, stepheight, jumping, falling, and gravity.
        // We also put the player in its starting position.
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1f, 7f, 1);
        player = new CharacterControl(capsuleShape, 1f);
        try{
            player.setJumpSpeed(Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.PLAYER_XML_PATH, "PLAYER_JUMSPEED")));
            player.setFallSpeed(Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.PLAYER_XML_PATH, "PLAYER_FALLSPEED")));
            player.setGravity(Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.PLAYER_XML_PATH, "PLAYER_GRAVITY")));
            float px = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.PLAYER_XML_PATH, "INIT_PLAYER_POSITION_X"));
            float py = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.PLAYER_XML_PATH, "INIT_PLAYER_POSITION_Y"));
            float pz = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.PLAYER_XML_PATH, "INIT_PLAYER_POSITION_Z"));
            player.setPhysicsLocation(new Vector3f(px,py,pz));
        }catch(Exception e){}
        // We attach the scene and the player to the rootnode and the physics space,
        // to make them appear in the game world.
        app.getBulletAppState().getPhysicsSpace().add(player);
        app.setPlayer(player);
        
    }
    
    public void update(float tpf){
        CollisionResults results = new CollisionResults();
        Ray ray = new Ray(myMain.getCamera().getLocation(), myMain.getCamera().getDirection());
        myMain.getRootNode().collideWith(ray, results);
        float distance=100;
        if (results.size() > 0) {
          CollisionResult closest = results.getClosestCollision();
          distance = myMain.getPlayer().getPhysicsLocation().distance(closest.getContactPoint());
        }
        
        try{
            if(distance<10){   
                String name2 = results.getCollision(0).getGeometry().getParent().getName();
                if(name!=name2){
                    name = name2;
                    path = myMain.getRootNode().getChild(name).getControl(ObjectControl.class).getLogoPath();
                    myMain.getGui().getMyInfoPanel().switchImages(path, true);
                }
            }else{
                name="";
                myMain.getGui().getMyInfoPanel().switchImages("", false);
            }
        }catch(NullPointerException e){}
        
        //Put Place
        setPlace(new SpacePosition(myMain).getNameSpace(player.getPhysicsLocation()));
    }
    
    

    /**
     * @return the player
     */
    public CharacterControl getPlayer() {
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(CharacterControl player) {
        this.player = player;
    }
    
    public void enablePlayer(boolean state){
        if(state){
            //SET PLAYER MOVEMENT ON
            //Set Drag to Rotate
            myMain.getFlyByCamera().setDragToRotate(false);
            //Set Player Enable
            myMain.getPlayer().setEnabled(true);
        }else{
            //SET PLAYER MOVEMENT OFF
            //Set Drag to Rotate
            myMain.getFlyByCamera().setDragToRotate(true);
            //Set Player Enable
            myMain.getPlayer().setEnabled(false);
            
        }
    }

    /**
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place the place to set
     */
    public void setPlace(String place) {
        this.place = place;
    }
    
}
