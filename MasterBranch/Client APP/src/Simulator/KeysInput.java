/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulator;

import static com.jme3.app.SimpleApplication.INPUT_MAPPING_EXIT;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Vector3f;
import Control.FocusAction;
import ObjectSource.Sphere;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author IdeaPad Z500
 */
public class KeysInput{
    Main myMain;
    private boolean left = false, right = false, up = false, down = false;
    private Vector3f walkDirection = new Vector3f();
    FocusAction focusAction;

    public KeysInput(Main app){
        myMain = app;
        setUpKeys();
        focusAction = new FocusAction(app);
    }
    
    private void setUpKeys() {
        myMain.getInputManager().deleteMapping(INPUT_MAPPING_EXIT);
        myMain.getInputManager().addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        myMain.getInputManager().addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        myMain.getInputManager().addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        myMain.getInputManager().addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        myMain.getInputManager().addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        myMain.getInputManager().addMapping("Menu", new KeyTrigger(KeyInput.KEY_ESCAPE));
        myMain.getInputManager().addMapping("Mouse_Left", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        myMain.getInputManager().addMapping("Mouse_Right", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        myMain.getInputManager().addMapping("Scroll_Button", new MouseButtonTrigger(MouseInput.BUTTON_MIDDLE));
        myMain.getInputManager().addMapping("Console", new KeyTrigger(KeyInput.KEY_T));
        myMain.getInputManager().addMapping("Rotate", new KeyTrigger(KeyInput.KEY_R));
        myMain.getInputManager().addMapping("Info", new KeyTrigger(KeyInput.KEY_O));
        
        myMain.getInputManager().addListener(actionListener, "Left");
        myMain.getInputManager().addListener(actionListener, "Right");
        myMain.getInputManager().addListener(actionListener, "Up");
        myMain.getInputManager().addListener(actionListener, "Down");
        myMain.getInputManager().addListener(actionListener, "Jump");
        myMain.getInputManager().addListener(actionListener, "Menu");
        myMain.getInputManager().addListener(actionListener, "Mouse_Left");
        myMain.getInputManager().addListener(actionListener,"Mouse_Right");
        myMain.getInputManager().addListener(actionListener,"Scroll_Button");
        myMain.getInputManager().addListener(actionListener,"Console");
        myMain.getInputManager().addListener(actionListener,"Rotate");
        myMain.getInputManager().addListener(actionListener,"Info");


    }
    
  
  private ActionListener actionListener = new ActionListener() {
    public void onAction(String name, boolean keyPressed, float tpf) {
       

        if (name.equals("Left")) {
          if (keyPressed) { left = true; } else { left = false; }
        } else if (name.equals("Right")) {
          if (keyPressed) { right = true; } else { right = false; }
        } else if (name.equals("Up")) {
          if (keyPressed) { up = true; } else { up = false; }
        } else if (name.equals("Down")) {
          if (keyPressed) { down = true; } else { down = false; }
        } else if (name.equals("Jump")) {
          myMain.getPlayer().jump();
        }

        if (name.equals("Info")  && keyPressed) {        
                //myMain.getInitApp().getMyPlayer().enablePlayer(false);
        }

        if(myMain.getBuildMode().isStatus()){
            
            //***BUILD MODE
            if (name.equals("Mouse_Right")  && keyPressed) {        
                myMain.getBuildMode().deleteObject();
            }
            if (name.equals("Scroll_Button") && keyPressed) {
                myMain.getBuildMode().switchObject();
            }
            if (name.equals("Mouse_Left") && keyPressed) {
                myMain.getBuildMode().createobject();
            }
            if (name.equals("Rotate") && keyPressed) {
                myMain.getBuildMode().rotateObject();
            }
        }else{
            //*** Normal Mode
            if (name.equals("Mouse_Right")  && keyPressed) {                    
                
                focusAction.tryInfo();
                System.gc();
                
            }
            if (name.equals("Mouse_Left") && keyPressed) {
                focusAction.tryAction();
                System.gc();
            }
        }
         
        if (name.equals("Console") && !keyPressed) { 
            //myMain.getMyDayControl().setDay();
            try {
                if(!myMain.getGui().getStatusGUI()){
                    myMain.getGui().showBlock("console");                        
                }else{
                    //myMain.getGui().hideGUI();                   
                }
            } catch (Exception ex) {
                Logger.getLogger(KeysInput.class.getName()).log(Level.SEVERE, null, ex);
            }
            myMain.getGui().setStatusGUI(!myMain.getGui().getStatusGUI());
        }
        if (name.equals("Rotate") && !keyPressed) {
            
                // 1. Reset results list.
                CollisionResults results = new CollisionResults();
                // 2. Aim the ray from cam loc to cam direction.
                Ray ray = new Ray(myMain.getCamera().getLocation(), myMain.getCamera().getDirection());
                // 3. Collect intersections between Ray and Shootables in results list.
                myMain.getRootNode().collideWith(ray, results);
                // 4. Print the results
                //System.out.println("----- Collisions? " + results.size() + "-----");
                // 5. Use the results (we mark the hit object)
                if (results.size() > 0) {
                   try{       
                       /*
                        Sphere s = new Sphere(16, 16, 1);
                        Geometry geom = new Geometry("Box", s);
                        Material mat = new Material(myMain.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
                        mat.setColor("Color", ColorRGBA.Blue);
                        geom.setMaterial(mat);
                        geom.setLocalTranslation(results.getCollision(0).getContactPoint());
                        myMain.getRootNode().attachChild(geom);*/
                       new Sphere(myMain, results.getCollision(0).getContactPoint());
                       System.out.println("Speher: "+results.getCollision(0).getContactPoint());
                    }catch(NullPointerException e){}
                } 
                
        }
        if (name.equals("Menu") && !keyPressed) {
            //Disabled Player
            myMain.getInitApp().getMyPlayer().enablePlayer(false);
            try {
               if(!myMain.getGui().getMyMainMenu().isStatusMainMenu()){
                   myMain.getGui().showBlock("MainMenu");                        
                   myMain.getGui().setStatusGUI(!myMain.getGui().getStatusGUI());        
               }else{

                    if(myMain.getGui().getMyControlPanel().isStatusControlPanel()){
                        //Close Control Panel
                        myMain.getGui().getMyControlPanel().cancelConfig();
                    }else if(myMain.getGui().getMyGraphGui().isStatusGraphOptionPanel()){                  
                        if(myMain.getGui().getMyGraphGui().isStatusGraphPanel()){
                            //Close Graph Panel
                            myMain.getGui().getMyGraphGui().acceptGraphPanel();
                        }else{
                            //Close Graph Option Panel
                            myMain.getGui().getMyGraphGui().cancelGraphOptionPanel();
                        }
                    }else if(myMain.getGui().getMyActionPanel().isStatusActionPanel()){
                        //Close Action Panel
                        myMain.getGui().getMyActionPanel().cancelActionPanel();
                    }else{
                         myMain.getGui().getMyMainMenu().closeMainMenu();                 
                         myMain.getGui().setStatusGUI(!myMain.getGui().getStatusGUI());
                         //Enabled Player
                         myMain.getInitApp().getMyPlayer().enablePlayer(true);
                    }
               }
           } catch (Exception ex) {
               Logger.getLogger(KeysInput.class.getName()).log(Level.SEVERE, null, ex);
           }
        }
    }
    
  };
  
  
  public void update(float tpf){
          
        Vector3f camDir = myMain.getCamera().getDirection().clone().multLocal(0.6f);
        Vector3f camLeft = myMain.getCamera().getLeft().clone().multLocal(0.4f);
        walkDirection.set(0, 0, 0);
        if (left)  { walkDirection.addLocal(camLeft); }
        if (right) { walkDirection.addLocal(camLeft.negate()); }
        if (up)    { walkDirection.addLocal(camDir); }
        if (down)  { walkDirection.addLocal(camDir.negate()); }
        myMain.getPlayer().setWalkDirection(walkDirection);
        myMain.getCamera().setLocation(myMain.getPlayer().getPhysicsLocation().add(0, 2f, 0));
        
        myMain.getBuildMode().update(tpf);
        
  }
  
  


    
}

