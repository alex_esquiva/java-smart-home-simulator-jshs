/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulator;

import Control.ObjectControl;
import XML.MyConfigXMLDefault;
import XML.MyXML;
import com.jme3.math.Vector3f;
import com.jme3.system.AppSettings;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author IdeaPad Z500
 */
public class MyConfig {
    Main myMain;
    
    public static final String PLAYER_XML_PATH = "player.xml";
    public static final String GENERAL_XML_PATH = "general.xml";
    //**Screen Config
    public static final int RESOLUTION_WIDTH = 1366;
    public static final int RESOLUTION_HEIGHT = 768;
    public static final int FRAMERATE = 60;
    //**Game Config
    public static final float SCALE = 5f;   
    //**GUI Config
    public static final String GUI_PATH = "Interface/mygui.xml";
    //**Data Base Config
    public static final String SERVER_PATH = "http://javasmarthomesimulator.tk"; 
    //public static final String SERVER_PATH = "http://localhost/JSHS"; 
    //**Build Mode [ALPHA]
    public static int NUM_OBJECT = 11;   
    
    //**Init Temperature
    public static final float INIT_TEMPERATURE = 18.0f;
    //**Generate Physical Variables xml 
    ArrayList<String> physicalObject = new ArrayList<String>();
    ArrayList<String> physicalKey = new ArrayList<String>();
    ArrayList<String> physicalValue = new ArrayList<String>();
    
    private MyXML xmlObject;
    private MyConfigXMLDefault myConfigXMLDefault;
    
    public MyConfig(Main main){
        myMain = main;
        xmlObject = new MyXML(main);
        myConfigXMLDefault = new MyConfigXMLDefault(main);
        myConfigXMLDefault.generateXMLDefault();
    }
    
    public ArrayList<String> getGeneralConfig() throws Exception{
        
        return getXmlObject().readXML("general.xml");
    }

    public ArrayList<String> getPlayerConfig() throws Exception{
        
        return getXmlObject().readXML("player.xml");
    }
    public ArrayList<String> getPhysicObjectsConfig() throws Exception{
        
        return getXmlObject().readObjectPhysicXML();
    }
    
    public ArrayList<String> getPhysicKeysConfig() throws Exception{
        
        return getXmlObject().readKeysPhysicXML();
    }
    //just for player and general xml
    public String findValueXML(String Name, String Key) throws Exception{
        return getXmlObject().findValueXML(Name, Key);
    }
    
    public String findPhysicValueXML(String Object, String Key) throws Exception{
        return getXmlObject().findPhysicValueXML(Object,Key);
    }
    
    public void ModifyXML(String Name, String Key, String NewValue) throws Exception{
        getXmlObject().modifyXML(Name, Key, NewValue);
    }
    
    public void ModifyPhysicXML(String Object, String Key, String NewValue) throws Exception{
        getXmlObject().modifyPhysicXML(Object, Key, NewValue);
    }
    
    
    public void updatePhysicalVariables(){
        for(int i = 0;i<myMain.getRootNode().getChildren().size();i++){
            
            String name = myMain.getRootNode().getChildren().get(i).getName();
            try{
                myMain.getRootNode().getChild(name).getControl(ObjectControl.class).updatePhysicalVariables();
            }catch(Exception e){
                
                myMain.getGui().getConsole().output("ERROR: "+name);
            }
        }
    }

    public void updatePlayerVariables() throws Exception{
        String xmlPath = MyConfig.PLAYER_XML_PATH;
        float jumpSpeed = Float.parseFloat(xmlObject.findValueXML(xmlPath, "PLAYER_JUMSPEED"));
        myMain.getPlayer().setJumpSpeed(jumpSpeed);
        float fallSpeed = Float.parseFloat(xmlObject.findValueXML(xmlPath, "PLAYER_FALLSPEED"));
        myMain.getPlayer().setFallSpeed(fallSpeed);
        float gravity = Float.parseFloat(xmlObject.findValueXML(xmlPath, "PLAYER_GRAVITY"));
        myMain.getPlayer().setGravity(gravity);
        float px = Float.parseFloat(xmlObject.findValueXML(xmlPath, "PLAYER_POSITION_X"));
        float py = Float.parseFloat(xmlObject.findValueXML(xmlPath, "PLAYER_POSITION_Y"));
        float pz = Float.parseFloat(xmlObject.findValueXML(xmlPath, "PLAYER_POSITION_Z"));
        myMain.getPlayer().setPhysicsLocation(new Vector3f(px,py,pz));
    }

    public void updateGeneralVariables() throws Exception{
        String xmlPath = MyConfig.GENERAL_XML_PATH;
        float oldScale = MyConfig.SCALE;
        float scale = Float.parseFloat(xmlObject.findValueXML(xmlPath, "SCALE"));
        if(oldScale!=scale){
            for(int i = 0;i<myMain.getRootNode().getChildren().size();i++){
            
                String name = myMain.getRootNode().getChildren().get(i).getName();
                try{
                    myMain.getRootNode().getChild(name).getControl(ObjectControl.class).getMyspatial().setLocalScale(scale);
                }catch(Exception e){
                    e.printStackTrace();
                    myMain.getGui().getConsole().output("ERROR "+name);
                }
            }
        }
        
        float internetConnection = Float.parseFloat(xmlObject.findValueXML(xmlPath, "INTERNET_CONNECTION"));
        if(internetConnection==1){
            myMain.getDb().setInternetConnection(true);
        }else{
            myMain.getDb().setInternetConnection(false);
        }
        
        float sendData = Float.parseFloat(xmlObject.findValueXML(xmlPath, "SEND_DATA"));
        if(sendData==1){
            myMain.getDb().setSenData(true);
        }else{
            myMain.getDb().setSenData(false);
        }
        
        float bm = Float.parseFloat(xmlObject.findValueXML(xmlPath, "BUILD_MODE_STATUS"));
        
        if(bm==1){
            myMain.getBuildMode().setStatus(true);
        }else{
            myMain.getBuildMode().setStatus(false);
        }
    }
    /**
     * @return the xmlObject
     */
    public MyXML getXmlObject() {
        return xmlObject;
    }

    /**
     * @param xmlObject the xmlObject to set
     */
    public void setXmlObject(MyXML xmlObject) {
        this.xmlObject = xmlObject;
    }

    /**
     * @return the myConfigXMLDefault
     */
    public MyConfigXMLDefault getMyConfigXMLDefault() {
        return myConfigXMLDefault;
    }

    /**
     * @param myConfigXMLDefault the myConfigXMLDefault to set
     */
    public void setMyConfigXMLDefault(MyConfigXMLDefault myConfigXMLDefault) {
        this.myConfigXMLDefault = myConfigXMLDefault;
    }
}
