/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package XML;

import java.io.File;
import java.util.ArrayList;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import Simulator.Main;

/**
 *
 * @author Alejandro Esquiva Rodríguez
 *
 * Mail: alejandro@geekytheory.com
 * Twitter: @alex_esquiva
 */
public class MyXML {
    Main myMain;
    public MyXML(Main app){
       myMain = app;
    }
    
    public void generate(String name, ArrayList<String> key,ArrayList<String> value) throws ParserConfigurationException, TransformerConfigurationException, TransformerException{
        
        if(key.size() == 0 || value.size() == 0 || key.size()!=value.size()){
            System.out.println("ERROR empty ArrayList");
            return;
        }else{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            DOMImplementation implementation = builder.getDOMImplementation();
            Document document = implementation.createDocument(null, name, null);
            document.setXmlVersion("1.0");


            //Main Node
            Element raiz = document.getDocumentElement();
            for(int i=0; i<key.size();i++){
                //Item Node
                Element itemNode = document.createElement("ITEM"); 
                //Key Node
                Element keyNode = document.createElement("KEY"); 
                Text nodeKeyValue = document.createTextNode(key.get(i));
                keyNode.appendChild(nodeKeyValue); 		
                //Value Node
                Element valueNode = document.createElement("VALUE"); 
                Text nodeValueValue = document.createTextNode(value.get(i));				
                valueNode.appendChild(nodeValueValue);
                //append keyNode and valueNode to itemNode
                itemNode.appendChild(keyNode);
                itemNode.appendChild(valueNode);
                //append itemNode to raiz
                raiz.appendChild(itemNode); //pegamos el elemento a la raiz "Documento"
            }                

            //Generate XML
            Source source = new DOMSource(document);
            Result result = new StreamResult(new java.io.File("assets/Config/"+name+".xml")); //nombre del archivo
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
        }
    }
    
    public void generatePhysicalXML(String name,ArrayList<String> object, ArrayList<String> key,ArrayList<String> value) throws ParserConfigurationException, TransformerConfigurationException, TransformerException{
        String val = "";
        try{
            val = myMain.getMyConfig().findPhysicValueXML("Door", "SPEED");
        }catch(Exception e){
            
        }
        if(object.size()== 0 || key.size() == 0 || value.size() == 0 || key.size()!=value.size() || val.equals("-1")){

            return;
        }else{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            DOMImplementation implementation = builder.getDOMImplementation();
            Document document = implementation.createDocument(null, name, null);
            document.setXmlVersion("1.0");


            //Main Node
            Element raiz = document.getDocumentElement();
            for(int i=0; i<object.size();i++){
                //Item Node
                Element itemNode = document.createElement("ITEM"); 
                //Object NOode
                Element objectNode = document.createElement("OBJECT"); 
                Text nodeObjectValue = document.createTextNode(object.get(i));
                objectNode.appendChild(nodeObjectValue);
                
                //number of keys
                int nK = 7;
                for(int j=0; j<nK;j++){
                    //Key Node
                    Element keyNode = document.createElement("KEY"); 
                    Text nodeKeyValue = document.createTextNode(key.get(nK*i+j));
                    keyNode.appendChild(nodeKeyValue); 		
                    //Value Node
                    Element valueNode = document.createElement("VALUE"); 
                    Text nodeValueValue = document.createTextNode(value.get(nK*i+j));				
                    valueNode.appendChild(nodeValueValue);                    
                    //append keyNode and valueNode to objectNode
                    objectNode.appendChild(keyNode);
                    objectNode.appendChild(valueNode);
                }
                itemNode.appendChild(objectNode);
                //append itemNode to raiz
                raiz.appendChild(itemNode); //pegamos el elemento a la raiz "Documento"
            }                

            //Generate XML
            Source source = new DOMSource(document);
            Result result = new StreamResult(new java.io.File("assets/Config/"+name+".xml")); //nombre del archivo
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, result);
        }
    }
    
    public ArrayList<String> readXML(String Name) throws Exception{
        
        
        ArrayList<String> key = new ArrayList<String>();
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/"+Name));
        
        doc.getDocumentElement ().normalize ();
        
        NodeList items = doc.getElementsByTagName("ITEM");

        int totalItems = items.getLength();
        for(int i = 0;i<totalItems;i++){
            Node itemN = items.item(i);
            NodeList keyN = itemN.getChildNodes();
            key.add(keyN.item(0).getTextContent());
        }
        
        return key;
    }
    
    public ArrayList<String> readObjectPhysicXML() throws Exception{
        
        ArrayList<String> object = new ArrayList<String>();
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/physicVariables.xml"));
        
        doc.getDocumentElement ().normalize ();
        
        NodeList items = doc.getElementsByTagName("OBJECT");
        
        int totalItems = items.getLength();
        for(int i=0; i<totalItems; i++){           
            Node itemN = items.item(i);          
            NodeList keyN = itemN.getChildNodes();
            object.add(keyN.item(0).getTextContent());
        }
 
        return object;
    }
    
    public ArrayList<String> readKeysPhysicXML() throws Exception{
               
        ArrayList<String> key = new ArrayList<String>();
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/physicVariables.xml"));
        
        doc.getDocumentElement ().normalize ();
        
        NodeList items = doc.getElementsByTagName("OBJECT");
        
        int totalItems = items.getLength();
        for(int i=0; i<totalItems; i++){
            
            Node itemN = items.item(i);          
            NodeList keyN = itemN.getChildNodes();
            for(int j=1;j<=14;j=j+2){
                key.add(keyN.item(j).getTextContent());
            }
            break;
        }
        
        return key;
    }
    
    public String findValueXML(String Name, String Key) throws Exception{
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/"+Name));
        
        doc.getDocumentElement ().normalize ();
        
        NodeList items = doc.getElementsByTagName("ITEM");
        int totalItems = items.getLength();
        String value = "0";
        String find_key = "";
        for(int i = 0;i<totalItems;i++){
            Node itemN = items.item(i);
            NodeList keyN = itemN.getChildNodes();
            find_key = keyN.item(0).getTextContent();
            if(find_key.equals(Key)){
                value = keyN.item(1).getTextContent()+"";
                break;
            }
        }
        return value;
    }
    
    public String findPhysicValueXML(String Object, String Key) throws Exception{
        
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/physicVariables.xml"));
        
        doc.getDocumentElement ().normalize ();
        
        NodeList items = doc.getElementsByTagName("OBJECT");
        
        int totalItems = items.getLength();
        String object_find = "";
        String key_find = "";
        String value = "0";
        for(int i=0; i<totalItems; i++){           
            Node itemN = items.item(i);        
            NodeList keyN = itemN.getChildNodes();
            object_find = keyN.item(0).getTextContent();
            if(object_find.equals(Object)){
                for(int j=1;j<=14;j=j+2){
                    key_find = keyN.item(j).getTextContent();
                    if(key_find.equals(Key)){
                        value = keyN.item(j+1).getTextContent();
                        break;
                    }else{
                        value="-1";
                    }
                }
                break;
            }else{
                value="-1";
            }
            
        }
        return value;
    }
    
    public void modifyXML(String Name,String Key, String NewValue) throws Exception{

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/"+Name));

        doc.getDocumentElement ().normalize();

        NodeList items = doc.getElementsByTagName("ITEM");
        int totalItems = items.getLength();
        String value = "";
        String find_key = "";
        for(int i = 0;i<totalItems;i++){
            Node itemN = items.item(i);
            NodeList keyN = itemN.getChildNodes();
            find_key = keyN.item(0).getTextContent();
            if(find_key.equals(Key)){
                //value = keyN.item(1).getTextContent()+"";
                keyN.item(1).setTextContent(NewValue);
                break;
            }
        }

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("assets/Config/"+Name));
        transformer.transform(source, result);

        System.out.println("Done!");   

    }
    
    public void modifyPhysicXML(String Object,String Key, String NewValue) throws Exception{
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse (new File("assets/Config/physicVariables.xml"));
        
        doc.getDocumentElement ().normalize ();
        
        NodeList items = doc.getElementsByTagName("OBJECT");
        
        int totalItems = items.getLength();
        String object_find = "";
        String key_find = "";
        String value = "";
        for(int i=0; i<totalItems; i++){           
            Node itemN = items.item(i);        
            NodeList keyN = itemN.getChildNodes();
            object_find = keyN.item(0).getTextContent();
            if(object_find.equals(Object)){
                for(int j=1;j<=14;j=j+2){
                    key_find = keyN.item(j).getTextContent();
                    if(key_find.equals(Key)){
                        //value = keyN.item(j+1).getTextContent();
                        keyN.item(j+1).setTextContent(NewValue);
                        break;
                    }
                }
                break;
            }
            
        }
    
    
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("assets/Config/physicVariables.xml"));
        transformer.transform(source, result);
    }
}
