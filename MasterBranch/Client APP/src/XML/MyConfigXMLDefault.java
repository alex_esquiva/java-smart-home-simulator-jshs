/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package XML;

import java.util.ArrayList;
import Simulator.Main;

/**
 *
 * @author Alejandro Esquiva Rodríguez
 *
 * Mail: alejandro@geekytheory.com
 * Twitter: @alex_esquiva
 */
public class MyConfigXMLDefault {
    
    //**Game Config
    public static final float SCALE = 5f;
    public static final float INIT_LIGHT_INTENSITY = 0.6f;
    
    //**Simulator Config
    public static final float INIT_ENERGY_CONSUMPTION = 0f;
    public static final float INIT_WATER_CONSUMPTION = 0f;
    public static final float INIT_TEMPERATURE = 0f;
    
    
    
    //**Player Config
    public static final float PLAYER_POSITION_X = 0f;
    public static final float PLAYER_POSITION_Y = 10f;
    public static final float PLAYER_POSITION_Z = 10f;
    public static final float INIT_PLAYER_POSITION_X = 0f;
    public static final float INIT_PLAYER_POSITION_Y = 10f;
    public static final float INIT_PLAYER_POSITION_Z = 10f;
    public static final float PLAYER_GRAVITY = 30f;
    public static final float PLAYER_FALLSPEED = 30f;
    public static final float PLAYER_JUMSPEED = 20f;
    
    //**Data Base Config
    public static boolean SEND_DATA = false;
    public static boolean INTERNET_CONNECTION = false;
    //public static final String SERVER_PATH = "http://geekytheory.com/";
    public static final String SERVER_PATH = "http://localhost/";
    
    //**Build Mode
    public static boolean BUILD_MODE_STATUS = false;

    
    //**Object's Parameters
    //--Doors
    public static final float DOOR_SPEED = 1f;
    public static final float MAIN_DOOR_SPEED = 1f;
    public static final float EXTERNAL_DOOR_SPEED = 1f;
    //--Fan
    public static final float FAN_SPEED = 5f;
    public static final float FAN_ENERGY_CONSUMPTION_ON = 0.08f; //KWh    
    public static final float FAN_TEMPERATURE_ON = -1f; //KWh    
    //--Dishwasher
    public static final float DISHWASHER_ENERGY_CONSUMPTION_ON = 0.93f; //KWh
    public static final float DISHWASHER_ENERGY_CONSUMPTION_OFF = 0.03f; //KWh
    public static final float DISHWASHER_WATER_CONSUMPTION_ON = 15f; //L
    //--Fridge
    public static final float FRIDGE_ENERGY_CONSUMPTION_ON = 0.0369f; //KWh
    public static final float FRIDGE_ENERGY_CONSUMPTION_OFF = 0.0369f; //KWh
    //--Radiator
    public static final float RADIATOR_ENERGY_CONSUMPTION_ON = 0.0666f; //KWh
    public static final float RADIATOR_TEMPERATURE_ON = 1f; //KWh
    
    //--Extractor
    public static final float EXRACTOR_ENERGY_CONSUMPTION_ON = 0.160f; //KWh
    //--LightBasic
    public static final float LIGHTBASIC_ENERGY_CONSUMPTION_ON = 0.01f; //KWh
    //--WashingMachine
    public static final float WASHINGMACHINE_ENERGY_CONSUMPTION_ON = 0.3f; //KWh
    public static final float WASHINGMACHINE_WATER_CONSUMPTION_ON = 150f; //KWh
    //--Microwave
    public static final float MICROWAVE_ENERGY_CONSUMPTION_ON = 1.375f; //KWh
    //--Oven
    public static final float OVEN_ENERGY_CONSUMPTION_ON = 2.625f; //KWh
    //Windows
    public static final float WindowSpeed = 2f;
    //Bathroom Set
    public static final float BATHROOMSET_WATER_CONSUMPTION_ON = 80f; //L
    //Shower
    public static final float SHOWER_WATER_CONSUMPTION_ON = 120f; //L
    //Bathroom sink
    public static final float BATHROOMSINK_WATER_CONSUMPTION_ON = 2f; //L
    //WC
    public static final float WC_WATER_CONSUMPTION_ON = 6f; //L
    //--Tv
    public static final float TV_ENERGY_CONSUMPTION_ON = 0.12f; //KWh
    //--Computer
    public static final float COMPUTER_ENERGY_CONSUMPTION_ON = 0.1333f; //KWh
    //--SmokeStack
    public static final float SMOKESTACK_TEMPERATURE_ON = 1f; //KWh
    
    
    
    //generate general xml
    ArrayList<String> generalKey = new ArrayList<String>();
    ArrayList<String> generalValue = new ArrayList<String>();
    //generate player xml
    ArrayList<String> playerKey = new ArrayList<String>();
    ArrayList<String> playerValue = new ArrayList<String>();
    //generate Physical Variables xml
    
    private ArrayList<String> physicalObject = new ArrayList<String>();
    private ArrayList<String> physicalKey = new ArrayList<String>();
    private ArrayList<String> physicalValue = new ArrayList<String>();
    
    private MyXML xml;
    Main myMain;
    public MyConfigXMLDefault(Main myMain){
        this.myMain = myMain;
        xml = new MyXML(myMain);
    }
    
    public void generateXMLDefault(){
        
        generalKey.add("SCALE");
        generalValue.add(SCALE+"");
        
        generalKey.add("INIT_LIGHT_INTENSITY");
        generalValue.add(INIT_LIGHT_INTENSITY+"");
        
        generalKey.add("INIT_ENERGY_CONSUMPTION");
        generalValue.add(INIT_ENERGY_CONSUMPTION+"");
        
        generalKey.add("INIT_WATER_CONSUMPTION");
        generalValue.add(INIT_WATER_CONSUMPTION+"");
        
        generalKey.add("INIT_TEMPERATURE");
        generalValue.add(INIT_TEMPERATURE+"");
        
        generalKey.add("SEND_DATA");
        if(SEND_DATA == true){
            generalValue.add("1");
        }else{
            generalValue.add("0");
        }
        
        generalKey.add("INTERNET_CONNECTION");
        if(INTERNET_CONNECTION == true){
            generalValue.add("1");
        }else{
            generalValue.add("0");
        }
        
        generalKey.add("SERVER_PATH");
        generalValue.add(SERVER_PATH+"");
        
        generalKey.add("BUILD_MODE_STATUS");
        if(BUILD_MODE_STATUS == true){
            generalValue.add("1");
        }else{
            generalValue.add("0");
        }
        
        try{
            getXml().generate("general", generalKey, generalValue);
        }catch(Exception e){}

        playerKey.add("PLAYER_POSITION_X");
        playerValue.add(PLAYER_POSITION_X+"");
        
        playerKey.add("PLAYER_POSITION_Y");
        playerValue.add(PLAYER_POSITION_Y+"");
        
        playerKey.add("PLAYER_POSITION_Z");
        playerValue.add(PLAYER_POSITION_Z+"");
        
        playerKey.add("INIT_PLAYER_POSITION_X");
        playerValue.add(INIT_PLAYER_POSITION_X+"");

        playerKey.add("INIT_PLAYER_POSITION_Y");
        playerValue.add(INIT_PLAYER_POSITION_Y+"");
        
        playerKey.add("INIT_PLAYER_POSITION_Z");
        playerValue.add(INIT_PLAYER_POSITION_Z+"");
        
        playerKey.add("PLAYER_GRAVITY");
        playerValue.add(PLAYER_GRAVITY+"");
        
        playerKey.add("PLAYER_FALLSPEED");
        playerValue.add(PLAYER_FALLSPEED+"");
        
        playerKey.add("PLAYER_JUMSPEED");
        playerValue.add(PLAYER_JUMSPEED+"");
        
        try{
            getXml().generate("player", playerKey, playerValue);
        }catch(Exception e){}
        
        //Physical Variables
        getPhysicalObject().add("Door");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(DOOR_SPEED+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("MainDoor");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(MAIN_DOOR_SPEED+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("ExternalDoor");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(EXTERNAL_DOOR_SPEED+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Fan");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(FAN_SPEED+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(FAN_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add(FAN_TEMPERATURE_ON+"");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Dishwasher");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(DISHWASHER_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add(DISHWASHER_ENERGY_CONSUMPTION_OFF+"");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add(DISHWASHER_WATER_CONSUMPTION_ON+"");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        
        getPhysicalObject().add("Fridge");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(FRIDGE_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add(FRIDGE_ENERGY_CONSUMPTION_OFF+"");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Radiator");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(RADIATOR_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add(RADIATOR_TEMPERATURE_ON+"");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("SmokeStack");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add(SMOKESTACK_TEMPERATURE_ON+"");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Extractor");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(EXRACTOR_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");        

        getPhysicalObject().add("LightBasic");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(LIGHTBASIC_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("LightFloorTraditional");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(LIGHTBASIC_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WashingMachine");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(WASHINGMACHINE_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add(WASHINGMACHINE_WATER_CONSUMPTION_ON+"");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Microwave");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(MICROWAVE_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Oven");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(OVEN_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WindowBig");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(WindowSpeed+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WindowLarge");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(WindowSpeed+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WindowLargeV2");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(WindowSpeed+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WindowMedium");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(WindowSpeed+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WindowSmall");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(WindowSpeed+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WindowType1");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add(WindowSpeed+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("BathroomSet");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add(BATHROOMSET_WATER_CONSUMPTION_ON+"");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("BathroomSink");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add(BATHROOMSINK_WATER_CONSUMPTION_ON+"");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Shower");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add(SHOWER_WATER_CONSUMPTION_ON+"");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("WC");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add(WC_WATER_CONSUMPTION_ON+"");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Computer");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(COMPUTER_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        getPhysicalObject().add("Tv");
        getPhysicalKey().add("SPEED");
        getPhysicalValue().add("0");
        getPhysicalKey().add("ENERGY_CONSUMPTION_ON");
        getPhysicalValue().add(TV_ENERGY_CONSUMPTION_ON+"");
        getPhysicalKey().add("ENERGY_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("WATER_CONSUMPTION_OFF");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_ON");
        getPhysicalValue().add("0");
        getPhysicalKey().add("TEMPERATURE_OFF");
        getPhysicalValue().add("0");
        
        try{
            getXml().generatePhysicalXML("physicVariables", getPhysicalObject(), getPhysicalKey(), getPhysicalValue());
        }catch(Exception e){
            System.err.println("ERRROOROROROOR");
        }
    }

    /**
     * @return the physicalObject
     */
    public ArrayList<String> getPhysicalObject() {
        return physicalObject;
    }

    /**
     * @param physicalObject the physicalObject to set
     */
    public void setPhysicalObject(ArrayList<String> physicalObject) {
        this.physicalObject = physicalObject;
    }

    /**
     * @return the physicalKey
     */
    public ArrayList<String> getPhysicalKey() {
        return physicalKey;
    }

    /**
     * @param physicalKey the physicalKey to set
     */
    public void setPhysicalKey(ArrayList<String> physicalKey) {
        this.physicalKey = physicalKey;
    }

    /**
     * @return the physicalValue
     */
    public ArrayList<String> getPhysicalValue() {
        return physicalValue;
    }

    /**
     * @param physicalValue the physicalValue to set
     */
    public void setPhysicalValue(ArrayList<String> physicalValue) {
        this.physicalValue = physicalValue;
    }

    /**
     * @return the xml
     */
    public MyXML getXml() {
        return xml;
    }

    /**
     * @param xml the xml to set
     */
    public void setXml(MyXML xml) {
        this.xml = xml;
    }
}
