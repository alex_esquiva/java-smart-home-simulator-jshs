/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;


import Control.CustomListBoxItem;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.render.NiftyImage;
import java.text.DecimalFormat;
import java.util.ArrayList;
import Simulator.Main;


/**
 *
 * @author IdeaPad Z500
 */
public class MyActionPanel {
    Main myMain;
    Nifty myNifty;
    NiftyImage image;
    NiftyImage iconStatus;
    
    private boolean statusActionPanel = false;
    
    public MyActionPanel(Main myMain, Nifty nifty) {
        this.myMain = myMain;
        myNifty = nifty;
        
        hideActionPanel();
       
    }

    public void updateListBox() {
        
        ListBox<CustomListBoxItem> mylist = myNifty.getCurrentScreen().findNiftyControl("myCustomListBox", ListBox.class);
        //clear list
        mylist.clear();
        
        ArrayList<ArrayList<String>> objectsInfo;
        
        objectsInfo = myMain.getGui().getObjectsInfo();
        
        for(int i=0;i<objectsInfo.size();i++){
            ArrayList<String> data;
            ArrayList<String> data_before = new ArrayList<String>();
            data = objectsInfo.get(i);
            
            try{
                data_before = objectsInfo.get(i-1);
            }catch(Exception e){
              data_before.add("0");
              data_before.add("0");
              data_before.add("0");
              data_before.add("0");
              data_before.add(myMain.getPhysicalCharacteristics().getEnergyConsumption()+"");
              data_before.add(myMain.getPhysicalCharacteristics().getWaterConsumption()+"");
              data_before.add(myMain.getPhysicalCharacteristics().getTemperature()+"");
              data_before.add("0");
              data_before.add("0");
              data_before.add("0");
            }
            
            String name = data.get(0);
            String where = data.get(7);
            
            if(Boolean.parseBoolean(data.get(3))){
                iconStatus = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),"Interface/on.png", false);
            }else{
                iconStatus = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),"Interface/off.png", false);
            }

            float eC = 0;
            float wC = 0;
            float tC = 0;
            try{
            eC = Float.parseFloat(data.get(4))-Float.parseFloat(data_before.get(4));
            wC = Float.parseFloat(data.get(5))-Float.parseFloat(data_before.get(5));
            tC = Float.parseFloat(data.get(6))-Float.parseFloat(data_before.get(6));
            }catch(Exception e){
                e.printStackTrace();
            }
            String text_ec = "";
            String text_wc = "";
            String text_tc = "";
            
            DecimalFormat df = new DecimalFormat("0.000");

            if(eC<0){
                text_ec = "[-]Increased EC\n"+df.format(Float.parseFloat(data.get(4)))+" ("+df.format(eC)+")";
            }else{
                text_ec = "[+]Increased EC\n"+df.format(Float.parseFloat(data.get(4)))+" (+"+df.format(eC)+")";
            }
            if(wC<0){
                text_wc = "[-]Increased WC\n"+df.format(Float.parseFloat(data.get(5)))+" ("+df.format(wC)+")";
            }else{
                text_wc = "[+]Increased WC\n"+df.format(Float.parseFloat(data.get(5)))+" (+"+df.format(wC)+")";
            }
            if(tC<0){
                text_tc = "[-]Increased Temp\n"+df.format(Float.parseFloat(data.get(6)))+" ("+df.format(tC)+")";
            }else{
                text_tc = "[+]Increased Temp\n"+df.format(Float.parseFloat(data.get(6)))+" (+"+df.format(tC)+")";
            }
            System.out.println(data.get(8));

            try{
                image = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),data.get(8), false);
            }catch(Exception e){
                image = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),"Interface/noimage.png", false);
            }

            CustomListBoxItem clb = new CustomListBoxItem(iconStatus,
                                                          data.get(9),
                                                          name,
                                                          where,
                                                          text_ec,
                                                          text_wc,
                                                          text_tc,
                                                          image
            );
            mylist.addItem(clb);  
            
        }            

    }
    
    public void hideActionPanel(){
        setStatusActionPanel(false);     
        myNifty.getCurrentScreen().findElementByName("actionPanel").hide();
    }
    
    public void showActionPanel(){
        setStatusActionPanel(true);     
        updateListBox();
        myNifty.getCurrentScreen().findElementByName("actionPanel").show();
    }

    /**
     * @return the statusActionPanel
     */
    public boolean isStatusActionPanel() {
        return statusActionPanel;
    }

    /**
     * @param statusActionPanel the statusActionPanel to set
     */
    public void setStatusActionPanel(boolean statusActionPanel) {
        this.statusActionPanel = statusActionPanel;
    }

    public void cancelActionPanel() {
        myMain.getGui().setStatusGUI(true);
        hideActionPanel();
    }
}
