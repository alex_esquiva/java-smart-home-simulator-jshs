/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import DataBase.ObjectsDB;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Console;
import de.lessvoid.nifty.controls.ConsoleCommands;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class MyConsole {
    private Nifty nifty;
    private Console console;
    private Main myMain;
    
    public MyConsole(Main app,Nifty nifty,Console console){
        this.myMain = app;
        this.console = console;
        this.nifty = nifty; 
        
        initConsole();
    }

    private void initConsole() {
        
        console.output("Welcome to Java Smart Home Simulator.");
        console.output("");
        ObjectsDB db = new ObjectsDB();
        db.updateSession();
        console.output("This is the session number: " + db.getSession());
        console.output("");
        console.output("Write 'help' to show all commands and their definitions.");
        console.output("");
        
        // create the console commands class and attach it to the console
        ConsoleCommands consoleCommands = new ConsoleCommands(nifty, console);

        // create a simple command (see below for implementation) this class will be called when the command is detected
        // and register the command as a command with the console
        MyCommands myCommands = new MyCommands();

        //**GENERAL COMMANDS
        consoleCommands.registerCommand("help", myCommands);
        consoleCommands.registerCommand("quit", myCommands);
        consoleCommands.registerCommand("exit", myCommands);
        consoleCommands.registerCommand("salir", myCommands);
        consoleCommands.registerCommand("out", myCommands);
        consoleCommands.registerCommand("clear", myCommands);
        consoleCommands.registerCommand("clc", myCommands);
        consoleCommands.registerCommand("setDebugPhysicMode ", myCommands);
        consoleCommands.registerCommand("setBuildMode ", myCommands);

        //**PLAYER COMMANDS
        consoleCommands.registerCommand("getGravity", myCommands);
        consoleCommands.registerCommand("setGravity", myCommands);
        consoleCommands.registerCommand("getJumpSpeed", myCommands);
        consoleCommands.registerCommand("setJumpSpeed ", myCommands);
        consoleCommands.registerCommand("getFallSpeed", myCommands);
        consoleCommands.registerCommand("setFallSpeed ", myCommands);

        //**DATA BASE COMMANDS
        consoleCommands.registerCommand("getSession ", myCommands);
        consoleCommands.registerCommand("getSessionInfo ", myCommands);
        consoleCommands.registerCommand("loadSession ", myCommands);
       
        //**PHYSICAL VARIABLE
        consoleCommands.registerCommand("display ", myCommands);
        consoleCommands.registerCommand("displayTemperature ", myCommands);
        consoleCommands.registerCommand("displayEnergyConsumption ", myCommands);
        consoleCommands.registerCommand("displayWaterConsumption ", myCommands);
        
        // finally enable command completion
        consoleCommands.enableCommandCompletion(true);
    }

    /**
     * @return the console
     */
    public Console getConsole() {
        return console;
    }

    /**
     * @param console the console to set
     */
    public void setConsole(Console console) {
        this.console = console;
    }
    
    private class MyCommands implements ConsoleCommands.ConsoleCommand {
        @Override
        public void execute(final String[] args) {

            //If there is just one arg
            if(args[0].equals("help"))  showHelp();
            if(args[0].equals("quit"))  myMain.stop();
            if(args[0].equals("exit"))  myMain.stop();
            if(args[0].equals("salir")) myMain.stop();
            if(args[0].equals("out"))  myMain.stop();
            if(args[0].equals("clear")){
                getConsole().clear();
                console.output("Write 'help' to show all commands and their definitions.");
                console.output("");
            }
            if(args[0].equals("clc")){
                getConsole().clear();
                console.output("Write 'help' to show all commands and their definitions.");
                console.output("");
            }

            if(args[0].equals("getGravity"))  getGravity();
            if(args[0].equals("getJumpSpeed"))  getJumpSpeed();
            if(args[0].equals("getFallSpeed"))  getFallSpeed();

            if(args[0].equals("getSessionInfo")) myMain.getDb().getSessionInfo();
            
            try {
                if(args[0].equals("displayTemperature")) myMain.getGui().getMyGraphGui().drawGraph(2);
                if(args[0].equals("displayWaterConsumption")) myMain.getGui().getMyGraphGui().drawGraph(1);
                if(args[0].equals("displayEnergyConsumption")) myMain.getGui().getMyGraphGui().drawGraph(0);
            } catch (IOException ex) {
                Logger.getLogger(MyConsole.class.getName()).log(Level.SEVERE, null, ex);
            }

            if(args.length==2){
                try{
                    if(args[0].equals("setDebugPhysicMode")){
                        setDebugMode(Boolean.parseBoolean(args[1]));
                    }

                    if(args[0].equals("setBuildMode")){
                        myMain.getBuildMode().setStatus(Boolean.parseBoolean(args[1]));
                    }

                    if(args[0].equals("setGravity")){
                        setGravity(Float.parseFloat(args[1]));
                    }
                    if(args[0].equals("setJumpSpeed")){
                        setJumpSpeed(Float.parseFloat(args[1]));
                    }
                    if(args[0].equals("setFallSpeed")){
                        setFallSpeed(Float.parseFloat(args[1]));
                    }
                    if(args[0].equals("display")){
                        myMain.getGui().getMyGraphGui().drawGraph(Integer.parseInt(args[1]));
                    }
                }catch(Exception e){
                        getConsole().outputError("Try Again!!!");
                }

                try{
                    if(args[0].equals("getSession")){
                        myMain.getDb().sessionConsolePrint(Integer.parseInt(args[1]));
                    }
                    if(args[0].equals("loadSession")){
                        myMain.getDb().loadScenaryDataBase(Integer.parseInt(args[1]));
                    }
                }catch(Exception e){
                        getConsole().outputError("Try Again!!!");
                }
            }
        }

            private void setDebugMode(boolean bool) {
                if(bool){
                    myMain.getBulletAppState().getPhysicsSpace().enableDebug(myMain.getAssetManager());
                }else{
                    myMain.getBulletAppState().getPhysicsSpace().disableDebug();
                }
            }

            public void setGravity(float gravity) {
            myMain.getPlayer().setGravity(gravity);
                getConsole().output("Gravity: " + gravity);
            }
            public void getGravity() {
                  getConsole().output("Gravity: "+myMain.getPlayer().getGravity());
            }

            public void setJumpSpeed(float speed) {
              myMain.getPlayer().setGravity(speed);
                  getConsole().output("Jump Speed: "+speed);
            }
            public void getJumpSpeed() {
                  getConsole().output("Jump Speed: "+myMain.getPlayer().getJumpSpeed());
            }
            public void setFallSpeed(float speed) {
                myMain.getPlayer().setGravity(speed);
                getConsole().output("Fall Speed: "+speed);
            }
            public void getFallSpeed() {
                getConsole().output("Fall Speed: "+myMain.getPlayer().getFallSpeed());
            }
      }
    
    public void showHelp(){
        console.output("This is the Help Guide.");
        console.output("");
        console.output("**GENERAL");
        console.output("quit / exit / salir / out || -> Close Program.");
        console.output("clear / clc || -> Clear console.");
        console.output("setDebugPhysicMode [true/false] || -> Show the physic space.");
        console.output("setBuildMode [true/false] || -> Change Normal mode to Build mode [BETA].");
        console.output("");
        console.output("**PLAYER");
        console.output("getGravity || -> Get the gravity of the player.");
        console.output("setGravity [VALUE] || -> Set the gravity of the player.");
        console.output("getJumpSpeed || -> Get the jump speed of the player.");
        console.output("setJumpSpeed [VALUE] || -> Set the jump speed of the player.");
        console.output("getFallSpeed || -> Get the fall speed of the player.");
        console.output("setFallSpeed [VALUE] || -> Set the fall speed of the player.");
        console.output("");
        console.output("**DATA BASE.");
        console.output("getSession [VALUE]|| -> Get the objects information of the session.");
        console.output("getSessionInfo || -> Get the number session and the time.");
        console.output("loadSession [VALUE] || -> Load the scenary of the session input.");
        console.output("");
        console.output("**PHYSICAL VARIABLES");
        console.output("display [INDEX]|| -> Plot the graph by Index|| 0 -> Energy || 1 -> Water || 2 -> Temperature");
        console.output("displayEnergyConsumption || -> Plot Energy Consumption");
        console.output("displayWaterConsumption || -> Plot Water Consumption");
        console.output("displayTemperature || -> Plot Temperature");
    }
    
     public void hideConsole(){  
        nifty.getCurrentScreen().findElementByName("console0").hide();        
    }
     
     public void showConsole(){
        nifty.getCurrentScreen().findElementByName("console0").show();
        console.getTextField().setFocus();
     }     
}
