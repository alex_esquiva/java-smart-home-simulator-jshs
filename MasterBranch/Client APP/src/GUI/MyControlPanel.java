/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.dropdown.DropDownControl;
import de.lessvoid.nifty.controls.textfield.TextFieldControl;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;
import Simulator.MyConfig;

/**
 *
 * @author Alejandro Esquiva Rodríguez
 *
 * Mail: alejandro@geekytheory.com
 * Twitter: @alex_esquiva
 */
public class MyControlPanel{
    private Main myMain;
    private Nifty nifty;
    private Label labelcp1;
    private int indexAntiloop1=0,indexAntiloop2 = 0,indexAntiloop3 = 0;
    private String keepText = "";
    private ArrayList<String> vGeneral;
    private ArrayList<String> vPlayer = new ArrayList<String>();
    private ArrayList<String> vPhysicsObjects = new ArrayList<String>();
    private ArrayList<String> vPhysicsKeys = new ArrayList<String>();
    
    DropDownControl dropDown1,dropDown2,dropDown3;
    TextFieldControl textField;
    
    private boolean statusControlPanel = false;
    
    public MyControlPanel(Main app, Nifty nifty){
        this.myMain = app;
        this.nifty = nifty;
        //HIDE CONTROL PANEL
        //hideControlPanel();
        
        //Hide Sub Panels
        
        //Init DropDown
        dropDown1 = nifty.getCurrentScreen().findControl("dropDown1", DropDownControl.class);
        dropDown2 = nifty.getCurrentScreen().findControl("dropDown2", DropDownControl.class);
        dropDown3 = nifty.getCurrentScreen().findControl("dropDown3", DropDownControl.class);
        textField = nifty.getCurrentScreen().findControl("tf", TextFieldControl.class);
        labelcp1 = nifty.getCurrentScreen().findNiftyControl("labelcp1", Label.class);
        //applyButton = nifty.getCurrentScreen().findControl("bapply", ButtonControl.class);
        
        initDropDown1();
        hideControlPanel();
        loadConfig();
        
    }
    
    public void showControlPanel(){
        loadConfig();
        setStatusControlPanel(true);
        nifty.getCurrentScreen().findElementByName("optionPanel").show();
        nifty.getCurrentScreen().findElementByName("cp1").show();
        nifty.getCurrentScreen().findElementByName("cp2").show();
        nifty.getCurrentScreen().findElementByName("cp6").show();
    }
    
    public void hideControlPanel(){
        setStatusControlPanel(false);
        nifty.getCurrentScreen().findElementByName("optionPanel").hide();
        nifty.getCurrentScreen().findElementByName("cp1").hide();
        nifty.getCurrentScreen().findElementByName("cp2").hide();
        nifty.getCurrentScreen().findElementByName("cp3").hide();
        nifty.getCurrentScreen().findElementByName("cp4").hide();
        nifty.getCurrentScreen().findElementByName("cp5").hide();
        nifty.getCurrentScreen().findElementByName("cp6").hide();
    }

    private void initDropDown1() {
        nifty.getCurrentScreen().findElementByName("cp3").hide();
        nifty.getCurrentScreen().findElementByName("cp4").hide();
        nifty.getCurrentScreen().findElementByName("cp5").hide();
        
        dropDown1.addItem("Not Selected");
        dropDown1.addItem("General variables");
        dropDown1.addItem("Player Variables");
        dropDown1.addItem("Pyshic Variables");
        
    }

    private void initDropDown2(int index) {
        labelcp1.setText("Variable");
        
        if(index == 0){
            nifty.getCurrentScreen().findElementByName("cp3").hide();
           dropDown2.clear();
           dropDown3.clear();
        }else{
            nifty.getCurrentScreen().findElementByName("cp3").show();
            
        }
        
        
        nifty.getCurrentScreen().findElementByName("cp4").hide();
        
        
        
        if(index == 1){
            
            dropDown2.clear();
            dropDown3.clear();
            dropDown2.addItem("NOT SELECTED");
            for(int i=0; i<vGeneral.size();i++){
                dropDown2.addItem(vGeneral.get(i));
            }

        }
        if(index == 2){
            dropDown2.clear();
            dropDown3.clear();      
            dropDown2.addItem("NOT SELECTED");
            for(int i=0; i<vPlayer.size();i++){
                dropDown2.addItem(vPlayer.get(i));
            }
        }
        if(index == 3){
            labelcp1.setText("Object");
            nifty.getCurrentScreen().findElementByName("cp5").hide();
            dropDown2.clear();
            dropDown3.clear();
            dropDown2.addItem("NOT SELECTED");
            for(int i=0; i<vPhysicsObjects.size();i++){
                dropDown2.addItem(vPhysicsObjects.get(i));
            }
        }
    }
    
    private void initDropDown3(int index){
        
        if(index == 0){
            dropDown3.clear();
            nifty.getCurrentScreen().findElementByName("cp4").hide();
        }else{
            nifty.getCurrentScreen().findElementByName("cp4").show();
            
            dropDown3.clear();
            dropDown3.addItem("NOT SELECTED");
            for(int i=0; i<vPhysicsKeys.size();i++){
                dropDown3.addItem(vPhysicsKeys.get(i));
            }          
        }
    }
    void update(float tpf) {
        
        if(dropDown1.getSelectedIndex()==0){
            nifty.getCurrentScreen().findElementByName("cp3").hide();
            nifty.getCurrentScreen().findElementByName("cp4").hide();
            nifty.getCurrentScreen().findElementByName("cp5").hide();
        }
        
        if(dropDown1.getSelectedIndex() != indexAntiloop1){
            updateDropsDown(1);
            indexAntiloop1 = dropDown1.getSelectedIndex();

        }
        if(dropDown2.getSelectedIndex() != indexAntiloop2){
            updateDropsDown(2);
            indexAntiloop2 = dropDown2.getSelectedIndex();
        }
        if(dropDown3.getSelectedIndex() != indexAntiloop3){
            updateDropsDown(3);
            indexAntiloop3 = dropDown3.getSelectedIndex();
        }

    }

    private void updateDropsDown(int level) {
        //First, check dropdown1
        int index_cdd1 = dropDown1.getSelectedIndex();
        int index_cdd2 = dropDown2.getSelectedIndex();
        int index_cdd3 = dropDown3.getSelectedIndex();
        //LEVEL 1
        if(level == 1){      
            initDropDown2(index_cdd1);
        }
        if(level == 2){
            if(index_cdd1 == 3) initDropDown3(index_cdd2);
        }
        
        //SHOW INPUT TEXT
        if(index_cdd1 != 0 && index_cdd2 != 0 && index_cdd3 != 0){
            nifty.getCurrentScreen().findElementByName("cp5").show();
        }else{
            nifty.getCurrentScreen().findElementByName("cp5").hide();
        }
        
        //INPUT VALUE
        if(nifty.getCurrentScreen().findElementByName("cp5").isVisible()){
            keepText = "";
            //General Config
            if(index_cdd1==1 && index_cdd2>0){
                String text = "";
                try {
                    text = myMain.getMyConfig().findValueXML(MyConfig.GENERAL_XML_PATH,vGeneral.get(index_cdd2-1)+"");
                } catch (Exception ex) {
                    Logger.getLogger(MyControlPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                textField.setText(text);
                keepText = text;
            }
            //Player Config
            if(index_cdd1==2 && index_cdd2>0){
                //update player position to XML
                try{
                    myMain.getMyConfig().ModifyXML(MyConfig.PLAYER_XML_PATH, "PLAYER_POSITION_X", myMain.getPlayer().getPhysicsLocation().getX()+"");
                    myMain.getMyConfig().ModifyXML(MyConfig.PLAYER_XML_PATH, "PLAYER_POSITION_Y", myMain.getPlayer().getPhysicsLocation().getY()+"");
                    myMain.getMyConfig().ModifyXML(MyConfig.PLAYER_XML_PATH, "PLAYER_POSITION_Z", myMain.getPlayer().getPhysicsLocation().getZ()+"");
                }catch(Exception e){}
                
                String text = "";
                try {
                    text = myMain.getMyConfig().findValueXML(MyConfig.PLAYER_XML_PATH,vPlayer.get(index_cdd2-1)+"");
                } catch (Exception ex) {
                    Logger.getLogger(MyControlPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                textField.setText(text);
                keepText = text;
            }
            //Physic Config
            if(index_cdd1==3 && index_cdd2>0 && index_cdd3>0){
                String text = "";
                try {
                    text = myMain.getMyConfig().findPhysicValueXML(dropDown2.getSelection().toString(),dropDown3.getSelection().toString());
                    //myMain.getGui().getConsole().output(dropDown2.getSelection().toString());
                    //myMain.getGui().getConsole().output(dropDown3.getSelection().toString());
                } catch (Exception ex) {
                    Logger.getLogger(MyControlPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                textField.setText(text);
                keepText = text;
            }
            
            
        }
        
        
        
    }

    private void loadConfig(){
        try{
            vGeneral = myMain.getMyConfig().getGeneralConfig();
            vPlayer = myMain.getMyConfig().getPlayerConfig();
            vPhysicsObjects = myMain.getMyConfig().getPhysicObjectsConfig();
            vPhysicsKeys = myMain.getMyConfig().getPhysicKeysConfig();
        }catch(Exception e){}
    }
    
    public void updateConfig()throws Exception{
        myMain.getGui().setStatusGUI(true);
        int index_cdd1 = dropDown1.getSelectedIndex();

        if(index_cdd1 == 1){
            updateGeneralVariables();
        }
        
        if(index_cdd1 == 2){
            updatePlayerVariables();
        }
        
        if(index_cdd1 == 3){
            updatePhysicalVariables();
        }
    }
    
    
    public void updateGeneralVariables(){
       //If we can parse to float the value we update physical variables else we put th etextfield the old value 
       try{
           Float.parseFloat(textField.getText());
           myMain.getMyConfig().ModifyXML(MyConfig.GENERAL_XML_PATH, dropDown2.getSelection().toString(), textField.getText());
           myMain.getMyConfig().updateGeneralVariables();
       }catch(Exception e){
           e.printStackTrace();
           textField.setText(keepText);
       }
    }
    
    public void updatePlayerVariables(){
       //If we can parse to float the value we update physical variables else we put th etextfield the old value 
       try{
           Float.parseFloat(textField.getText());
           myMain.getMyConfig().ModifyXML(MyConfig.PLAYER_XML_PATH, dropDown2.getSelection().toString(), textField.getText());
           myMain.getMyConfig().updatePlayerVariables();
       }catch(Exception e){
           textField.setText(keepText);
       }
    }
    public void updatePhysicalVariables() throws Exception{
        //If we can parse to float the value we update physical variables else we put th etextfield the old value 
        try{
            Float.parseFloat(textField.getText());
            myMain.getMyConfig().ModifyPhysicXML(dropDown2.getSelection().toString(), dropDown3.getSelection().toString(), textField.getText());
            myMain.getMyConfig().updatePhysicalVariables();
        }catch(Exception e){
            textField.setText(keepText);
        }
    }

    public void acceptConfig() {
        try {
        hideControlPanel();
            updateConfig();
            myMain.getGui().setStatusGUI(false);
        } catch (Exception ex) {
            Logger.getLogger(MyControlPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cancelConfig() {
        hideControlPanel();
        myMain.getGui().setStatusGUI(false);
    }

    /**
     * @return the statusControlPanel
     */
    public boolean isStatusControlPanel() {
        return statusControlPanel;
    }

    /**
     * @param statusControlPanel the statusControlPanel to set
     */
    public void setStatusControlPanel(boolean statusControlPanel) {
        this.statusControlPanel = statusControlPanel;
    }
    
}
