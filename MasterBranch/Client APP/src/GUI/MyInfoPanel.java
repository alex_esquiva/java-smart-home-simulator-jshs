/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import Control.SpacePosition;
import com.jme3.math.Vector3f;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.effects.EffectEventId;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class MyInfoPanel {
    private Nifty myNifty;
    private Main myMain;
    
    private Label position,worldtime,time,wifi,energy,water,temperature;
    private Element element;
    private NiftyImage newImage,wifiImage;
    long startTime = System.currentTimeMillis();
    private long timeSimulatorMilis = 0;
    private long extratime = 0;
    private DateFormat hourdateFormat;
    private Date date;
    
    public MyInfoPanel(Main app, Nifty nifty){
        myMain = app;
        myNifty = nifty;
        
        position = nifty.getCurrentScreen().findNiftyControl("position", Label.class);
        worldtime = nifty.getCurrentScreen().findNiftyControl("worldtime", Label.class);
        time = nifty.getCurrentScreen().findNiftyControl("time", Label.class);
        wifi = nifty.getCurrentScreen().findNiftyControl("wifi", Label.class);
        energy = nifty.getCurrentScreen().findNiftyControl("energy", Label.class);
        water = nifty.getCurrentScreen().findNiftyControl("water", Label.class);
        temperature = nifty.getCurrentScreen().findNiftyControl("temperature", Label.class);
        
        switchImagesWifi();
        
    }
    
    public void update(float tpf){
        time.setText(getSimulationTime());
    }
    
    public String getSimulationTime(){
         //making date object;
      date = new Date();
        //Format date
      hourdateFormat  = new SimpleDateFormat("dd/MM/yyyy\nHH:mm:ss");
        //Update WorldTime
        getWorldtime().setText(hourdateFormat.format(date));
        //Time
        setTimeSimulatorMilis(getExtratime() + System.currentTimeMillis() - startTime - 3600000)/*1hour*/;
      hourdateFormat  = new SimpleDateFormat("HH:mm:ss");   
      
      return hourdateFormat.format(new Date(getTimeSimulatorMilis()));
      
    }
    
    public void switchImages(String path, boolean show) {

        // find the element with it's id
        element = myNifty.getCurrentScreen().findElementByName("actionIcon"); 

        if(show){
            //Show Element
            element.show();
            // first load the new image
            newImage = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),path, false);
            // change the image with the ImageRenderer
            element.getRenderer(ImageRenderer.class).setImage(newImage);
            element.startEffect(EffectEventId.onCustom);
        }else{
            element.hide();
        }

    }
    
    public void switchImagesWifi(){
      // find the element with it's id
        element = myNifty.getCurrentScreen().findElementByName("wifiIcon"); 
        
        if(myMain.getDb().isInternetConnection()){
            wifiImage = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),"Interface/WifiIconOn.png", false);
            // change the image with the ImageRenderer
            element.getRenderer(ImageRenderer.class).setImage(wifiImage);
            getWifi().setText("ON");
        }else{
            wifiImage = myNifty.getRenderEngine().createImage(myNifty.getCurrentScreen(),"Interface/WifiIconOff.png", false);
            // change the image with the ImageRenderer
            element.getRenderer(ImageRenderer.class).setImage(wifiImage);
            getWifi().setText("OFF");
        }
    }
    
    public void updatePosition(Vector3f v){
      DecimalFormat df = new DecimalFormat("0.0");
      String msg = "x = "+df.format(v.getX())+"\n"
                  +"y = "+df.format(v.getY())+"\n"
                  +"z = "+df.format(v.getZ())+"\n";
      SpacePosition spos = new SpacePosition(myMain);
      msg = msg + spos.getNameSpace(v);
        getPosition().setText(msg);
    }
  
    public void updateEnergyLabel(float energyvalue){
        DecimalFormat df = new DecimalFormat("0.000");
        getEnergy().setText(df.format(energyvalue) + " KWh");
    }

    public void updateWaterLabel(float watervalue){
        DecimalFormat df = new DecimalFormat("0.0");
        getWater().setText(df.format(watervalue) + " L");
    }

    public void updateTemperatureLabel(float temperaturevalue){
        DecimalFormat df = new DecimalFormat("0.0");
        getTemperature().setText(df.format(temperaturevalue) + "ºC");
    }


    /**
     * @return the temperature
     */
    public Label getTemperatureLabel() {
        return getTemperature();
    }

    /**
     * @return the time
     */
    public Label getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Label time) {
        this.time = time;
    }

    /**
     * @return the position
     */
    public Label getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Label position) {
        this.position = position;
    }

    /**
     * @return the worldtime
     */
    public Label getWorldtime() {
        return worldtime;
    }

    /**
     * @param worldtime the worldtime to set
     */
    public void setWorldtime(Label worldtime) {
        this.worldtime = worldtime;
    }

    /**
     * @return the wifi
     */
    public Label getWifi() {
        return wifi;
    }

    /**
     * @param wifi the wifi to set
     */
    public void setWifi(Label wifi) {
        this.wifi = wifi;
    }

    /**
     * @return the energy
     */
    public Label getEnergy() {
        return energy;
    }

    /**
     * @param energy the energy to set
     */
    public void setEnergy(Label energy) {
        this.energy = energy;
    }

    /**
     * @return the water
     */
    public Label getWater() {
        return water;
    }

    /**
     * @param water the water to set
     */
    public void setWater(Label water) {
        this.water = water;
    }

    /**
     * @return the temperature
     */
    public Label getTemperature() {
        return temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(Label temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the timeSimulatorMilis
     */
    public long getTimeSimulatorMilis() {
        return timeSimulatorMilis;
    }

    /**
     * @param timeSimulatorMilis the timeSimulatorMilis to set
     */
    public void setTimeSimulatorMilis(long timeSimulatorMilis) {
        this.timeSimulatorMilis = timeSimulatorMilis;
    }

    /**
     * @return the extratime
     */
    public long getExtratime() {
        return extratime;
    }

    /**
     * @param extratime the extratime to set
     */
    public void setExtratime(long extratime) {
        this.extratime = extratime;
    }
    
    public void addExtratime(long extra){
        setExtratime(extratime+extra);
    }
    
}
