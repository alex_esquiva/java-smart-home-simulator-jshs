/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import Control.PlotGraph;
import com.jme3.asset.DesktopAssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.texture.Image;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import java.io.IOException;
import java.util.ArrayList;
import Simulator.Main;

/**
 *
 * @author Alejandro Esquiva Rodríguez
 *
 * Mail: alejandro@geekytheory.com
 * Twitter: @alex_esquiva
 */
public class MyGraphGui {
    private Main myMain;
    private Nifty nifty;
    Element graphElement;
    private NiftyImage graph;
        
    private boolean statusGraphPanel = false;
    private boolean statusGraphOptionPanel = false;
    
    MyGraphGui(Main myMain, Nifty nifty) {
        this.myMain = myMain;
        this.nifty = nifty;
        
        hideGraphPanel();
        hideGraphOptionPanel();
    }
    
    public void drawGraph(int index) throws IOException{
        myMain.getGui().setStatusGUI(true);
        /*
         * index = 0 -> SHOW ENERGY CONSUMPTION GRAPH
         * index = 1 -> SHOW WATER CONSUMPTION GRAPH
         * index = 2 -> SHOW TEMPERATURE DATA GRAPH
         */
        showGraphPanel();
        //Object of the graph
        PlotGraph t=new PlotGraph(myMain);        
  
        
        ArrayList<String> data = new ArrayList<String>();
        ArrayList<String> time = new ArrayList<String>();
        
        for(int i=0;i<myMain.getGui().getObjectsInfo().size();i++){
            ArrayList<String> object = myMain.getGui().getObjectsInfo().get(i);
            data.add(object.get(4+index));
            time.add(object.get(9));
        }                
        
        String name = "";
        
        if(index == 0)  name = "Electrical Consumption";
        if(index == 1)  name = "Water Consumption";
        if(index == 2)  name = "Temperature";
        
        
        /*
        for(int i=0;i<myMain.getGui().getPhysicalInfo().size();i=i+5){                
            //myMain.getGui().getConsole().output(myMain.getGui().getPhysicalInfo().get(i+index)+"");    
            data.add(myMain.getGui().getPhysicalInfo().get(i+index));   
            time.add(myMain.getGui().getPhysicalInfo().get(i+3));   
        } 
        */
        t.plotChart(name, data, time);
   
        // change the image with the ImageRenderer
        Image img;
        AWTLoader awt = new AWTLoader();
        //load the imagBuffered of the graph into a image object
        img = awt.load(t.getImageBuffered(),false);
        //Creation of textura
        Texture2D texture = new Texture2D(img);
        //Add to cache the image
        int ran = (int)Math.round(Math.random()*1000000);
        TextureKey tk = new TextureKey("chart"+ran+".png");
        ((DesktopAssetManager)myMain.getAssetManager()).addToCache(tk, texture);
        //load the texture in a niftyImage
        graph = nifty.getRenderEngine().createImage(nifty.getCurrentScreen(),"chart"+ran+".png", false);
        //select element and set image.
        graphElement = nifty.getCurrentScreen().findElementByName("graph"); 
        graphElement.getRenderer(ImageRenderer.class).setImage(graph);
    }
    
    public void clearGraph(){
        try{
            graphElement.hide();
        }catch(NullPointerException e){
            
        }
        ((DesktopAssetManager)myMain.getAssetManager()).clearCache();
                
    }
    
    public void hideGraphPanel(){
        setStatusGraphPanel(false);
        nifty.getCurrentScreen().findElementByName("graphPanel").hide();
    }
    
    public void showGraphPanel(){
        setStatusGraphPanel(true);
        nifty.getCurrentScreen().findElementByName("graphPanel").show();
    }

    public void hideGraphOptionPanel(){
        setStatusGraphOptionPanel(false);
        nifty.getCurrentScreen().findElementByName("graphOptionPanel").hide();
    }
    
    public void showGraphOptionPanel(){
        setStatusGraphOptionPanel(true);
        nifty.getCurrentScreen().findElementByName("graphOptionPanel").show();
    }

    /**
     * @return the statusGraphPanel
     */
    public boolean isStatusGraphPanel() {
        return statusGraphPanel;
    }

    /**
     * @param statusGraphPanel the statusGraphPanel to set
     */
    public void setStatusGraphPanel(boolean statusGraphPanel) {
        this.statusGraphPanel = statusGraphPanel;
    }

    public void acceptGraphPanel() {
        myMain.getGui().setStatusGUI(true);
        hideGraphPanel();
    }

    public void cancelGraphOptionPanel() {
        myMain.getGui().setStatusGUI(true);
        hideGraphOptionPanel();
    }

    /**
     * @return the statusGraphOptionPanel
     */
    public boolean isStatusGraphOptionPanel() {
        return statusGraphOptionPanel;
    }

    /**
     * @param statusGraphOptionPanel the statusGraphOptionPanel to set
     */
    public void setStatusGraphOptionPanel(boolean statusGraphOptionPanel) {
        this.statusGraphOptionPanel = statusGraphOptionPanel;
    }

}
