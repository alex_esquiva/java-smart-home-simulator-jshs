/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GUI;

import de.lessvoid.nifty.Nifty;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.KeysInput;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class MyMainMenu {
    private Nifty myNifty;
    private Main myMain;
    
    private boolean statusMainMenu = false;
    
    public MyMainMenu(Main app, Nifty nifty){
        myMain = app;
        myNifty = nifty;
        hideMainMenu();
    }
    
    public void hideMainMenu(){
        setStatusMainMenu(false);
        myNifty.getCurrentScreen().findElementByName("mainMenuPanel").hide();
    }
    
    public void showMainMenu(){
        setStatusMainMenu(true);
        myNifty.getCurrentScreen().findElementByName("mainMenuPanel").show();    
    }

    public void closeMainMenu() {
        hideMainMenu();
        myMain.getGui().hideGUI();
        myMain.getGui().setStatusGUI(false);
    }
    
    public void exitSimulator(){
        myMain.exitSimulator();
    }

    void graphsMainMenu() {
        myMain.getGui().setStatusGUI(true);
        myMain.getGui().getMyGraphGui().showGraphOptionPanel();
    }

    void toolsMainMenu() {
        myMain.getGui().setStatusGUI(true);
        myMain.getGui().getMyControlPanel().showControlPanel();
        /*myMain.getGui().setStatusGUI(false);
        try {
            if(!myMain.getGui().getStatusGUI()){
                myMain.getGui().showBlock("controlpanel");                        
            }else{
                myMain.getGui().hideGUI();                   
            }
        } catch (Exception ex) {
            Logger.getLogger(KeysInput.class.getName()).log(Level.SEVERE, null, ex);
        }
       // myMain.getGui().setStatusGUI(!myMain.getGui().getStatusGUI());
        */
    }

    void actionListMainMenu() {
        myMain.getGui().setStatusGUI(true);
        myMain.getGui().getMyActionPanel().showActionPanel();
    }

    /**
     * @return the statusMainMenu
     */
    public boolean isStatusMainMenu() {
        return statusMainMenu;
    }

    /**
     * @param statusMainMenu the statusMainMenu to set
     */
    public void setStatusMainMenu(boolean statusMainMenu) {
        this.statusMainMenu = statusMainMenu;
    }
}
