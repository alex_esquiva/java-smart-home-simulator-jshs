package GUI;
 
import Control.ObjectControl;
import Control.SimulatorDelay;
import Control.SpacePosition;
import com.jme3.app.Application; 
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Console;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;
 
public class MyGUI extends AbstractAppState implements ScreenController {
 
    private Nifty nifty;
    private Screen screen;
    private Main myMain;
    private Console console;

    private MyMainMenu myMainMenu;
    private MyInfoPanel myInfoPanel;
    private MyGraphGui myGraphGui;
    private MyControlPanel myControlPanel;
    private MyConsole myConsole;
    private MyActionPanel myActionPanel;
    private SimulatorDelay simulatorDelay;

    private Boolean statusGUI=false;

    private ArrayList<ArrayList<String>> objectsInfo = new ArrayList<ArrayList<String>>();
    private ArrayList<String> physicalInfo = new ArrayList<String>();

    /** custom methods */ 

    public MyGUI(Main app) { 
        this.myMain = app;  
        //update physical variables values
        
    } 

    /** Nifty GUI ScreenControl methods */ 

    public void bind(Nifty nifty, Screen screen) {
        this.setNifty(nifty);
        this.screen = screen;
        //INIT INFO PANEL
        setMyInfoPanel(new MyInfoPanel(myMain, nifty));
        //INIT MAIN MENU
        setMyMainMenu(new MyMainMenu(myMain, nifty));
        //INIT CONSOLE
        console = nifty.getCurrentScreen().findNiftyControl("console0", Console.class);
        setMyConsole(new MyConsole(myMain,nifty,console));
        //INIT GRAPH   
        setMyGraphGui(new MyGraphGui(myMain, nifty));
        //INIT CONTROL PANEL
        try {
            setMyControlPanel(new MyControlPanel(myMain, nifty));//Control Panel
        } catch (Exception ex) {
            Logger.getLogger(MyGUI.class.getName()).log(Level.SEVERE, null, ex);
        }  

        myActionPanel = new MyActionPanel(myMain,nifty);
        setSimulatorDelay(new SimulatorDelay(myMain,0,"",""));
        nifty.getCurrentScreen().findElementByName("blackpanel").hide();

    }
    public void onStartScreen(){}
    
    public void onEndScreen(){}
 
    /** jME3 AppState methods */ 

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
      super.initialize(stateManager, app); 
    }

    @Override
    public void update(float tpf) { 
      /** jME update loop! */ 
      myInfoPanel.update(tpf);
      myControlPanel.update(tpf);
      simulatorDelay.update(tpf);
    }

    public SimpleApplication getContext(){
        return myMain;
    }
    /**
     * @return the console
     */
    public Console getConsole() {
        return console;
    }

    /**
     * @param console the console to set
     */
    public void setConsole(Console console) {
        this.console = console;
    }

    
    public void addObjectInfo(ObjectControl control){
        
        final ArrayList<String> info = new ArrayList<String>();
        
        String action_object = "";
        if(control.getAction()){
            action_object = "ON";
        }else{
            action_object = "OFF";
        }
        //Index 0: Name
        info.add(control.getName());
        //Index 1: Code
        info.add(control.getCode());       
        //Index 2: Action_Meaning
        info.add(action_object);
        //Index 3: action bool
        info.add(control.getAction()+"");    
        //Index 4: Energy Consumption
        info.add(myMain.getPhysicalCharacteristics().getEnergyConsumption()+"");    
        //Index 5: Water Consumption
        info.add(myMain.getPhysicalCharacteristics().getWaterConsumption()+"");    
        //Index 6: Temperature Consumption
        info.add(myMain.getPhysicalCharacteristics().getTemperature()+"");    
        //Index 7: place
        String place = new SpacePosition(myMain).getNameSpace(control.getPhysicsLocation());
        if(place.equals("")){
            place = "Not Founded";
        }
        info.add(place);    
        //Index 8: Logo Path
        info.add(control.getLogoPath());    
        //Index 9: Time
        info.add(myInfoPanel.getTime().getText());
        
        objectsInfo.add(info);
        
        myMain.getDb().sendInRealTimeObjectData(info);
        
    }

    
    /**
     * @return the physicalInfo
     */
    public ArrayList<String> getPhysicalInfo() {
        return physicalInfo;
    }
    

    /**
     * @param physicalInfo the physicalInfo to set
     */
    public void setPhysicalInfo(ArrayList<String> physicalInfo) {
        this.physicalInfo = physicalInfo;
    }
    
    /**
     * @return the myGraphGui
     */
    public MyGraphGui getMyGraphGui() {
        return myGraphGui;
    }

    /**
     * @param myGraphGui the myGraphGui to set
     */
    public void setMyGraphGui(MyGraphGui myGraphGui) {
        this.myGraphGui = myGraphGui;
    }

    /**
     * @return the myControlPanel
     */
    public MyControlPanel getMyControlPanel() {
        return myControlPanel;
    }

    /**
     * @param myControlPanel the myControlPanel to set
     */
    public void setMyControlPanel(MyControlPanel myControlPanel) {
        this.myControlPanel = myControlPanel;
    }

    /**
     * @return the myConsole
     */
    public MyConsole getMyConsole() {
        return myConsole;
    }

    /**
     * @param myConsole the myConsole to set
     */
    public void setMyConsole(MyConsole myConsole) {
        this.myConsole = myConsole;
    }
    
    public void buttonAction(String action) throws Exception{
        if(action.equals("updateConfigPanelControl")) myControlPanel.updateConfig();
        if(action.equals("acceptConfigPanelControl")) myControlPanel.acceptConfig();
        if(action.equals("cancelConfigPanelControl")) myControlPanel.cancelConfig();
        
        if(action.equals("graphsMainMenu")) myMainMenu.graphsMainMenu();
        if(action.equals("toolsMainMenu")) myMainMenu.toolsMainMenu();
        if(action.equals("actionListMainMenu")) myMainMenu.actionListMainMenu();
        if(action.equals("closeMainMenu")) myMainMenu.closeMainMenu();
        if(action.equals("exitSimulator")) myMainMenu.exitSimulator();
        
        if(action.equals("acceptGraphPanel")) myGraphGui.acceptGraphPanel();
        if(action.equals("showEnergyConsumptionGraph")) myGraphGui.drawGraph(0);
        if(action.equals("showWaterConsumptionGraph")) myGraphGui.drawGraph(1);
        if(action.equals("showTemperatureGraph")) myGraphGui.drawGraph(2);
        if(action.equals("cancelGraphOptionPanel")) myGraphGui.cancelGraphOptionPanel();
        
        if(action.equals("cancelActionPanel")) myActionPanel.cancelActionPanel();        
    }
    
    public void hideGUI(){
        getMyMainMenu().hideMainMenu();
        getMyControlPanel().hideControlPanel();
        getMyGraphGui().hideGraphOptionPanel();
        getMyGraphGui().hideGraphPanel();
        getMyConsole().hideConsole();
        //Set Drag to Rotate
        myMain.getFlyByCamera().setDragToRotate(false);
        //Set Player Enable
        myMain.getPlayer().setEnabled(true);
    }
    
    public void showBlock(String block) throws IOException{
        hideGUI();
        //Set Drag to Rotate
        myMain.getFlyByCamera().setDragToRotate(true);
        //Set Player Enable
        myMain.getPlayer().setEnabled(false);
        if(block.equals("console")){
            getMyConsole().showConsole();
        }
        
        if(block.equals("graph")){
            getMyGraphGui().drawGraph(0);
        }
        
        if(block.equals("controlpanel")){
            getMyControlPanel().showControlPanel();
        }
        
        if(block.equals("MainMenu")){
            getMyMainMenu().showMainMenu();
        }
    }

    /**
     * @return the statusGUI
     */
    public Boolean getStatusGUI() {
        return statusGUI;
    }

    /**
     * @param statusGUI the statusGUI to set
     */
    public void setStatusGUI(Boolean statusGUI) {
        this.statusGUI = statusGUI;
    }

    /**
     * @return the myMainMenu
     */
    public MyMainMenu getMyMainMenu() {
        return myMainMenu;
    }

    /**
     * @param myMainMenu the myMainMenu to set
     */
    public void setMyMainMenu(MyMainMenu myMainMenu) {
        this.myMainMenu = myMainMenu;
    }

    /**
     * @return the myInfoPanel
     */
    public MyInfoPanel getMyInfoPanel() {
        return myInfoPanel;
    }

    /**
     * @param myInfoPanel the myInfoPanel to set
     */
    public void setMyInfoPanel(MyInfoPanel myInfoPanel) {
        this.myInfoPanel = myInfoPanel;
    }

    /**
     * @return the myActionPanel
     */
    public MyActionPanel getMyActionPanel() {
        return myActionPanel;
    }

    /**
     * @param myActionPanel the myActionPanel to set
     */
    public void setMyActionPanel(MyActionPanel myActionPanel) {
        this.myActionPanel = myActionPanel;
    }

    /**
     * @return the objectsInfo
     */
    public ArrayList<ArrayList<String>> getObjectsInfo() {
        return objectsInfo;
    }

    /**
     * @param objectsInfo the objectsInfo to set
     */
    public void setObjectsInfo(ArrayList<ArrayList<String>> objectsInfo) {
        this.objectsInfo = objectsInfo;
    }

    /**
     * @return the nifty
     */
    public Nifty getNifty() {
        return nifty;
    }

    /**
     * @param nifty the nifty to set
     */
    public void setNifty(Nifty nifty) {
        this.nifty = nifty;
    }

    /**
     * @return the simulatorDelay
     */
    public SimulatorDelay getSimulatorDelay() {
        return simulatorDelay;
    }

    /**
     * @param simulatorDelay the simulatorDelay to set
     */
    public void setSimulatorDelay(SimulatorDelay simulatorDelay) {
        this.simulatorDelay = simulatorDelay;
    }
      
}

