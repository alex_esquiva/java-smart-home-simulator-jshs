/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBase;

import ObjectSource.Dishwasher;
import ObjectSource.Door;
import ObjectSource.DoorExterior;
import ObjectSource.Extractor;
import ObjectSource.Fan;
import ObjectSource.Fridge;


import ObjectSource.MainDoor;
import ObjectSource.House;
import ObjectSource.LightBasic;
import ObjectSource.Microwave;
import ObjectSource.MyTerrain;
import ObjectSource.Oven;
import ObjectSource.Radiator;
import ObjectSource.SmokeStack;
import ObjectSource.WashingMachine;
import ObjectSource.WindowBig;
import ObjectSource.WindowLarge;
import ObjectSource.WindowLargeV2;
import ObjectSource.WindowMedium;
import ObjectSource.WindowSmall;


import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import Simulator.Main;
import Simulator.MyConfig;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
/**
 *
 * @author IdeaPad Z500
 */
public class ObjectsDB{
    
    private final String USER_AGENT = "Mozilla/5.0";
    
    private URL obj;
    private HttpURLConnection con;
    private BufferedReader in;
    private StringBuffer response;
    private String session="";
    private boolean internetConnection=false;
    private boolean senData=false;
    private Main myMain;
    
    public ObjectsDB(){
        
    }
    public ObjectsDB(Main app){
        myMain = app;
        
        float intCone = 0;
        
        try{
             intCone = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.GENERAL_XML_PATH, "INTERNET_CONNECTION"));
        }catch(Exception e){
            
        }
        
        if(intCone == 1){
            internetConnection=true;
        }else{
            internetConnection=false;
        }
        
        try{
             intCone = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.GENERAL_XML_PATH, "SEND_DATA"));
        }catch(Exception e){
            
        }
        
        if(intCone == 1){
            senData=true;
        }else{
            senData=false;
        }
        
        updateSession();
        tryConnection();
        if(isInternetConnection()){
            sendNameAndSession();            
        }
    }
    
    public void insert(String table, String JSONString){
               
        String action = "";
        
        if(table.equals("Objects")){
            action = "Insert_Objects";
        }
        
        if(table.equals("ObjectsData")){
            action = "Insert_ObjectsData";
        }
        
        if(table.equals("ScenaryData")){
            action = "Insert_ScenaryData";
        }
        
        if(table.equals("Sessions")){
            action = "Insert_Sessions";
        }
        
        if(table.equals("Close_Session")){
            action = "Close_Session";
        }
        
        if(action!=""){
            try {

                JSONString = URLEncoder.encode(JSONString, "UTF-8");
                String url = MyConfig.SERVER_PATH+"/action.php";

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                String urlParameters = "action="+action+"&json="+JSONString;

                // Send post request
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                String outputmsg = "Sending "+action+" || Code Response: "+responseCode;
                System.out.println(outputmsg);
                /*System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);
                */
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                System.out.println("Response: "+response);
                System.out.println();
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void sendObjectsInfo() throws Exception{
        
        ArrayList<String> objects = myMain.getMyConfig().getPhysicObjectsConfig();
        for(int i=0;i<objects.size();i++){
            System.out.println(objects.get(i));
        }
        
        JSONObject[] jsonObj = new JSONObject[objects.size()];
        List  l = new LinkedList();
        
        int count =0;
        
        for(int i=0;i<objects.size();i++){  


            String speed = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "SPEED");
            String energyOn = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "ENERGY_CONSUMPTION_ON");
            String energyOff = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "ENERGY_CONSUMPTION_OFF");
            String waterOn = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "WATER_CONSUMPTION_ON");
            String waterOff = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "WATER_CONSUMPTION_OFF");
            String temperatureOn = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "TEMPERATURE_ON");
            String temperatureOff = myMain.getMyConfig().findPhysicValueXML(objects.get(i), "TEMPERATURE_OFF");
            
            if(energyOn != ""){
                jsonObj[count] = new JSONObject();
                jsonObj[count].put("name",objects.get(i));
                jsonObj[count].put("code","000");
                jsonObj[count].put("speed", speed);
                jsonObj[count].put("energyOn", energyOn);
                jsonObj[count].put("energyOff", energyOff);
                jsonObj[count].put("waterOn", waterOn);
                jsonObj[count].put("waterOff", waterOff);
                jsonObj[count].put("temperatureOn", temperatureOn);
                jsonObj[count].put("temperatureOff", temperatureOff);
                l.addAll(Arrays.asList(jsonObj));
                count++;
            }
        }
        String jsonString = JSONValue.toJSONString(l);
        final String jsonString2 = jsonString.replace(",null", "");
        System.out.println(jsonString2);
        // Thread
        new Thread(new Runnable() {
            @Override public void run() {
                insert("Objects", jsonString2);
            }
        }).start();
    }
    
    public void sendInRealTimeObjectData(ArrayList<String> data){
        JSONObject[] jsonObj = new JSONObject[1];
        List  l = new LinkedList();
        int count =0;    
        jsonObj[count] = new JSONObject();
        
        DecimalFormat df = new DecimalFormat("0.000");
        
        String energy = df.format(Float.parseFloat(data.get(4)));
        energy = energy.replace(",", ".");
        
        String water = df.format(Float.parseFloat(data.get(5)));
        water = water.replace(",", ".");
        
        String temperature = df.format(Float.parseFloat(data.get(6)));
        temperature = temperature.replace(",", ".");
        
        jsonObj[count].put("session",session);
        jsonObj[count].put("name",data.get(0));
        jsonObj[count].put("code", data.get(1));
        jsonObj[count].put("status", data.get(3));
        jsonObj[count].put("energy", energy);
        jsonObj[count].put("water", water);
        jsonObj[count].put("temperature", temperature);
        jsonObj[count].put("place", data.get(7));
        jsonObj[count].put("iconpath", data.get(8));
        jsonObj[count].put("sim_time",data.get(9));
        count++;
        l.addAll(Arrays.asList(jsonObj));
        final String jsonString = JSONValue.toJSONString(l);
        // Thread
        new Thread(new Runnable() {
            @Override public void run() {
                insert("ObjectsData", jsonString);
            }
        }).start();
    }
    
    
    public void sendNameAndSession(){
        
        if(!session.equals("")){
            JSONObject[] jsonObj = new JSONObject[1];
            List  l = new LinkedList();
            int count =0;    

            jsonObj[count] = new JSONObject();
            jsonObj[count].put("session",session);
            jsonObj[count].put("name", "Alejandro Esquiva");

            l.addAll(Arrays.asList(jsonObj));
            String jsonString = JSONValue.toJSONString(l);
            insert("Sessions", jsonString);
        }
    }
    
    public void closeSession(){
        if(!session.equals("")){
            JSONObject[] jsonObj = new JSONObject[1];
            List  l = new LinkedList();
            int count =0;    

            jsonObj[count] = new JSONObject();
            jsonObj[count].put("session",session);
            jsonObj[count].put("name", "Alejandro Esquiva");

            l.addAll(Arrays.asList(jsonObj));
            String jsonString = JSONValue.toJSONString(l);
            insert("Close_Session", jsonString);
        }
    }
    
    public void insert(String name,String code,String data,String time){
        
        String url = MyConfig.SERVER_PATH+"/insert.php?session="+session+"&name="+name+"&code="+code+"&data="+data+"&time="+time;
        
        url = url.replaceAll(" ", "%20");
        try {
            obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public int updateSession(){
        //Check internet
        tryConnection();
        int session_number = 0;
        if(isInternetConnection()){       
            String url = MyConfig.SERVER_PATH+"/getSession.php";  
            url = url.replaceAll(" ", "%20");
                
            try {
                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();
                // optional default is GET
                con.setRequestMethod("GET");
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                int responseCode = con.getResponseCode();
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                }
                in.close();    
                session = (Integer.parseInt(response.toString())+1)+"";
                session_number = (Integer.parseInt(response.toString())+1);
                //setSession((Integer.parseInt(response.toString())+1)+"");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
            return session_number;
    }
    
    public boolean tryConnection(){
        if(internetConnection){
            String dirWeb = MyConfig.SERVER_PATH;
            int puerto = 80;

            try{
                Socket s = new Socket(dirWeb, puerto);
                if(s.isConnected()){
                    setInternetConnection(true);
                }
            }catch(Exception e){
                    setInternetConnection(false);
            }
        }else{
            setInternetConnection(false);
        }
        System.out.println("TRY CONNECTION");
        System.out.println("INTERNET CONNECTION "+ isInternetConnection());
        
        //return isInternetConnection();
        /************TO CHANGE*******************/
        setInternetConnection(true);
        return true;
  }
    
    private void insertPhysicalData(ArrayList<String> physicalInfo) throws UnsupportedEncodingException {
        /*//get Seasson
        updateSession();
        JSONObject[] jsonObj = new JSONObject[physicalInfo.size()/5];
        List  l = new LinkedList();
        int count =0;    
        for(int i =0;i<physicalInfo.size();i=i+5){   
            jsonObj[count] = new JSONObject();
            Map m2 = new HashMap();
            jsonObj[count].put("session",session);
            jsonObj[count].put("eConsumption", physicalInfo.get(i));
            jsonObj[count].put("wConsumption", physicalInfo.get(i+1));
            jsonObj[count].put("tData", physicalInfo.get(i+2));
            jsonObj[count].put("simulatorTime", physicalInfo.get(i+3));
            jsonObj[count].put("globalTime", physicalInfo.get(i+4));
            count++;
        }

        l.addAll(Arrays.asList(jsonObj));
        String jsonString = JSONValue.toJSONString(l); 
        jsonString = URLEncoder.encode(jsonString, "UTF-8");
        //String decodedUrl = URLDecoder.decode(url, "UTF-8");
        String url = MyConfig.SERVER_PATH+"JSHS/insertPhysicalJson.php";
        //url = url.replaceAll(" ", "%20");
        //url = url.replaceAll("\"", "\\");
        System.out.println(url);
        try {
            obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "json="+jsonString;

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);

            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }
    
    public void sessionConsolePrint(int session){
         //Check internet
        tryConnection();
        if(isInternetConnection()){       
            String url = MyConfig.SERVER_PATH+"/getInfo.php?session="+session;  
            url = url.replaceAll(" ", "%20");

            try {
                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();
                // optional default is GET
                con.setRequestMethod("GET");
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                int responseCode = con.getResponseCode();
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                        //response.append(inputLine);
                        myMain.getGui().getConsole().output(inputLine);
                }
                in.close();            
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            myMain.getGui().getConsole().output("No internet connection");
        }
    }
    
    public void getSessionInfo(){
        //Check internet
        tryConnection();
        if(isInternetConnection()){       
            String url = MyConfig.SERVER_PATH+"/?action=console";  
            url = url.replaceAll(" ", "%20");

            try {
                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();
                // optional default is GET
                con.setRequestMethod("GET");
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                int responseCode = con.getResponseCode();
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                        //response.append(inputLine);
                        myMain.getGui().getConsole().output(inputLine);
                }
                in.close();            
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            myMain.getGui().getConsole().output("No internet connection");
        }
    }
    
    public void saveScenaryDataBase() throws UnsupportedEncodingException {
    /*   
        
        ArrayList dataScenary = new ArrayList();
        for(int i = 0;i<myMain.getRootNode().getChildren().size();i++){
            
            try{
                //try get the control, if there are a exception continue the loop
                myMain.getRootNode().getChildren().get(i).getControl(ObjectControl.class).getName();
            }catch(NullPointerException e){
                continue;
            }
            
            //Add Name
            dataScenary.add(myMain.getRootNode().getChildren().get(i).getControl(ObjectControl.class).getName()+"");
            //Add code
            dataScenary.add(myMain.getRootNode().getChildren().get(i).getControl(ObjectControl.class).getCode()+"");
            //Add Position
            dataScenary.add(myMain.getRootNode().getChildren().get(i).getLocalTranslation().toString());
            //Add Rotation
            dataScenary.add(myMain.getRootNode().getChildren().get(i).getLocalRotation().toString());
            //Add Action Status
            dataScenary.add(myMain.getRootNode().getChildren().get(i).getControl(ObjectControl.class).getAction()+"");

        }
        System.out.println(dataScenary.size());
        if(isInternetConnection()){        
            //get Seasson
            updateSession();
            JSONObject[] jsonObj = new JSONObject[dataScenary.size()/5];
            List  l = new LinkedList();
            int count =0;    
            for(int i =0;i<dataScenary.size();i=i+5){
                System.out.println(count);
                jsonObj[count] = new JSONObject();
                Map m2 = new HashMap();
                jsonObj[count].put("session",session);
                jsonObj[count].put("name", dataScenary.get(i));
                jsonObj[count].put("code", dataScenary.get(i+1));
                jsonObj[count].put("position", dataScenary.get(i+2));
                jsonObj[count].put("rotation", dataScenary.get(i+3));
                jsonObj[count].put("status", dataScenary.get(i+4));
                count++;
            }

            l.addAll(Arrays.asList(jsonObj));
            String jsonString = JSONValue.toJSONString(l); 
            System.out.println(jsonString);
            jsonString = URLEncoder.encode(jsonString, "UTF-8");
            
            String url = MyConfig.SERVER_PATH+"JSHS/insertScenaryJson.php";

            try{
		obj = new URL(url);
		con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
 
		String urlParameters = "json="+jsonString;
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
            }catch(Exception e){
                e.printStackTrace();
            }
            //print result
            System.out.println(response.toString());
        }
        */
    }
    public void loadScenaryDataBase(int session) throws UnsupportedEncodingException {
        //Erase Root Node
        myMain.getBulletAppState().getPhysicsSpace().removeAll(myMain.getRootNode());
        myMain.getRootNode().detachAllChildren();
        
         //Check internet
        tryConnection();
        if(isInternetConnection()){       
            String url = MyConfig.SERVER_PATH+"/getScenary.php?session="+session;  
            url = url.replaceAll(" ", "%20");

            try {
                obj = new URL(url);
                con = (HttpURLConnection) obj.openConnection();
                // optional default is GET
                con.setRequestMethod("GET");
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                int responseCode = con.getResponseCode();
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                }
                in.close();            
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Salida: "+response.toString());
        Object jsonObject =JSONValue.parse(response.toString());
        JSONArray array=(JSONArray)jsonObject;
        for(int i=0;i<array.size();i++){
            myMain.getGui().getConsole().output("======the "+(i+1)+"nd element of array======");
            myMain.getGui().getConsole().output(array.get(i)+"");
        
            JSONObject row=(JSONObject)array.get(i);
            String name = row.get("name").toString();
            String code = row.get("code").toString();
            String position = row.get("position").toString();
            String rotation = row.get("rotation").toString();
            String status = row.get("status").toString();
            
            myMain.getGui().getConsole().output("Name: "+row.get("name"));
            myMain.getGui().getConsole().output("Code: "+row.get("code"));
            myMain.getGui().getConsole().output("Position: "+row.get("position"));
            myMain.getGui().getConsole().output("Rotation: "+row.get("rotation"));
            myMain.getGui().getConsole().output("Status: "+row.get("status"));
            
            createObject(name,code,position,rotation,status);
        }        
        
    }

    private void createObject(String name, String code, String position, String rotation, String status) {
        
        Vector3f myposition = new Vector3f();
        Quaternion myrotation = new Quaternion();
        boolean mystatus = Boolean.parseBoolean(status);
        int mycode = Integer.parseInt(code);
        
        //Get the position
        float x;
        float y;
        float z;
        
        position = position.replace("(","");
        position = position.replace(")","");
        String []split1 = position.split(", ");
        x = Float.parseFloat(split1[0]);
        y = Float.parseFloat(split1[1]);
        z = Float.parseFloat(split1[2]);
        
        //Set position
        myposition.setX(x);
        myposition.setY(y);
        myposition.setZ(z);
        
        myMain.getGui().getConsole().output(myposition.toString());
        
        //Get The Rotation
        float w;
        rotation = rotation.replace("(","");
        rotation = rotation.replace(")","");
        String []split2 = rotation.split(", ");
        x = Float.parseFloat(split2[0]);
        y = Float.parseFloat(split2[1]);
        z = Float.parseFloat(split2[2]);
        w = Float.parseFloat(split2[3]);
        //Set Rotation
        myrotation.set(x, y, z, w);
        myMain.getGui().getConsole().output(myrotation.toString());
        
        float rotationFloat = 0f;
        
        if(y == -0.7191712f || y == 0.0f){
            rotationFloat = 0f;
        }
        
        if(Math.round(y) == 1f){
            rotationFloat = (float) Math.PI;
        }
        if(y == -0.70710677f){
            rotationFloat = (float) -Math.PI/2;
        }
        if(y == 0.70710677f){
            rotationFloat = (float) Math.PI/2;
        }
        
        
        switch (mycode) {
            case 0:  new MyTerrain(myMain);
                     break;  
            case 1:  new House(myMain);
                     break;
            case 2:  Door door = new Door(myMain, myposition, rotationFloat);
                     door.getRigidSpatial().setAction(mystatus);
                     break;
            case 3:  Fan fan = new Fan(myMain, myposition);
                     fan.getRigidSpatial().setAction(mystatus);
                     break;
            case 4:  Fridge fridge = new Fridge(myMain, myposition,rotationFloat);
                     fridge.getRigidSpatial().setAction(mystatus);
                     break;
            case 5:  new LightBasic(myMain, myposition);
                     break;
            case 6:  //new LightFloorTraditional(myMain, myposition);
                     break;
            case 7:  Microwave microwave = new Microwave(myMain, myposition,rotationFloat);
                     microwave.getRigidSpatial().setAction(mystatus);
                     break;
            case 8:  new SmokeStack(myMain, myposition);
                     break;
            case 9:  //new Tv(myMain, myposition);
                     break;  
            case 10:  new WindowSmall(myMain, myposition);
                     break;
            case 11: new WindowBig(myMain, myposition,rotationFloat);
                     break;
            case 12: new WindowLarge(myMain, myposition,rotationFloat);
                     break;
            case 13: new WindowMedium(myMain, myposition,rotationFloat);
                     break;
            case 14: new WindowLargeV2(myMain, myposition,rotationFloat);
                     break;
            case 16: Dishwasher dishwasher = new Dishwasher(myMain, myposition,rotationFloat);
                     dishwasher.getRigidSpatial().setAction(mystatus);
                     break;
            case 17: Extractor extractor = new Extractor(myMain, myposition,rotationFloat);
                     extractor.getRigidSpatial().setAction(mystatus);
                     break;
            case 18: new MainDoor(myMain, myposition,rotationFloat);
                     break;
            case 19: new DoorExterior(myMain, myposition,rotationFloat);
                     break;
            case 22: Oven oven = new Oven(myMain, myposition,rotationFloat);
                     oven.getRigidSpatial().setAction(mystatus);
                     break;
            case 23: Radiator radiator = new Radiator(myMain, myposition,rotationFloat);
                     radiator.getRigidSpatial().setAction(mystatus);
                     break;
            case 26: WashingMachine washingMachine = new WashingMachine(myMain, myposition,rotationFloat);
                     washingMachine.getRigidSpatial().setAction(mystatus);
                     break;
            default: 
                     break;
        }
        
        
    }

    public void sendAllInfo(ArrayList<ArrayList<String>> objectsInfo) throws UnsupportedEncodingException {
        
        if(senData){
            tryConnection();
            //updateSession();
            if(objectsInfo.size()>0 && isInternetConnection()){
                //sendAllObjectData();
            }
            if(isInternetConnection()){
                //saveScenaryDataBase();
            }
        }
    }
    public void sendAllPhysicalInfo(ArrayList<String> physicalInfo) throws UnsupportedEncodingException {
        if(senData){
            tryConnection();
            //updateSession();
            if(physicalInfo.size()>0 && isInternetConnection()){
                insertPhysicalData(physicalInfo);
            }
        }
    }

    /**
     * @return the session
     */
    public String getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * @return the internetConnection
     */
    public boolean isInternetConnection() {
        return internetConnection;
    }

    /**
     * @param internetConnection the internetConnection to set
     */
    public void setInternetConnection(boolean internetConnection) {
        this.internetConnection = internetConnection;
    }

    /**
     * @return the senData
     */
    public boolean isSenData() {
        return senData;
    }

    /**
     * @param senData the senData to set
     */
    public void setSenData(boolean senData) {
        this.senData = senData;
    }
}
