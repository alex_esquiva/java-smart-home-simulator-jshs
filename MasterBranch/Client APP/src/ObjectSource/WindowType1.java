package ObjectSource;

import Control.ObjectControl;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;
import Simulator.MyConfig;

/**
 * 
 * @author Alejandro Esquiva Rodríguez
 * 
 * 
 */
public class WindowType1 {
    
    //Show Object?
    static final boolean SHOW = true;
    //Object Name
    static final String NAME = "WindowType1";
    //Where is the model?
    static final String PATH_MODEL1 = "Models/WindowType1/WindowType1Part1/WindowType1Part1.mesh.j3o";
    static final String PATH_MODEL2 = "Models/WindowType1/WindowType1Part2/WindowType1Part2.mesh.j3o";
    //Object Code
    static final String CODE = "055";
    //Icon Path example: Interface/FanIcon.png
    static final String PATH_ACTION_ICON = "Interface/WindowIcon.png";
    //Init state
    static final boolean INIT_ACTION = false;
    //jME object to represent the object
    private CollisionShape sceneSpatial;
    //Object Control, child of ObjectControl
    private WindowType1Control rigidSpatial;
    //Object Position
    private Vector3f position;
    //Led position, put (0,0,0) to disable led
    private final Vector3f ledPosition = new Vector3f(0, 0, 0);
    //Instance of Main class
    private final Main myMain;
    //Object Rotation
    private float rotation = 0f;
    
    /**
     * Constructor
     * @param app main class instance
     * @param pos vector3f position
     */
    public WindowType1(Main app, Vector3f pos) {
        myMain = app;
        position = pos;
        if(SHOW)initObjects();
    }
    /**
     * 
     * @param app main class instance
     * @param pos vector3f position
     * @param rotation rotation in rad
     */
     public WindowType1(Main app, Vector3f pos, float rotation) {
        myMain = app; 
        position = pos;
        this.rotation = rotation;
        if(SHOW)initObjects();
    }
    /**
     * Init the objects, init spatial object, put the scale and set the spatial.
     */
    private void initObjects(){        

        Spatial myspatial = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL1);    
        myspatial.scale(MyConfig.SCALE);
        setSpatial1(myspatial, (Vector3f) position);
        myspatial = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL2);
        myspatial.scale(MyConfig.SCALE);
        setSpatial2(myspatial, (Vector3f) position);
    }
      
     public void setSpatial1(Spatial myspatial,Vector3f position){
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);     
        RigidBodyControl rigidSpatial = new RigidBodyControl(sceneSpatial);
        rigidSpatial.setKinematic(true);
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        myMain.getRootNode().attachChild(myspatial);
        myMain.getBulletAppState().getPhysicsSpace().add(rigidSpatial); 
    }
      
      public void setSpatial2(Spatial myspatial,Vector3f position){
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);     
        WindowType1Control rigidSpatial = new WindowType1Control(sceneSpatial,myMain);
        rigidSpatial.setKinematic(true);
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        myMain.getRootNode().attachChild(myspatial);
        myMain.getBulletAppState().getPhysicsSpace().add(rigidSpatial); 
    }

    /**
     * @return the position
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the rigidSpatial
     */
    public WindowType1Control getRigidSpatial() {
        return rigidSpatial;
    }

    /**
     * @param rigidSpatial the rigidSpatial to set
     */
    public void setRigidSpatial(WindowType1Control rigidSpatial) {
        this.rigidSpatial = rigidSpatial;
    
    }        
    /**
     *Object Control, child of ObjectControl.java, we use all the native methods of ObjectControl
     */
    public class WindowType1Control extends ObjectControl{
        /**
         * Constructor
         * 
         * @param sceneDoor
         * @param main 
         */
        boolean ini = true;
        float initPosition; 
        boolean antiLoops = false;
        
        public WindowType1Control(CollisionShape sceneDoor,Main main) {
            super(sceneDoor,main);
            //Set Name
            this.setName(NAME);
            //Set Logo Path
            this.setLogoPath(PATH_ACTION_ICON);
            //Set Code
            this.setCode(CODE);
        }
        /**
         * Update thread
         * @param tpf velocity
         */
        @Override
        public void update(float tpf) {
            super.update(tpf);
                
            if(ini){
                initPosition = spatial.getLocalTranslation().getY();
                ini = false;
            }

            if(this.getAction()){           
                if(spatial.getLocalTranslation().getY()<initPosition+3.5f){
                    spatial.move(0, this.getSpeed()*tpf,0);
                }
            }else{
                if(spatial.getLocalTranslation().getY()>initPosition){
                    spatial.move(0, -this.getSpeed()*tpf,0);
                }
            }
        }    
    } 
}
