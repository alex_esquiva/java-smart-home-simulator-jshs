package ObjectSource;

import Control.ObjectControl;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;
import Simulator.MyConfig;

/**
 * 
 * @author Alejandro Esquiva Rodríguez
 * 
 * 
 */
public class Computer {
    
    //Show Object?
    static final boolean SHOW = true;
    //Object Name
    static final String NAME = "Computer";
    //Where is the model?
    static final String PATH_MODEL = "Models/Computer/Computer.mesh.j3o";
    //Object Code
    static final String CODE = "853";
    //Icon Path example: Interface/FanIcon.png
    static final String PATH_ACTION_ICON = "Interface/ComputerIcon.png";
    //Init state
    static final boolean INIT_ACTION = false;
    //jME object to represent the object
    private CollisionShape sceneSpatial;
    //Object Control, child of ObjectControl
    private ComputerControl rigidSpatial;
    //Object Position
    private Vector3f position;
    //Led position, put (0,0,0) to disable led
    private final Vector3f ledPosition = new Vector3f(0, 0, 0);
    //Instance of Main class
    private final Main myMain;
    //Object Rotation
    private float rotation = 0f;
    
    /**
     * Constructor
     * @param app main class instance
     * @param pos vector3f position
     */
    public Computer(Main app, Vector3f pos) {
        myMain = app;
        position = pos;
        if(SHOW)initObjects();
    }
    /**
     * 
     * @param app main class instance
     * @param pos vector3f position
     * @param rotation rotation in rad
     */
     public Computer(Main app, Vector3f pos, float rotation) {
        myMain = app; 
        position = pos;
        this.rotation = rotation;
        if(SHOW)initObjects();
    }
    /**
     * Init the objects, init spatial object, put the scale and set the spatial.
     */
    private void initObjects(){
      
      //Create spatial to move/rotate/change
      Spatial myspatial = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL);    
      //Change scale
      myspatial.scale(MyConfig.SCALE);
      //config spatial to rootNode
      setSpatial(myspatial, position);

    }
    /**
     * Set Spatial into rootNode
     * @param myspatial spatial object
     * @param position position
     */
    public void setSpatial(Spatial myspatial,Vector3f position){        
        //create the rigid object
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);
        //creating the control
        rigidSpatial = new ComputerControl(sceneSpatial,myMain);
        rigidSpatial.setKinematic(true);
        //put led where we want
        if(ledPosition.getX()!=0 || ledPosition.getY()!=0 || ledPosition.getZ()!=0){
            rigidSpatial.setLedPosition(position.add(ledPosition));
        }
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        myspatial.setLocalRotation(new Quaternion().fromAngles(0f,(float) rotation, 0f));
        //atach spatial to rootnode
        myMain.getRootNode().attachChild(myspatial);
        myMain.getBulletAppState().getPhysicsSpace().add(rigidSpatial); 
    }

    /**
     * @return the position
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the rigidSpatial
     */
    public ComputerControl getRigidSpatial() {
        return rigidSpatial;
    }

    /**
     * @param rigidSpatial the rigidSpatial to set
     */
    public void setRigidSpatial(ComputerControl rigidSpatial) {
        this.rigidSpatial = rigidSpatial;
    
    }        
    /**
     *Object Control, child of ObjectControl.java, we use all the native methods of ObjectControl
     */
    public class ComputerControl extends ObjectControl{
        /**
         * Constructor
         * 
         * @param sceneDoor
         * @param main 
         */
        
        private boolean antiloop = true;
        
        public ComputerControl(CollisionShape sceneDoor,Main main) {
            super(sceneDoor,main);
            //Set Name
            this.setName(NAME);
            //Set Logo Path
            this.setLogoPath(PATH_ACTION_ICON);
            //Set Code
            this.setCode(CODE);
        }
        /**
         * Update thread
         * @param tpf velocity
         */
        @Override
        public void update(float tpf) {
            super.update(tpf);
            //Here we have to create the control, for example the movement of the object
            if(this.getAction()==true){
                if(antiloop){
                    String msg = "*You are using the computer 1 hour\n\n*You have cosumed "+super.getEnergyConsumptionON()+ "W";
                    myMain.getGui().getSimulatorDelay().execute(this,3000, "Using "+NAME+"...",msg,1000*60*60/*1 hour*/);
                    antiloop = !antiloop;
                }
            }else{
                antiloop = true;
            }
        }    
    } 
}
