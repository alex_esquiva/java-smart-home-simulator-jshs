package ObjectSource;

import Control.ObjectControl;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import Simulator.Main;
import Simulator.MyConfig;

/**
 * 
 * @author Alejandro Esquiva Rodríguez
 * 
 * 
 */
public class LightBasic {
    
    //Show Object?
    static final boolean SHOW = true;
    //Object Name
    static final String NAME = "LightBasic";
    //Where is the model?
    static final String PATH_MODEL = "Models/LightBasic/LightBasic.mesh.j3o";
    //Object Code
    static final String CODE = "005";
    //Icon Path example: Interface/FanIcon.png
    static final String PATH_ACTION_ICON = "Interface/LightIcon.png";
    //Init state
    static final boolean INIT_ACTION = false;
    //jME object to represent the object
    private CollisionShape sceneSpatial;
    //Object Control, child of ObjectControl
    private LightBasicControl rigidSpatial;
    //Object Position
    private Vector3f position;
    //Led position, put (0,0,0) to disable led
    private final Vector3f ledPosition = new Vector3f(0.59143776f, 0.12699986f, -0.4122734f);
    //Instance of Main class
    private final Main myMain;
    //Object Rotation
    private float rotation = 0f;
    
    private PointLight light;
    boolean antiLoops = false;
    
    /**
     * Constructor
     * @param app main class instance
     * @param pos vector3f position
     */
    public LightBasic(Main app, Vector3f pos) {
        myMain = app;
        position = pos;
        if(SHOW)initObjects();
    }
    /**
     * 
     * @param app main class instance
     * @param pos vector3f position
     * @param rotation rotation in rad
     */
     public LightBasic(Main app, Vector3f pos, float rotation) {
        myMain = app; 
        position = pos;
        this.rotation = rotation;
        if(SHOW)initObjects();
    }
    /**
     * Init the objects, init spatial object, put the scale and set the spatial.
     */
    private void initObjects(){
      
      //Create spatial to move/rotate/change
      Spatial myspatial = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL);    
      //Change scale
      myspatial.scale(MyConfig.SCALE);
      //config spatial to rootNode
      setSpatial(myspatial, position);

    }
    /**
     * Set Spatial into rootNode
     * @param myspatial spatial object
     * @param position position
     */
    public void setSpatial(Spatial myspatial,Vector3f position){        
        //create the rigid object
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);
        //creating the control
        rigidSpatial = new LightBasicControl(sceneSpatial,myMain);
        rigidSpatial.setKinematic(true);
        //put led where we want
        rigidSpatial.setLedPosition(position.add(ledPosition));
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        myspatial.setLocalRotation(new Quaternion().fromAngles(0f,(float) rotation, 0f));
        //atach spatial to rootnode
        myMain.getRootNode().attachChild(myspatial);
        myMain.getBulletAppState().getPhysicsSpace().add(rigidSpatial); 
    }

    /**
     * @return the position
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the rigidSpatial
     */
    public LightBasicControl getRigidSpatial() {
        return rigidSpatial;
    }

    /**
     * @param rigidSpatial the rigidSpatial to set
     */
    public void setRigidSpatial(LightBasicControl rigidSpatial) {
        this.rigidSpatial = rigidSpatial;
    
    }        
    /**
     *Object Control, child of ObjectControl.java, we use all the native methods of ObjectControl
     */
    public class LightBasicControl extends ObjectControl{
        /**
         * Constructor
         * 
         * @param sceneDoor
         * @param main 
         */
        public LightBasicControl(CollisionShape sceneDoor,Main main) {
            super(sceneDoor,main);
            //Set Name
            this.setName(NAME);
            //Set Logo Path
            this.setLogoPath(PATH_ACTION_ICON);
            //Set Code
            this.setCode(CODE);
            initLight();
        }
        /**
         * Update thread
         * @param tpf velocity
         */
        @Override
        public void update(float tpf) {
            super.update(tpf);
        
            if(this.getAction()==true){
               if(!antiLoops){
                    lightOn();
                    antiLoops=true;
               }
            }else{
                if(antiLoops){
                    lightOff();
                    antiLoops=false;
                }
           }
       }
    
        private void initLight() {
            light = new PointLight();
            light.setName("light");
            light.setRadius(100f);
            light.setColor(ColorRGBA.White.mult(0.2f));
       }
        
        public void lightOn(){
            light.setPosition(new Vector3f(spatial.getLocalTranslation().getX(),spatial.getLocalTranslation().getY()-3,spatial.getLocalTranslation().getZ()));
            myMain.getRootNode().addLight(light);
        }
        
        public void lightOff(){
            myMain.getRootNode().removeLight(light);
        }
    } 
}
