package ObjectSource;

import Control.ObjectControl;
import static ObjectSource.Door.NAME;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;
import Simulator.MyConfig;

/**
 * 
 * @author Alejandro Esquiva Rodríguez
 * 
 * 
 */
public class DoorExterior {
    
    //Show Object?
    static final boolean SHOW = true;
    //Object Name
    static final String NAME = "DoorExterior";
    //Where is the model?
    static final String PATH_MODEL = "Models/DoorExterior/DoorExterior.mesh.j3o";
    //Object Code
    static final String CODE = "019";
    //Icon Path example: Interface/FanIcon.png
    static final String PATH_ACTION_ICON = "Interface/DoorIcon.png";
    //Init state
    static final boolean INIT_ACTION = false;
    //jME object to represent the object
    private CollisionShape sceneSpatial;
    //Object Control, child of ObjectControl
    private DoorExteriorControl rigidSpatial;
    //Object Position
    private Vector3f position;
    //Led position, put (0,0,0) to disable led
    private final Vector3f ledPosition = new Vector3f(0, 0, 0);
    //Instance of Main class
    private final Main myMain;
    //Object Rotation
    private float rotation = 0f;
    
    private float iniangle = 0f;
    private float counterAngle = 0f,angleUp=0f;
    boolean antiLoops = false;
    /**
     * Constructor
     * @param app main class instance
     * @param pos vector3f position
     */
    public DoorExterior(Main app, Vector3f pos) {
        myMain = app;
        position = pos;
        if(SHOW)initObjects();
    }
    /**
     * 
     * @param app main class instance
     * @param pos vector3f position
     * @param rotation rotation in rad
     */
     public DoorExterior(Main app, Vector3f pos, float rotation) {
        myMain = app; 
        position = pos;
        this.rotation = rotation;
        if(SHOW)initObjects();
    }
    /**
     * Init the objects, init spatial object, put the scale and set the spatial.
     */
    private void initObjects(){
      
      //Create spatial to move/rotate/change
      Spatial myspatial = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL);    
      //Change scale
      myspatial.scale(MyConfig.SCALE);
      //config spatial to rootNode
      setSpatial(myspatial, position);

    }
    /**
     * Set Spatial into rootNode
     * @param myspatial spatial object
     * @param position position
     */
    public void setSpatial(Spatial myspatial,Vector3f position){        
        //create the rigid object
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);
        //creating the control
        rigidSpatial = new DoorExteriorControl(sceneSpatial,myMain);
        rigidSpatial.setKinematic(true);
        //put led where we want
        if(ledPosition.getX()!=0 || ledPosition.getY()!=0 || ledPosition.getZ()!=0){
            rigidSpatial.setLedPosition(position.add(ledPosition));
        }
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        myspatial.setLocalRotation(new Quaternion().fromAngles(0f,(float) rotation, 0f));
        //atach spatial to rootnode
        myMain.getRootNode().attachChild(myspatial);
        myMain.getBulletAppState().getPhysicsSpace().add(rigidSpatial); 
    }

    /**
     * @return the position
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the rigidSpatial
     */
    public DoorExteriorControl getRigidSpatial() {
        return rigidSpatial;
    }

    /**
     * @param rigidSpatial the rigidSpatial to set
     */
    public void setRigidSpatial(DoorExteriorControl rigidSpatial) {
        this.rigidSpatial = rigidSpatial;
    
    }        
    /**
     *Object Control, child of ObjectControl.java, we use all the native methods of ObjectControl
     */
    public class DoorExteriorControl extends ObjectControl{
        /**
         * Constructor
         * 
         * @param sceneDoor
         * @param main 
         */
        public DoorExteriorControl(CollisionShape sceneDoor,Main main) {
            super(sceneDoor,main);
            //Set Name
            this.setName(NAME);
            //Set Logo Path
            this.setLogoPath(PATH_ACTION_ICON);
            //Set Code
            this.setCode(CODE);
        }
        /**
         * Update thread
         * @param tpf velocity
         */
        @Override
        public void update(float tpf) {
            Vector3f vector = new Vector3f();
            vector = QuaternionToEuler(spatial.getLocalRotation());

            float actualAngle = radToAngle(QuaternionToEuler(spatial.getLocalRotation()).getX());

            //First iteraction
            if(this.isInit()){ 
                
                spatial.setName(NAME);
                for(int i=0;i<myMain.getRootNode().getChildren().size();i++){
                    if(myMain.getRootNode().getChildren().get(i).getName()==NAME){
                        myMain.getRootNode().getChildren().get(i).setName(NAME+i);
                        break;
                    }
                }
                
                iniangle = this.radToAngle(vector.getX()); //initial angle
                counterAngle = iniangle; //counter to spatial

                //position 0º
                if(Math.round(iniangle) == 0.0f){
                    angleUp = -90f;
                }

                //position -90º
                if(Math.round(iniangle) == -90f){
                    angleUp = -179f;
                }

                //position -180º
                if(Math.round(iniangle) == -180f){
                    iniangle = 179f;
                    counterAngle = iniangle;
                    spatial.setLocalRotation(new Quaternion().fromAngles(0, angleToRad(179), 0));
                    actualAngle = radToAngle(QuaternionToEuler(spatial.getLocalRotation()).getX());
                    angleUp = 90f;
                }

                //position 90º
                if(Math.round(iniangle) == 90f){
                    angleUp = 0f;
                }

                this.setInit(false);
            }

            //if we click E
            if(this.getAction()==true){
                //Opening
                if(Math.round(actualAngle) - Math.round(angleUp) >= 0 && Math.round(actualAngle) - Math.round(angleUp) <= 90){
                     if(!antiLoops){
                        //actualizamos el valor del angulo
                        counterAngle = counterAngle - radToAngle(tpf*this.getSpeed());
                        //update physical space
                        spatial.setLocalRotation(new Quaternion().fromAngles(0, angleToRad(counterAngle), 0));
                     }
                }else{
                    antiLoops = true;
                }

            }else{      
                if(Math.round(actualAngle)>= Math.round(iniangle)+2 || Math.round(actualAngle)<= Math.round(iniangle)-2){
                    if(antiLoops){
                        //actualizamos el valor del angulo
                        counterAngle = counterAngle + radToAngle(tpf*this.getSpeed());
                        //update physical space
                        spatial.setLocalRotation(new Quaternion().fromAngles(0, angleToRad(counterAngle), 0));
                    }
                }else{
                    antiLoops = false;
                }             
            }
            //After the control
            super.update(tpf); 
        }
        /**
         * 
         * @param q Quaternion to convert to Euler
         * @return Vector 3f with the change
         */
        public Vector3f QuaternionToEuler(Quaternion q){ 
            Vector3f v = Vector3f.ZERO; 

            v.setX((float)Math.atan2  
            ( 
                2 * q.getY() * q.getW() - 2 * q.getX() * q.getZ(), 
                   1 - 2 * Math.pow(q.getY(), 2) - 2 * Math.pow(q.getZ(), 2) 
            )); 

            v.setZ((float)Math.asin 
            ( 
                2 * q.getX() * q.getY() + 2 * q.getZ() * q.getW() 
            )); 

            v.setY((float)Math.atan2 
            ( 
                2 * q.getX() * q.getW() - 2 * q.getY() * q.getZ(), 
                1 - 2 * Math.pow(q.getX(), 2) - 2 * Math.pow(q.getZ(), 2) 
            )); 

            if (q.getX() * q.getY() + q.getZ() * q.getW() == 0.5) 
            { 
                v.setX( (float)(2 * Math.atan2(q.getX(), q.getW()))); 
                v.setY(0); 
            } 

            else if (q.getX() * q.getY() + q.getZ() * q.getW() == -0.5) 
            { 
                v.setX((float)(-2 * Math.atan2(q.getX(), q.getW()))); 
                v.setY(0); 
            } 

            return v; 
        }  
        /**
         * convert radians to angle
         * @param rad 
         * @return the angle
         */
        public float radToAngle(float rad){
            return (float) ((rad*180)/Math.PI);
        }
        /**
         * convert angles to radians
         * @param angle 
         * @return radian
         */
        public float angleToRad(float angle){
            return (float) ((angle*Math.PI)/180);
        }
    } 
}
