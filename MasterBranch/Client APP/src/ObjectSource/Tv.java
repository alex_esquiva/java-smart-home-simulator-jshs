package ObjectSource;

import Control.ObjectControl;
import static ObjectSource.BathroomSet.NAME;
import static ObjectSource.WindowBig.PATH_MODEL1;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;
import Simulator.MyConfig;

/**
 * 
 * @author Alejandro Esquiva Rodríguez
 * 
 * 
 */
public class Tv {
    
    //Show Object?
    static final boolean SHOW = true;
    //Object Name
    static final String NAME = "Tv";
    //Where is the model?
    static final String PATH_MODEL1 = "Models/Tv/TvPart1/TvPart1.mesh.j3o";
    static final String PATH_MODEL2 = "Models/Tv/TvPart2/TvPart2.mesh.j3o";
    //Object Code
    static final String CODE = "125";
    //Icon Path example: Interface/FanIcon.png
    static final String PATH_ACTION_ICON = "Interface/TvIcon.png";
    //Init state
    static final boolean INIT_ACTION = false;
    //jME object to represent the object
    private CollisionShape sceneSpatial;
    //Object Control, child of ObjectControl
    private TvControl rigidSpatial;
    //Object Position
    private Vector3f position;
    //Led position, put (0,0,0) to disable led
    private final Vector3f ledPosition = new Vector3f(0, 0, 0);
    //Instance of Main class
    private final Main myMain;
    //Object Rotation
    private float rotation = 0f;
    
    private Spatial tvframe;
    
    /**
     * Constructor
     * @param app main class instance
     * @param pos vector3f position
     */
    public Tv(Main app, Vector3f pos) {
        myMain = app;
        position = pos;
        if(SHOW)initObjects();
    }
    /**
     * 
     * @param app main class instance
     * @param pos vector3f position
     * @param rotation rotation in rad
     */
     public Tv(Main app, Vector3f pos, float rotation) {
        myMain = app; 
        position = pos;
        this.rotation = rotation;
        if(SHOW)initObjects();
    }
    /**
     * Init the objects, init spatial object, put the scale and set the spatial.
     */
    private void initObjects(){
      
        Spatial myspatial = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL1);    
        myspatial.scale(MyConfig.SCALE);
        setSpatial1(myspatial, (Vector3f) position);
        tvframe = (Spatial)myMain.getAssetManager().loadModel(PATH_MODEL2);
        tvframe.scale(MyConfig.SCALE);
        tvframe.setLocalTranslation(position.add(0.04f,0, 0.04f));
        tvframe.setLocalRotation(new Quaternion().fromAngles(0f,(float) rotation, 0f));
        

    }
    /**
     * Set Spatial into rootNode
     * @param myspatial spatial object
     * @param position position
     */
    public void setSpatial1(Spatial myspatial,Vector3f position){
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);     
        Tv.TvControl rigidSpatial = new Tv.TvControl(sceneSpatial,myMain);
        rigidSpatial.setKinematic(true);
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        myspatial.setLocalRotation(new Quaternion().fromAngles(0f,(float) rotation, 0f));
        myMain.getRootNode().attachChild(myspatial);
        myMain.getBulletAppState().getPhysicsSpace().add(rigidSpatial);  
    }


    /**
     * @return the position
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the rigidSpatial
     */
    public TvControl getRigidSpatial() {
        return rigidSpatial;
    }

    /**
     * @param rigidSpatial the rigidSpatial to set
     */
    public void setRigidSpatial(TvControl rigidSpatial) {
        this.rigidSpatial = rigidSpatial;
    
    }        
    /**
     *Object Control, child of ObjectControl.java, we use all the native methods of ObjectControl
     */
    public class TvControl extends ObjectControl{
        
        private boolean antiloop = true;
        
        /**
         * Constructor
         * 
         * @param sceneDoor
         * @param main 
         */
        public TvControl(CollisionShape sceneDoor,Main main) {
            super(sceneDoor,main);
            //Set Name
            this.setName(NAME);
            //Set Logo Path
            this.setLogoPath(PATH_ACTION_ICON);
            //Set Code
            this.setCode(CODE);
            
        }
        /**
         * Update thread
         * @param tpf velocity
         */
        @Override
        public void update(float tpf) {
            super.update(tpf);
            //Here we have to create the control, for example the movement of the object
            if(this.getAction()==true){
                if(antiloop){
                    myMain.getRootNode().attachChild(tvframe);
                    antiloop=!antiloop;
                }
            }else{
                if(!antiloop){
                    myMain.getRootNode().detachChild(tvframe);
                    antiloop=!antiloop;
                }
            }
        }    
    } 
}
