/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectSource;

import Control.ObjectControl;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Vector;
import Simulator.Main;
import Simulator.MyConfig;
import com.jme3.bullet.control.RigidBodyControl;

/**
 *
 * @author IdeaPad Z500
 */
public class House {
    
    //static final int NUM_OBJECTS = 1;
    static final boolean SHOW = true;
    static final String NAME = "House";
    static final String CODE = "001";
    static final String PATH_ACTION_ICON = "Interface/OpenDoorIcon.png";
    static final String PATH_MODEL = "Models/MyHouse/MyHouse.mesh.j3o";
    
    private Main myMain;
    private Node rootNode;
    private AssetManager assetManager;
    private BulletAppState bulletAppState;
    private CollisionShape sceneSpatial;
    private Vector allPosition = new Vector();

    public House(Main app) {
        myMain = app;
        rootNode = app.getRootNode();
        assetManager = app.getAssetManager();
        bulletAppState = app.getBulletAppState();        
        
        if(SHOW)initObjects();
    }

    private void initObjects(){
      //add position objects
        allPosition.add(new Vector3f(0, 0, 0));
      //add allObjects
      for(int i=0;i<allPosition.size();i++){          
          Spatial myspatial = (Spatial)assetManager.loadModel(PATH_MODEL);    
          myspatial.scale(MyConfig.SCALE);
          setSpatial(myspatial, (Vector3f) allPosition.get(i));
          
      }

    }
      
      public void setSpatial(Spatial myspatial,Vector3f position){
        sceneSpatial = CollisionShapeFactory.createMeshShape((Node)myspatial);     
        RigidBodyControl rigidSpatial = new RigidBodyControl(sceneSpatial);
        rigidSpatial.setKinematic(true);
        myspatial.addControl(rigidSpatial);
        myspatial.setLocalTranslation(position);
        rootNode.attachChild(myspatial);
        bulletAppState.getPhysicsSpace().add(rigidSpatial); 
    }
}