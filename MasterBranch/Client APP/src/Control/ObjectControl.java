/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import de.lessvoid.nifty.render.NiftyImage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class ObjectControl extends RigidBodyControl{
    
    //Phisical Characteristics
    private float speed = 1f;
    private float energyConsumptionON = 0;
    private float energyConsumptionOFF = 0;
    private float waterConsumptionON = 0;
    private float waterConsumptionOFF = 0;
    private float temperatureON = 0;
    private float temperatureOFF = 0;
    
    private String name;
    private String code = 0+"";
    private String LogoPath;
    private String place = "";
    private NiftyImage image;
    private boolean action = false;//true=On, false=Off
    private boolean init = true;
    private Spatial myspatial;
    private float rotation = 0;
    private Vector3f position = new Vector3f(0, 0, 0);
    private Vector3f ledPosition = new Vector3f(0, 0, 0);
    private LedStatus myLed;
    private boolean antiLoops = false;
    private Main myMain;
              
    public ObjectControl(CollisionShape sceneDoor,Main main) {
        super(sceneDoor);   
        myMain = main;
        myLed = new LedStatus(main, ledPosition,rotation);
        
    }
    @Override
    public void update(float tpf) {
        super.update(tpf);
        if(init){
            spatial.setName(name);
            for(int i=0;i<myMain.getRootNode().getChildren().size();i++){
                if(myMain.getRootNode().getChildren().get(i).getName()==name){
                    myMain.getRootNode().getChildren().get(i).setName(name+i);
                    break;
                }
            }
                        
            setMyspatial(spatial);
            if(ledPosition.getX()==0 && ledPosition.getY()==0 && ledPosition.getZ()==0){
                myLed.destroy();
            }
            //Set Image Logo Path
            setImage(myMain.getGui().getNifty().getRenderEngine().createImage(myMain.getGui().getNifty().getCurrentScreen(),LogoPath, false));
            
            init = false;

            //Physical Characteristics
            myMain.getPhysicalCharacteristics().increaseEnergyConsumption(energyConsumptionOFF);
            myMain.getPhysicalCharacteristics().increaseWaterConsumption(waterConsumptionOFF);
            myMain.getPhysicalCharacteristics().increaseTemperature(temperatureOFF);

            //update physical variables
            try {
                checkPhysicalVariables();
            } catch (Exception ex) {
                Logger.getLogger(ObjectControl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(this.getAction()==true){
            if(!antiLoops){
               myLed.setStatus(false);          
               
               //Physical Characteristics
               myMain.getPhysicalCharacteristics().decreaseEnergyConsumption(energyConsumptionOFF);
               myMain.getPhysicalCharacteristics().increaseEnergyConsumption(energyConsumptionON);
               myMain.getPhysicalCharacteristics().decreaseWaterConsumption(waterConsumptionOFF);
               myMain.getPhysicalCharacteristics().increaseWaterConsumption(waterConsumptionON);
               myMain.getPhysicalCharacteristics().decreaseTemperature(temperatureOFF);
               myMain.getPhysicalCharacteristics().increaseTemperature(temperatureON);
                
               antiLoops=true;
               updateDataInfo();
            }
         }else{
             if(antiLoops){
               //I've activated the loop
               myLed.setStatus(true);
               
               //Physical Characteristics
               myMain.getPhysicalCharacteristics().decreaseEnergyConsumption(energyConsumptionON);
               myMain.getPhysicalCharacteristics().increaseEnergyConsumption(energyConsumptionOFF);
               myMain.getPhysicalCharacteristics().decreaseWaterConsumption(waterConsumptionON);
               myMain.getPhysicalCharacteristics().increaseWaterConsumption(waterConsumptionOFF);
               myMain.getPhysicalCharacteristics().decreaseTemperature(temperatureON);
               myMain.getPhysicalCharacteristics().increaseTemperature(temperatureOFF);
               
               antiLoops=false;
               updateDataInfo();
             }
        }
    }
    public void actionObject(){
        setAction(!action);
        
    }
    
    private void updateDataInfo(){
               
        myMain.getGui().addObjectInfo(this);
        //myMain.getGui().getConsole().output("addobject called");
        
        myMain.getGui().getConsole().clear();
       for(int i=0;i<myMain.getGui().getObjectsInfo().size();i++){
            ArrayList<String> info = myMain.getGui().getObjectsInfo().get(i);
            myMain.getGui().getConsole().output(info.get(0));
            myMain.getGui().getConsole().output("############");
        }       
    }
    
    private void checkPhysicalVariables() throws Exception{
        String val = myMain.getMyConfig().findPhysicValueXML(name, "SPEED");
        if(val.equals("-1")){
            //GENERATE INFO IN XML
            ArrayList<String> object = new ArrayList<String>();
            ArrayList<String> key = new ArrayList<String>();
            ArrayList<String> value = new ArrayList<String>();
            
            object = myMain.getMyConfig().getMyConfigXMLDefault().getPhysicalObject();
            key = myMain.getMyConfig().getMyConfigXMLDefault().getPhysicalKey();
            value = myMain.getMyConfig().getMyConfigXMLDefault().getPhysicalValue();
            
            object.add(name);
            key.add("SPEED");
            value.add("1");
            key.add("ENERGY_CONSUMPTION_ON");
            value.add("0");
            key.add("ENERGY_CONSUMPTION_OFF");
            value.add("0");
            key.add("WATER_CONSUMPTION_ON");
            value.add("0");
            key.add("WATER_CONSUMPTION_OFF");
            value.add("0");
            key.add("TEMPERATURE_ON");
            value.add("0");
            key.add("TEMPERATURE_OFF");
            value.add("0");
            
            myMain.getMyConfig().getMyConfigXMLDefault().setPhysicalObject(object);
            myMain.getMyConfig().getMyConfigXMLDefault().setPhysicalKey(key);
            myMain.getMyConfig().getMyConfigXMLDefault().setPhysicalValue(value);
            
            myMain.getMyConfig().getMyConfigXMLDefault().getXml().generatePhysicalXML("physicVariables", object, key, value);
            updatePhysicalVariables();
        }else{
            updatePhysicalVariables();
        }
    }

    public void updatePhysicalVariables() throws Exception{
        String NAME = this.getName();
        //Set Speed from physicalVariables.xml
        float speed = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "SPEED"));
        setSpeed(speed);
        //Set Energy Consumption On from physicalVariables.xml
        float eco = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "ENERGY_CONSUMPTION_ON"));
        setEnergyConsumptionON(eco);
        //Set Energy Consumption Off from physicalVariables.xml
        float ecf = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "ENERGY_CONSUMPTION_OFF"));
        setEnergyConsumptionOFF(ecf);
        //Set Water Consumption On from physicalVariables.xml
        float wco = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "WATER_CONSUMPTION_ON"));
        setWaterConsumptionON(wco);
        //Set Water Consumption Off from physicalVariables.xml
        float wcf = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "WATER_CONSUMPTION_OFF"));
        setWaterConsumptionOFF(wcf);
        //Set Temperature On from physicalVariables.xml
        float to = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "TEMPERATURE_ON"));
        setTemperatureON(to);
        //Set Temperature Off from physicalVariables.xml
        float tf = Float.parseFloat(myMain.getMyConfig().findPhysicValueXML(NAME, "TEMPERATURE_OFF"));
        setTemperatureOFF(tf);
    }
    
    public void showInfo(){
        place = new SpacePosition(myMain).getNameSpace(spatial.getLocalTranslation());
        String msg = ""
                   + "[Energy] "
                   + "On: "+getEnergyConsumptionON()+" Off: "+getEnergyConsumptionOFF()+"\n"
                   + "[Water]"
                   + "On: "+getWaterConsumptionON()+" Off: "+getWaterConsumptionOFF()+"\n"
                   + "[Temperature]"
                   + "On: "+getTemperatureON()+" Off: "+getTemperatureOFF()+"\n"
                   + "Place: "+place;
        myMain.getGui().getSimulatorDelay().execute(3000, name + " Information", msg);
    }

    /**
     * @return the speed
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * @return the action
     */
    public boolean isAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(boolean action) {
        this.action = action;
    }
    
    public boolean getAction() {
        return action;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the actionLogoPath
     */
    public String getLogoPath() {
        return LogoPath;
    }

    /**
     * @param actionLogoPath the actionLogoPath to set
     */
    public void setLogoPath(String actionLogoPath) {
        this.LogoPath = actionLogoPath;
    }

    /**
     * @return the init
     */
    public boolean isInit() {
        return init;
    }

    /**
     * @param init the init to set
     */
    public void setInit(boolean init) {
        this.init = init;
    }

    /**
     * @return the myspatial
     */
    public Spatial getMyspatial() {
        return myspatial;
    }

    /**
     * @param myspatial the myspatial to set
     */
    public void setMyspatial(Spatial myspatial) {
        this.myspatial = myspatial;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return the ledPosition
     */
    public Vector3f getLedPosition() {
        return ledPosition;
    }

    /**
     * @param ledPosition the ledPosition to set
     */
    public void setLedPosition(Vector3f ledPosition) {
        this.ledPosition = ledPosition;
        myLed.setPosition(ledPosition);
    }

    /**
     * @return the energyConsumptionON
     */
    public float getEnergyConsumptionON() {
        return energyConsumptionON;
    }

    /**
     * @param energyConsumptionON the energyConsumptionON to set
     */
    public void setEnergyConsumptionON(float energyConsumptionON) {
        this.energyConsumptionON = energyConsumptionON;
    }

    /**
     * @return the energyConsumptionOFF
     */
    public float getEnergyConsumptionOFF() {
        return energyConsumptionOFF;
    }

    /**
     * @param energyConsumptionOFF the energyConsumptionOFF to set
     */
    public void setEnergyConsumptionOFF(float energyConsumptionOFF) {
        this.energyConsumptionOFF = energyConsumptionOFF;
    }

    /**
     * @return the waterConsumptionON
     */
    public float getWaterConsumptionON() {
        return waterConsumptionON;
    }

    /**
     * @param waterConsumptionON the waterConsumptionON to set
     */
    public void setWaterConsumptionON(float waterConsumptionON) {
        this.waterConsumptionON = waterConsumptionON;
    }

    /**
     * @return the waterConsumptionOFF
     */
    public float getWaterConsumptionOFF() {
        return waterConsumptionOFF;
    }

    /**
     * @param waterConsumptionOFF the waterConsumptionOFF to set
     */
    public void setWaterConsumptionOFF(float waterConsumptionOFF) {
        this.waterConsumptionOFF = waterConsumptionOFF;
    }

    /**
     * @return the temperatureON
     */
    public float getTemperatureON() {
        return temperatureON;
    }

    /**
     * @param temperatureON the temperatureON to set
     */
    public void setTemperatureON(float temperatureON) {
        this.temperatureON = temperatureON;
    }

    /**
     * @return the temperatureOFF
     */
    public float getTemperatureOFF() {
        return temperatureOFF;
    }

    /**
     * @param temperatureOFF the temperatureOFF to set
     */
    public void setTemperatureOFF(float temperatureOFF) {
        this.temperatureOFF = temperatureOFF;
    }

    /**
     * @return the position
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
    }

    /**
     * @return the image
     */
    public NiftyImage getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(NiftyImage image) {
        this.image = image;
    }

    /**
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * @param place the place to set
     */
    public void setPlace(String place) {
        this.place = place;
    }

    

}
