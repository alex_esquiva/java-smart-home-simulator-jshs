/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Control;

import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class SimulatorDelay {
    
    private Main myMain;
    private Element element;
    private NiftyImage image;
    private boolean init = true;
    private boolean change = false;
    
    
    private long time = 2000;
    private long extratime = 0;
    private String title = "";
    private String msg = ""; 
    long initmilistime = 0;
    long milistime = 0;
    private boolean antiloop = true;
    
    private ObjectControl control;
    
    public SimulatorDelay(Main app){
        myMain = app;
    }
       
    public SimulatorDelay(Main app,long time,String title, String msg){
        myMain = app;
        this.time = time;
        this.title = title;
        this.msg = msg;
    }
    
    public void reset(){
        initmilistime = myMain.getGui().getMyInfoPanel().getTimeSimulatorMilis();
        milistime = initmilistime;        
    }
    
    public void update(float tpf){
        if(init){
            initmilistime = myMain.getGui().getMyInfoPanel().getTimeSimulatorMilis();
            milistime = initmilistime;
            element = myMain.getGui().getNifty().getCurrentScreen().findElementByName("blackpanel");
            //set title
            myMain.getGui().getNifty().getCurrentScreen().findNiftyControl("titlelabel", Label.class).setText(getTitle());
            myMain.getGui().getNifty().getCurrentScreen().findNiftyControl("msglabel", Label.class).setText(getMsg());
            init = !init;
            control = null;
        }
        
        if(change){
            if(control!=null)control.setAction(false);
            control = null;
            change = !change;
        }
        
        milistime = myMain.getGui().getMyInfoPanel().getTimeSimulatorMilis();
        if((milistime - initmilistime) >=0 && (milistime - initmilistime) < getTime()){
            element.show();
            
            image = myMain.getGui().getNifty().getRenderEngine().createImage(myMain.getGui().getNifty().getCurrentScreen(),"Interface/info.png", false);
            myMain.getGui().getNifty().getCurrentScreen().findElementByName("info").getRenderer(ImageRenderer.class).setImage(image);
            myMain.getGui().getNifty().getCurrentScreen().findNiftyControl("titlelabel", Label.class).setText(getTitle());
            myMain.getGui().getNifty().getCurrentScreen().findNiftyControl("msglabel", Label.class).setText(getMsg());
            float count = (getTime() - (milistime - initmilistime))/1000;
            myMain.getGui().getNifty().getCurrentScreen().findNiftyControl("countlabel", Label.class).setText(Math.round(count)+"");
            myMain.getInitApp().getMyPlayer().enablePlayer(false);
            antiloop = true;
        }else{
            if(antiloop){
                element.hide();
                myMain.getInitApp().getMyPlayer().enablePlayer(true);
                myMain.getGui().getMyInfoPanel().addExtratime(extratime);
                change = true;
                extratime = 0;
                antiloop = !antiloop;
            }
        }
        
    }
    
    public void execute(long time, String title, String msg){
        setTime(time);
        setTitle(title);
        setMsg(msg);
        reset();
    }
    public void execute(ObjectControl control,long time, String title, String msg){
        this.control = control;
        setTime(time);
        setTitle(title);
        setMsg(msg);
        reset();
    }
    public void execute(ObjectControl control,long time, String title, String msg, long simulatorTime){
        this.control = control;
        setTime(time);
        setTitle(title);
        setMsg(msg);
        setExtratime(simulatorTime);
        reset();
    }
    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the extratime
     */
    public long getExtratime() {
        return extratime;
    }

    /**
     * @param extratime the extratime to set
     */
    public void setExtratime(long extratime) {
        this.extratime = extratime;
    }
}
