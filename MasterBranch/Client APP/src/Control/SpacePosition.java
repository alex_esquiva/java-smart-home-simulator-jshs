/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Control;

import ObjectSource.Sphere;
import com.jme3.math.Vector3f;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class SpacePosition {
    private Main myMain;
    boolean drawSpheres = true;
    //HALL
    private Vector3f livingroomInit = new Vector3f(-28,0,-50);
    private Vector3f livingroomRadius = new Vector3f(3,16.3f,-99.999f);
    private Vector3f livingroomInit2 = new Vector3f(-4.1350784f, 0f, -51.21028f);
    private Vector3f livingroomRadius2 = new Vector3f(7.1149216f, 16.3f, -76f);
    //KITCHEN
    private Vector3f kitchenInit = new Vector3f(-28,0,-100);
    private Vector3f kitchenRadius = new Vector3f(4f, 16.3f, -130.33813f);
    //GARAGE
    private Vector3f garageInit = new Vector3f(9,0,50);
    private Vector3f garageRadius = new Vector3f(52,16.3f,-75);
    private Vector3f garageInit2 = new Vector3f(9,0,50);
    private Vector3f garageRadius2 = new Vector3f(52,16.3f,-75);
    //Stairs
    private Vector3f stairsInit = new Vector3f(3.2591228f, 0, -75.33179f);
    private Vector3f stairsRadius = new Vector3f(19.246183f, 16.3f, -117.60817f);
    //Bathroom 1
    private Vector3f bathroom1Init = new Vector3f(19.159674f, 0f, -101.19978f);
    private Vector3f bathroom1Radius = new Vector3f(37f, 16.3f, -116f);
    //Bathroom 2
    private Vector3f bathroom2Init = new Vector3f(-28.635078f, 17f, -75.0f);
    private Vector3f bathroom2Radius = new Vector3f(-8.000724f, 35f, -91.67616f);
    //Bathroom 3
    private Vector3f bathroom3Init = new Vector3f(-4.5750732f, 17f, -101.18541f);
    private Vector3f bathroom3Radius = new Vector3f(20.110685f, 35f, -129.83813f);
    //Office 
    private Vector3f officeInit = new Vector3f(19.14151f, 0f, -83.069916f);
    private Vector3f officeRadius = new Vector3f(36.487923f, 16.3f, -100.5f);
    //Hall
    private Vector3f hallInit = new Vector3f(-7.326937f, 17f, -75.19299f);
    private Vector3f hallRadius = new Vector3f(36.61492f, 35f, -92f);
    //Bedroom 1
    private Vector3f bedroom1Init = new Vector3f(7.650307f, 17f, -46.287064f);
    private Vector3f bedroom1Radius = new Vector3f(36.61492f, 35f, -74.34355f);
    //Bedroom 2
    private Vector3f bedroom2Init = new Vector3f(-28.621454f, 17f, -50f);
    private Vector3f bedroom2Radius = new Vector3f(7.5f, 35f, -74.339516f);
    //Bedroom 3
    private Vector3f bedroom3Init = new Vector3f(-28.635078f, 17f, -92.17615f);
    private Vector3f bedroom3Radius = new Vector3f(-4.6350784f, 35f, -129.83813f);
    //Bedroom 4
    private Vector3f bedroom4Init1 = new Vector3f(7.1149216f, 17f, -92.17615f);
    private Vector3f bedroom4Radius1 = new Vector3f(36.61492f, 35f, -100.48691f);
    private Vector3f bedroom4Init2 = new Vector3f(20.346184f, 17f, -92.17615f);
    private Vector3f bedroom4Radius2 = new Vector3f(36.61492f, 35f, -118.116165f);
    
    
    
    public SpacePosition(Main app){
        myMain = app;
    }
    
    public String getNameSpace(Vector3f input){
        String space = "";
        
        if (checkSpace(input, livingroomInit, livingroomRadius)) return space = "Living Room";
        if (checkSpace(input, livingroomInit2, livingroomRadius2)) return space = "Living Room";
        if (checkSpace(input, kitchenInit, kitchenRadius)) return space = "Kitchen";
        if (checkSpace(input, garageInit, garageRadius)) return space = "Garage";
        if (checkSpace(input, stairsInit, stairsRadius)) return space = "Stairs"; 
        if (checkSpace(input, bathroom1Init, bathroom1Radius)) return space = "Bathroom 1"; 
        if (checkSpace(input, bathroom2Init, bathroom2Radius)) return space = "Bathroom 2"; 
        if (checkSpace(input, bathroom3Init, bathroom3Radius)) return space = "Bathroom 3"; 
        if (checkSpace(input, officeInit, officeRadius)) return space = "Office"; 
        if (checkSpace(input, hallInit, hallRadius)) return space = "Hall"; 
        if (checkSpace(input, bedroom1Init, bedroom1Radius)) return space = "Bedroom 1"; 
        if (checkSpace(input, bedroom2Init, bedroom2Radius)) return space = "Bedroom 2"; 
        if (checkSpace(input, bedroom3Init, bedroom3Radius)) return space = "Bedroom 3"; 
        if (checkSpace(input, bedroom4Init1, bedroom4Radius1)) return space = "Bedroom 4"; 
        if (checkSpace(input, bedroom4Init2, bedroom4Radius2)) return space = "Bedroom 4"; 
        return space;
    }
    
    private Boolean checkSpace(Vector3f input,Vector3f init,Vector3f radius){
        boolean bool=false;
        float inputx = input.getX();
        float inputy = input.getY();
        float inputz = input.getZ();
        
        //myMain.getGui().getConsole().output(inputx + ", "+inputy + ", " + inputz);
        
        if(inputx>=init.getX() && inputx<=radius.getX()){
             //myMain.getGui().getConsole().output("check X ok");
             
            if(inputy>=init.getY() && inputy<=radius.getY()){
               // myMain.getGui().getConsole().output("check Y ok");
                
                if(Math.abs(inputz)>=Math.abs(init.getZ()) && Math.abs(inputz)<=Math.abs(radius.getZ())){
                   // myMain.getGui().getConsole().output("check Z ok");
                    bool = true;
                }else{
                    bool = false;
                }
            }else{
                bool = false;
            }
        }else{
            bool = false;
        }
        
        return bool;
    }
    
    public void drawSpheres(){
        new Sphere(myMain, livingroomInit);
        new Sphere(myMain, livingroomInit2);
        new Sphere(myMain, livingroomRadius);
        new Sphere(myMain, livingroomRadius2);
        new Sphere(myMain, kitchenInit);
        new Sphere(myMain, kitchenRadius);
        new Sphere(myMain, garageInit);
        new Sphere(myMain, garageInit2);
        new Sphere(myMain, garageRadius);
        new Sphere(myMain, garageRadius2);
        new Sphere(myMain, stairsInit);
        new Sphere(myMain, bathroom1Init);
        new Sphere(myMain, bathroom1Radius);
        new Sphere(myMain, officeInit);
        new Sphere(myMain, officeRadius);
        new Sphere(myMain, hallInit);
        new Sphere(myMain, hallRadius);
    }
}
