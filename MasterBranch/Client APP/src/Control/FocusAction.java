/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class FocusAction {
    private Spatial spatial;
    private Node rootNode;
    private Geometry mark;
    private Camera cam;
    private AssetManager assetManager;
    private Main myMain;
    public FocusAction(Main app){
        myMain = app;
        spatial = app.getRootNode();
        rootNode = app.getRootNode();
        cam = app.getCamera();
        assetManager = app.getAssetManager();
        
        initMark();
    }
    
    protected void initMark() {
        Sphere sphere = new Sphere(30, 30, 0.1f);
        mark = new Geometry("BOOM!", sphere);
        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Blue);
        mark.setMaterial(mark_mat);
    }

    public void tryAction(){
              
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        spatial.collideWith(ray, results);
        // 4. Print the results
        //System.out.println("----- Collisions? " + results.size() + "-----");
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
           try{
                String name = results.getCollision(0).getGeometry().getParent().getName();
                //myMain.getGui().getConsole().output(name);
                ObjectControl control = myMain.getRootNode().getChild(name).getControl(ObjectControl.class);
                control.actionObject();
                myMain.getGui().getConsole().output(name+"->"+control.getAction());
            }catch(NullPointerException e){}
        }
        
        
    
    }

    public void tryInfo() {
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        spatial.collideWith(ray, results);
        // 4. Print the results
        //System.out.println("----- Collisions? " + results.size() + "-----");
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
           try{
                String name = results.getCollision(0).getGeometry().getParent().getName();
                //myMain.getGui().getConsole().output(name);
                ObjectControl control = myMain.getRootNode().getChild(name).getControl(ObjectControl.class);
                control.showInfo();
                myMain.getGui().getConsole().output(name+"->"+control.getAction());
            }catch(NullPointerException e){}
        }        
    }
    
}

