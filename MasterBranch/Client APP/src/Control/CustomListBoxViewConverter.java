/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package Control;

import de.lessvoid.nifty.controls.ListBox.ListBoxViewConverter;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.tools.Color;




/**
*
* @author Javi
 * @param <T>
*/
public class CustomListBoxViewConverter<T> implements ListBoxViewConverter<CustomListBoxItem>
{
private static final String ICON_STATUS = "#status";
private static final String TIME = "#time";
private static final String NAME = "#name";
private static final String WHERE = "#where";
private static final String EC = "#ec";
private static final String WC = "#wc";
private static final String TC = "#tc";
private static final String ICON = "#icono";

public final void display(final Element listBoxItem, final CustomListBoxItem item)
{
    final Element icon_status = listBoxItem.findElementByName(ICON_STATUS);
    final ImageRenderer icon_statusRenderer = icon_status.getRenderer(ImageRenderer.class);
    final Element time = listBoxItem.findElementByName(TIME);
    final TextRenderer timeRenderer = time.getRenderer(TextRenderer.class);
    final Element name = listBoxItem.findElementByName(NAME);
    final TextRenderer nameRenderer = name.getRenderer(TextRenderer.class);
    final Element where = listBoxItem.findElementByName(WHERE);
    final TextRenderer whereRenderer = where.getRenderer(TextRenderer.class);
    final Element ec = listBoxItem.findElementByName(EC);
    final TextRenderer ecRenderer = ec.getRenderer(TextRenderer.class);
    final Element wc = listBoxItem.findElementByName(WC);
    final TextRenderer wcRenderer = wc.getRenderer(TextRenderer.class);
    final Element tc = listBoxItem.findElementByName(TC);
    final TextRenderer tcRenderer = tc.getRenderer(TextRenderer.class);
    final Element icon = listBoxItem.findElementByName(ICON);
    final ImageRenderer iconRenderer = icon.getRenderer(ImageRenderer.class);
 
    
    String label_consumption = item.getEc();
    String status_consumption = label_consumption.substring(0,3);
    String text_label_energy_consumption = label_consumption.substring(3);
    
    if(status_consumption.equals("[-]")){
        ecRenderer.setColor(new Color("#ff0000ff"));
    }else{
        ecRenderer.setColor(new Color("#00ff00ff"));
    }
    
    label_consumption = item.getWc();
    status_consumption = label_consumption.substring(0,3);
    String text_label_water_consumption = label_consumption.substring(3);
    
    if(status_consumption.equals("[-]")){
        wcRenderer.setColor(new Color("#ff0000ff"));
    }else{
        wcRenderer.setColor(new Color("#00ff00ff"));
    }
    
    label_consumption = item.getTc();
    status_consumption = label_consumption.substring(0,3);
    String text_label_temperature_consumption = label_consumption.substring(3);
    
    if(status_consumption.equals("[-]")){
        tcRenderer.setColor(new Color("#ff0000ff"));
    }else{
        tcRenderer.setColor(new Color("#00ff00ff"));
    }
    
    if (item != null) {
        icon_statusRenderer.setImage(item.getIconStatus());
        timeRenderer.setText(item.getTime());
        nameRenderer.setText(item.getName());
        whereRenderer.setText(item.getWhere());
        ecRenderer.setText(text_label_energy_consumption);
        wcRenderer.setText(text_label_water_consumption);
        tcRenderer.setText(text_label_temperature_consumption);
        iconRenderer.setImage(item.getIcon());
    } else {
        icon_statusRenderer.setImage(null);
        timeRenderer.setText("");
        nameRenderer.setText("");
        whereRenderer.setText("");
        ecRenderer.setText("");
        wcRenderer.setText("");
        tcRenderer.setText("");
        iconRenderer.setImage(null);
    }
}


public final int getWidth(Element listBoxItem, CustomListBoxItem item)
{
final Element time = listBoxItem.findElementByName(TIME);
final TextRenderer timeRenderer = time.getRenderer(TextRenderer.class);
final Element name = listBoxItem.findElementByName(NAME);
final TextRenderer nameRenderer = name.getRenderer(TextRenderer.class);
final Element ec = listBoxItem.findElementByName(EC);
final TextRenderer ecRenderer = ec.getRenderer(TextRenderer.class);
final Element wc = listBoxItem.findElementByName(WC);
final TextRenderer wcRenderer = wc.getRenderer(TextRenderer.class);
final Element tc = listBoxItem.findElementByName(TC);
final TextRenderer tcRenderer = tc.getRenderer(TextRenderer.class);

final Element icon = listBoxItem.findElementByName(ICON);
final ImageRenderer iconRenderer = icon.getRenderer(ImageRenderer.class);
    
    int value = ((timeRenderer.getFont() == null) ? 0 : timeRenderer.getFont().getWidth(item.getTime()))
                + ((nameRenderer.getFont() == null) ? 0 : nameRenderer.getFont().getWidth(item.getName()))
                + ((ecRenderer.getFont() == null) ? 0 : ecRenderer.getFont().getWidth(item.getEc()))
                + ((wcRenderer.getFont() == null) ? 0 : wcRenderer.getFont().getWidth(item.getWc()))
                + ((tcRenderer.getFont() == null) ? 0 : tcRenderer.getFont().getWidth(item.getTc()))
                + ((iconRenderer.getImage() == null) ? 0 : iconRenderer.getImage().getWidth()
    );

    return value;
}
}