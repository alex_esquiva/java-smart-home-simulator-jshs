/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Control;

/**
 *
 * @author IdeaPad Z500
 */

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Sphere;
import Simulator.Main;

public class LedStatus {

    private boolean SHOW = true;
    private boolean status = false;
    private Geometry mark;
    private Material mark_mat;
    private Vector3f position;
    private float rotation;
    Main myMain;

    public LedStatus(Main app, Vector3f pos ,float rot) {
        myMain = app; 
        position = pos;
        rotation = rot;
        if(SHOW)initObjects();
    }

    private void initObjects(){        

        Sphere sphere = new Sphere(30, 30, 0.08f);
        mark = new Geometry("Mark", sphere);
        Vector3f p;
        
        mark.setLocalTranslation(position);
        mark.setLocalRotation(new Quaternion().fromAngles(0f,(float) rotation, 0f));
        mark_mat = new Material(myMain.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Red);
        mark.setMaterial(mark_mat);
        mark.setLocalTranslation(position);
        myMain.getRootNode().attachChild(mark);
  
    }
      


    /**
     * @return the position of the object
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector3f position) {
        this.position = position;
        mark.setLocalTranslation(position);
    }

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
        if(status){
            mark_mat.setColor("Color", ColorRGBA.Red);
        }else{
            mark_mat.setColor("Color", ColorRGBA.Green);
        }
    }

    void destroy() {
        myMain.getRootNode().detachChild(mark);
    }

    
}
