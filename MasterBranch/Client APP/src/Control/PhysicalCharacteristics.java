/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Control;

import java.text.DecimalFormat;
import Simulator.MyConfig;

/**
 *
 * @author IdeaPad Z500
 */
public class PhysicalCharacteristics {
   
    private float temperature = MyConfig.INIT_TEMPERATURE;
    private float energyConsumption = 0;
    private float waterConsumption = 0;
    
    private float totalWater = 0;
    
    public PhysicalCharacteristics(){}

    /**
     * @return the temperature
     */
    public float getTemperature() {
        return temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }
    
    public void increaseTemperature(float increase){
        this.temperature = temperature + increase;
    }
    
    public void decreaseTemperature(float decrease){
        this.temperature = temperature - decrease;
    }

    /**
     * @return the eneryConsumption
     */
    public float getEnergyConsumption() {
        return energyConsumption;
    }

    /**
     * @param eneryConsumption the eneryConsumption to set
     */
    public void setEnergyConsumption(float eneryConsumption) {
        this.energyConsumption = eneryConsumption;
    }
    
    public void increaseEnergyConsumption(float increase){
        this.energyConsumption = energyConsumption + increase;
    }
    
    public void decreaseEnergyConsumption(float decrease){
        this.energyConsumption = energyConsumption - decrease;
    }

    /**
     * @return the waterConsumption
     */
    public float getWaterConsumption() {
        return waterConsumption;
    }

    /**
     * @param waterConsumption the waterConsumption to set
     */
    public void setWaterConsumption(float waterConsumption) {
        this.waterConsumption = waterConsumption;
        totalWater = waterConsumption;
    }
    
    public void increaseWaterConsumption(float increase){
        this.waterConsumption = waterConsumption + increase;
        totalWater += increase;
    }
    
    public void decreaseWaterConsumption(float decrease){
        this.waterConsumption = waterConsumption - decrease;
    }

    public float getTotalWater() {
        return totalWater;
    }
   
    
}
