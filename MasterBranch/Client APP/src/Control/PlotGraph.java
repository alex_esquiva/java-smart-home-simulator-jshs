package Control;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.*;
import Simulator.Main;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class PlotGraph {

    public static JFrame frame;
    private Main myMain;
    BufferedImage img;
    public static int WIDTH = 400;
    public static int HEIGHT = 300;
    
    
    public PlotGraph(Main myMain) {
        this.myMain = myMain;
    }

    public PlotGraph() throws IOException {

    }


    public void plotChart(String name,ArrayList<String> arrayData,ArrayList<String> arrayTime) throws IOException{
        // Create a simple Bar chart
        String measure = "";
        
        if(name.equals("Electrical Consumption")) measure = "KW/H";
        if(name.equals("Water Consumption")) measure = "L";
        if(name.equals("Temperature")) measure = "ºC";
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for(int i=0;i<arrayData.size();i++){
            dataset.addValue(Float.parseFloat(arrayData.get(i)), measure, arrayTime.get(i)+""); 
        }
        
        JFreeChart chart = ChartFactory.createLineChart( 
            name, // Titulo 
            "Time", // Etiqueta de datos 
            measure, // Etiqueta de valores 
            dataset, // Datos 
            PlotOrientation.VERTICAL, // orientacion 
            false, // Incluye leyenda 
            true, // Incluye tooltips 
            false // urls 
        ); 
        
        //PLOT STYLE
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        //LINE COLOR
        plot.getRenderer().setSeriesPaint(0, Color.BLUE);
        //thickness line
        plot.getRenderer().setSeriesStroke(0, new BasicStroke(5.0f));
        //SET GRID
        plot.setRangeGridlinePaint(Color.PINK);
        //BACKGROUND COLOR
        plot.setBackgroundPaint(Color.WHITE);
        
        int resolution = 50;
        int screenResolution = Toolkit.getDefaultToolkit().getScreenResolution();
        double scaleRatio = resolution / screenResolution;
        int rasterWidth = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()* scaleRatio);
        int rasterHeight = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()* scaleRatio);
        
        img = (BufferedImage) chart.createBufferedImage(800,600, BufferedImage.TRANSLUCENT, new ChartRenderingInfo(new StandardEntityCollection()));
        
        AffineTransform transform = AffineTransform.getScaleInstance(2, -2);
        transform.translate(0,-img.getHeight());
        
        
        AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
        img = op.filter(img, null);

        
        
        
    }
    
    private BufferedImage getHighResChartImage(BufferedImage img, int resolution) {
        int screenResolution = Toolkit.getDefaultToolkit().getScreenResolution();
        double scaleRatio = resolution / screenResolution;
        int rasterWidth = (int) (img.getWidth() * scaleRatio);
        int rasterHeight = (int) (img.getHeight() * scaleRatio);
        BufferedImage image = new BufferedImage(rasterWidth, rasterHeight, BufferedImage.TYPE_INT_RGB);
        return image;
    }
    

    public Image getImage() {
        return img;
    }
    
    public BufferedImage getImageBuffered() {
        return img;
    }

      
}