/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import ObjectSource.Fan;
import ObjectSource.Fridge;
import ObjectSource.LightBasic;
import ObjectSource.Microwave;
import ObjectSource.SmokeStack;
import ObjectSource.WashingMachine;
import ObjectSource.WindowBig;
import ObjectSource.WindowLarge;
import ObjectSource.WindowSmall;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import java.util.Objects;
import java.util.Vector;
import Simulator.Main;
import Simulator.MyConfig;

/**
 *
 * @author Alejandro Esquiva Rodríguez
 * 
 * When status is true We switch the app to the BuildMode now we can create objects int the scenary
 * Mouse Left -> Create an object
 * Mouse Right -> Delete an Object
 * Mouse Scroll Button -> Switch Object
 */
public class BuildMode {
    private Main myMain;
    private Vector objectsBox = new Vector();
    private int indexObject = 0;
    private boolean status = false;
    private float rotation = 0;
    
    public BuildMode(Main app){
        myMain = app;
        
        float buildM = 0;
        
        try{
             buildM = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.GENERAL_XML_PATH, "BUILD_MODE_STATUS"));
        }catch(Exception e){
            
        }
        
        if(buildM == 1){
            status=true;
        }else{
            status=false;
        }
    }
    
    public void createobject(){
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(myMain.getCamera().getLocation(), myMain.getCamera().getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        myMain.getRootNode().collideWith(ray, results);
        // 4. Print the results
        //System.out.println("----- Collisions? " + results.size() + "-----");
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
           try{
                vectorObjectBox(indexObject, results.getCollision(0).getContactPoint());
            }catch(NullPointerException e){}
        }  
    
    }
    
    public void deleteObject(){
                // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(myMain.getCamera().getLocation(), myMain.getCamera().getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        myMain.getRootNode().collideWith(ray, results);
        // 4. Print the results
        //System.out.println("----- Collisions? " + results.size() + "-----");
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
           try{             
                int index = myMain.getRootNode().getChildIndex(results.getCollision(0).getGeometry().getParent());
                System.out.println("Index: "+index);
                myMain.getBulletAppState().getPhysicsSpace().remove(results.getCollision(0).getGeometry().getParent());
                myMain.getRootNode().detachChildAt(index); 
            }catch(NullPointerException e){}
        }
    }
    
    public void switchObject(){
        indexObject++;
        if(indexObject == MyConfig.NUM_OBJECT) indexObject = 0 ;
    }
    
    public void vectorObjectBox(int add,Vector3f position){
  
      switch (add) {
            case 0:  
                     break;  
            case 1:  objectsBox.add(new WashingMachine(myMain,position,rotation));
                     break;
            case 2:  objectsBox.add(new Fan(myMain,position));
                     break;
            case 3:  //objectsBox.add(new LightFloorTraditional(myMain,position));
                     break;
            case 4:  objectsBox.add( new LightBasic(myMain,position));
                     break;
            case 5:  objectsBox.add(new Fridge(myMain,position,rotation));
                     break;
            case 6:  objectsBox.add(new SmokeStack(myMain,position));
                     break;
            case 7:  objectsBox.add(new WindowSmall(myMain,position));
                     break;
            case 8:  objectsBox.add(new WindowLarge(myMain,position));
                     break;
            case 9:  objectsBox.add(new WindowBig(myMain,position));
                     break;
            case 10: //objectsBox.add(new Tv(myMain,position));
                     break;  
            case 11: objectsBox.add(new Microwave(myMain,position));
                     break;  
            default: 
                     break;
        }
      
      for(int i = 0;i<myMain.getRootNode().getChildren().size();i++){
        String name = myMain.getRootNode().getChildren().get(i).getName();
        myMain.getRootNode().getChildren().get(i).setName("object "+(i+1));
      }
  }
    
    
    public void update(float tpf){
        if(status){
            try{
                int size = myMain.getRootNode().getChildren().size();
                
                //Miramos el código para saber cuantos spatial tiene cada uno
                int code = Integer.parseInt(myMain.getRootNode().getChildren().get(size-1).getControl(ObjectControl.class).getCode());
                myMain.getGui().getConsole().output("CODE: "+code);
                if(code == 9 || code == 10 || code == 11){
                    //Si tiene dos partes
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-1).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-1);
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-2).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-2);   
                }else if(code == 12){
                    //4 partes
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-1).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-1);
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-2).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-2);   
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-3).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-3);
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-4).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-4);   
                }else{
                    //1 parte
                    myMain.getGui().getConsole().output("1Parte");
                    myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-1).getControl(ObjectControl.class));
                    myMain.getRootNode().detachChildAt(size-1); 
                }                
        
                createobject();
            }catch(Exception e){
            
            }
        }

    }

    /**
     * @return the indexObject
     */
    public int getIndexObject() {
        return indexObject;
    }

    /**
     * @param indexObject the indexObject to set
     */
    public void setIndexObject(int indexObject) {
        this.indexObject = indexObject;
    }

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the rotation
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation the rotation to set
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public void rotateObject() {
        float rota = this.getRotation();
        rota = (float) (rota + Math.PI/2);
        this.setRotation(rota);
    }
    
}
