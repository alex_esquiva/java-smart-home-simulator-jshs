/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Control;

import ObjectSource.LightBasic;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import java.util.ArrayList;
import Simulator.Main;
import Simulator.MyConfig;

/**
 *
 * @author IdeaPad Z500
 */


public class LightControl {
    Main myMain;
    Spatial sky = null;
    AmbientLight al = null;
    DirectionalLight dl1;
    DirectionalLight dl;
    private String status = "day";
    private String playerPlace = "";
    
    public LightControl(Main app){
        myMain = app;
        setUpLight(0.6f);
    }
    
    public void setDay(){
            status = "day";
            myMain.getRootNode().detachChildNamed("sky");
            modifyLight(0.6f);
    }
    
    public void setNight(){
        status = "night";
        
        Texture west = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_west.jpg");
        Texture east = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_east.jpg");
        Texture north = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_north.jpg");
        Texture south = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_south.jpg");
        Texture up = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_up.jpg");
        Texture down = myMain.getAssetManager().loadTexture("Textures/Sky/Lagoon/lagoon_down.jpg");

        sky = SkyFactory.createSky(myMain.getAssetManager(), west, east, north, south, up, down);
        sky.setName("sky");
        myMain.getRootNode().attachChild(sky);
        
        modifyLight(0.1f);
        
    }
    
     private void setUpLight(float intensity) {
        //float intensity = 0.6f;
         
        try{
            intensity = Float.parseFloat(myMain.getMyConfig().getXmlObject().findValueXML(MyConfig.GENERAL_XML_PATH, "INIT_LIGHT_INTENSITY"));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(intensity));
        al.setName("al");
        myMain.getRootNode().addLight(al);

        dl1 = new DirectionalLight();
        dl1.setDirection(new Vector3f(1, -1, 1).normalizeLocal());
        //dl1.setColor(new ColorRGBA(0.965f, 0.949f, 0.772f, 1f).mult(0.7f));
        dl1.setColor(ColorRGBA.White.mult(intensity));
        dl1.setName("dl1");
        myMain.getRootNode().addLight(dl1);

        dl = new DirectionalLight();
        dl.setDirection(new Vector3f(-1, -1, -1).normalizeLocal());
        //dl.setColor(new ColorRGBA(0.965f, 0.949f, 0.772f, 1f).mult(0.7f));
        dl.setColor(ColorRGBA.White.mult(intensity));
        dl.setName("dl");
        myMain.getRootNode().addLight(dl);
    }
     
     public void modifyLight(float intensity){
         al.setColor(ColorRGBA.White.mult(intensity));
         dl1.setColor(ColorRGBA.White.mult(intensity));
         dl.setColor(ColorRGBA.White.mult(intensity));
     }
     //MODO BETA 
     public void smartLights(){
         SpacePosition space = new SpacePosition(myMain);
         String place = myMain.getInitApp().getMyPlayer().getPlace();
         
         if(playerPlace.equals(place)){
             return;
         }else{            
             playerPlace = place;
         }
         
         if(status.equals("day")){
             for(int i=0;i<myMain.getRootNode().getChildren().size();i++){
                 String name = "LightBasic"+i;
                 try{
                     ObjectControl control = myMain.getRootNode().getChild(name).getControl(ObjectControl.class);
                     if(place == space.getNameSpace(control.getPosition()) && place!=""){
                         control.setAction(false);
                     }
                 }catch(NullPointerException e){}
             }
             
         }
         if(status.equals("night")){
             for(int i=0;i<myMain.getRootNode().getChildren().size();i++){
                 String name = "LightBasic"+i;
                 try{
                     ObjectControl control = myMain.getRootNode().getChild(name).getControl(ObjectControl.class);
                     String objectPlace = space.getNameSpace(control.getPhysicsLocation());
                     if(objectPlace.equals(place)){
                         control.setAction(true);
                     }else{
                         control.setAction(false);
                     }
                 }catch(NullPointerException e){}
             }
         }
     }
     
     public void switchRoom(String place){
         SpacePosition space = new SpacePosition(myMain);
         for(int i=0;i<myMain.getRootNode().getChildren().size();i++){
            String name = "LightBasic"+i;
            try{
                ObjectControl control = myMain.getRootNode().getChild(name).getControl(ObjectControl.class);
                if(place == space.getNameSpace(control.getPhysicsLocation()) && place!=""){
                    control.actionObject();
                    System.out.println("ACTIVADO");
                }
            }catch(NullPointerException e){}
        }
     }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */    
    
}
