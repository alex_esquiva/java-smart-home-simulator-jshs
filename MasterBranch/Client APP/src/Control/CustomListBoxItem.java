/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package Control;

import de.lessvoid.nifty.render.NiftyImage;

/**
*
* @author Javi
*/
public class CustomListBoxItem  {

    private NiftyImage iconStatus;
    private String time;
    private String name;
    private String where;
    private String ec;
    private String wc;
    private String tc;
    private NiftyImage icono;


    public CustomListBoxItem(NiftyImage iconStatus,String time, String name,String where,String ec, String wc,String tc, NiftyImage icono)
    {
        this.iconStatus = iconStatus;
        this.time = time;
        this.name = name;
        this.where = where;
        this.ec = ec;
        this.wc = wc;
        this.tc = tc;
        this.icono = icono;
    }

    public NiftyImage getIcon()
    {
        return this.icono;
    }

    public void setIcon(NiftyImage icono)
    {
        this.icono = icono;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ec
     */
    public String getEc() {
        return ec;
    }

    /**
     * @param ec the ec to set
     */
    public void setEc(String ec) {
        this.ec = ec;
    }

    /**
     * @return the wc
     */
    public String getWc() {
        return wc;
    }

    /**
     * @param wc the wc to set
     */
    public void setWc(String wc) {
        this.wc = wc;
    }

    /**
     * @return the tc
     */
    public String getTc() {
        return tc;
    }

    /**
     * @param tc the tc to set
     */
    public void setTc(String tc) {
        this.tc = tc;
    }

    /**
     * @return the iconStatus
     */
    public NiftyImage getIconStatus() {
        return iconStatus;
    }

    /**
     * @param iconStatus the iconStatus to set
     */
    public void setIconStatus(NiftyImage iconStatus) {
        this.iconStatus = iconStatus;
    }

    /**
     * @return the where
     */
    public String getWhere() {
        return where;
    }

    /**
     * @param where the where to set
     */
    public void setWhere(String where) {
        this.where = where;
    }

}