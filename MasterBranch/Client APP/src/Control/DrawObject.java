/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import ObjectSource.Fan;
import ObjectSource.Fridge;
import ObjectSource.LightBasic;
import ObjectSource.Microwave;
import ObjectSource.SmokeStack;
import ObjectSource.WindowBig;
import ObjectSource.WindowLarge;
import ObjectSource.WindowSmall;
import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Vector;
import Simulator.Main;

/**
 *
 * @author IdeaPad Z500
 */
public class DrawObject {
    private Spatial spatial;
    private Node rootNode;
    private Geometry mark;
    private Camera cam;
    private AssetManager assetManager;
    private Main myMain;
    private Vector objectsBox = new Vector();
    private int indexObject = 0;
    
    public DrawObject(Main app){
        myMain = app;
        spatial = app.getRootNode();
        rootNode = app.getRootNode();
        cam = app.getCamera();
        assetManager = app.getAssetManager();   
    }
    
    public void tryShoot(){
              
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        spatial.collideWith(ray, results);
        // 4. Print the results
        //System.out.println("----- Collisions? " + results.size() + "-----");
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
           try{
                vectorObjectBox(indexObject, results.getCollision(0).getContactPoint());
            }catch(NullPointerException e){}
        }
        
        
    
    }
    
    public void vectorObjectBox(int add,Vector3f position){
  
      switch (add) {
            case 0:  objectsBox.add(new SmokeStack(myMain,position));
                     break;  
            case 1:  objectsBox.add(new Microwave(myMain,position));
                     break;
            case 2:  objectsBox.add(new Fan(myMain,position));
                     break;
            case 3:  //objectsBox.add(new LightFloorTraditional(myMain,position));
                     break;
            case 4:  objectsBox.add( new LightBasic(myMain,position));
                     break;
            case 5:  objectsBox.add(new Fridge(myMain,position));
                     break;
            case 6:  objectsBox.add(new WindowSmall(myMain,position));
                     break;
            case 7:  objectsBox.add(new WindowBig(myMain,position));
                     break;
            case 8:  objectsBox.add(new WindowLarge(myMain,position));
                     break;
            case 9:  //objectsBox.add(new Tv(myMain,position));
                     break;
            default: 
                     break;
        }
      
      if(objectsBox.size()==20) objectsBox.remove(0);
      
      /*for(int i = 0;i<myMain.getRootNode().getChildren().size();i++){
          String name = myMain.getRootNode().getChildren().get(i).getName();
          int count = 1;
          
          for(int j = i+1;j<myMain.getRootNode().getChildren().size();j++){
            String name2 = myMain.getRootNode().getChildren().get(j).getName();          
            if(name2 == name){
                myMain.getRootNode().getChildren().get(j).setName(name2+count);
                count++;
            }
          }
      }*/
      
      for(int i = 0;i<myMain.getRootNode().getChildren().size();i++){
        String name = myMain.getRootNode().getChildren().get(i).getName();
        myMain.getRootNode().getChildren().get(i).setName("object "+(i+1));
      }
  }
    
    
    public void update(float tpf){
        try{
            int size = myMain.getRootNode().getChildren().size();
            myMain.getBulletAppState().getPhysicsSpace().remove(myMain.getRootNode().getChildren().get(size-1).getControl(ObjectControl.class));
            myMain.getRootNode().detachChildAt(size-1);         
            drawBoundObject();
        }catch(Exception e){
            
        }

    }

    private void drawBoundObject() {
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        spatial.collideWith(ray, results);
        // 4. Print the results
        //System.out.println("----- Collisions? " + results.size() + "-----");
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
           try{
             vectorObjectBox(indexObject, results.getCollision(0).getContactPoint());
           }catch(NullPointerException e){}
        }
    }

    /**
     * @return the indexObject
     */
    public int getIndexObject() {
        return indexObject;
    }

    /**
     * @param indexObject the indexObject to set
     */
    public void setIndexObject(int indexObject) {
        this.indexObject = indexObject;
    }
    
}

